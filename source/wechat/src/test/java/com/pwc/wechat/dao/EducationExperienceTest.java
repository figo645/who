package com.pwc.wechat.dao;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Commit;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.pwc.wechat.config.spring.WebContext;
import com.pwc.wechat.dao.candidate.HybridDao;
import com.pwc.wechat.entity.candidate.EducationExperience;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebContext.class)
@WebAppConfiguration
@Transactional
public class EducationExperienceTest {

	@Autowired
	private HybridDao hibernateDao;
	
	@Commit
	@Test
	public void testInsert() {
		EducationExperience app = new EducationExperience();
		app.setNationalId("3666");
		app.setExperienceId(12);
		app.setUniversity("Intern");
		app.setPeriod("Success");
		app.setMajor("hhh");
		app.setGpa("uu");
	}
	
	@Commit
	@Test
	public void testUpdate() {
		EducationExperience app = new EducationExperience();
		app.setNationalId("31010520160101000X");
		app.setExperienceId(123);
		app.setUniversity("Int");
		app.setPeriod("Suc");
		app.setMajor("h");
		app.setGpa("uurr");
	}
	
	@Commit
	@Test
	public void testFind() {
		EducationExperience app = new EducationExperience();
		app.setNationalId("31010520160101000X");
		app.setExperienceId(123);
	}
	
	@Commit
	@Test
	public void testdelete(){
	}
	
}
