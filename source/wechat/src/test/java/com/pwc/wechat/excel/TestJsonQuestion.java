package com.pwc.wechat.excel;

import org.junit.Test;

import com.pwc.wechat.entity.candidate.QuestAndAnswer;
import com.pwc.wechat.util.JsonUtil;

public class TestJsonQuestion {

	@Test
	public void test() {
		String string = "{\"answer\": \"310105199001070100\", \"question\": null, \"questionnaireList\": [{\"answer\": \"水电费第三方第三方\", \"question\": \"你毕业之后有出国留学的打算吗？\", \"questionnaireList\": []}, {\"answer\": \"的费大幅度\", \"question\": \"目前成绩在专业中排名多少？专业一共多少人？\", \"questionnaireList\": []}, {\"answer\": \"是对方的身份\", \"question\": \"有在准备保研或考研吗？\", \"questionnaireList\": []}, {\"answer\": \"对方的说法\", \"question\": \"按照年薪计算，你毕业之后的期望薪水多少？\", \"questionnaireList\": []}, {\"answer\": \"第三方第三方\", \"question\": \"你毕业之后想从事哪些方面的工作？3-5年的职业规划是怎样的？\", \"questionnaireList\": []}, {\"answer\": null, \"question\": \"你如何看待大学毕业生创业？你有创业的打算吗？\", \"questionnaireList\": []}, {\"answer\": null, \"question\": \"你有考公务员的计划，或正在准备/已经参加公务员考试吗？\", \"questionnaireList\": []}, {\"answer\": null, \"question\": \"国企、外企、事业单位、大型民营企业，你更加倾向于哪类企业？为什么？\", \"questionnaireList\": []}, {\"answer\": \"撒阿萨德\", \"question\": \"你对软件开发和IT 咨询行业有兴趣吗？有过相关的实习或者项目经验吗？如有，请谈谈你在这些活动中担任的角色和贡献？\", \"questionnaireList\": []}]}";
		QuestAndAnswer questAndAnswer = JsonUtil.toNormalObject(string, QuestAndAnswer.class);
		questAndAnswer.getQuestAndAnswerList().forEach(one->System.out.println(one.getQuestion()));
	}

}
