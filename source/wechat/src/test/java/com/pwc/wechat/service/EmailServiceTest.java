package com.pwc.wechat.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.pwc.wechat.config.spring.WebContext;
import com.pwc.wechat.entity.hr.Config;
import com.pwc.wechat.service.hr.round.ITalentDataSetHandler;
import com.pwc.wechat.service.mail.EmailService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebContext.class)
@WebAppConfiguration
public class EmailServiceTest {

	@Autowired
	private EmailService emailService;
	@Autowired
	private ITalentDataSetHandler iTalentDataSetHandler;
	@Test
	public void test() {
		Config config = new Config();
		//config.setNationalId("123456789");
		config.setRoundMoveFlag("true");
		iTalentDataSetHandler.addmoveflag(config);
	}
}
