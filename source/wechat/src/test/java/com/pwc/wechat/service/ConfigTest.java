package com.pwc.wechat.service;

import java.io.IOException;
import java.net.MalformedURLException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.test.annotation.Commit;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.pwc.wechat.config.spring.WebContext;
import com.pwc.wechat.dao.candidate.HybridDao;
import com.pwc.wechat.dao.hr.IEmailCrud;
import com.pwc.wechat.entity.hr.Config;
import com.pwc.wechat.entity.hr.TalentData;
import com.pwc.wechat.entity.report.ReportColumn;
import com.pwc.wechat.entity.report.ReportConfig;
import com.pwc.wechat.service.hr.round.ITalentDataSetHandler;
import com.pwc.wechat.service.mail.IEmailHandler;
import com.pwc.wechat.util.JsonUtil;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebContext.class)
@WebAppConfiguration
public class ConfigTest {
	
	@Autowired
	private HybridDao hybridDao;
	@Autowired
	private IEmailCrud iEmailCrud;
	@Autowired
	private IEmailHandler imail;
	@Autowired
	private ITalentDataSetHandler iTalentDataSetHandler;
	@Test
	public void meiju(){
		for(ReportColumn ReportName:ReportColumn.values()){
			System.out.println("ReportName:"+ReportColumn.valueOf(ReportName.toString()).getColumnName());
		}
	}
	
	@Test
	public void testResources() throws IOException{
		Resource yaHeiFontResource = new ClassPathResource("/font/msyh.ttf");
		System.out.println(yaHeiFontResource.getFile().getAbsolutePath());
	}
	
	
	@Test
  public void uploadwwImage() throws IOException{
		Config config = imail.getEmailTemplate("STATUS_Q0");
    
    System.out.println("@@@"+config.getEmailContextTemplate());

	}
	

	@Test
	@Commit
	public void testinsert() {
		Config config = new Config();
		TalentData a = new TalentData();
		a.setAppId("1003");
		a.setDate("2012-01-01");
		config.setApplyNumber(a.getDate().replaceAll("-", "")+"-"+a.getAppId());
		config.setRoundMoveFlag("true");
		iTalentDataSetHandler.addmoveflag(config);
	}
	@Test
	@Commit
	public void testdelate() {
		Config config = new Config();
		config.setApplyNumber("20160707-1351");
		//config.setRoundMoveFlag("true");
		//config.setId(11);
		iTalentDataSetHandler.delmoveflag(iEmailCrud.findAllByApplyNumber(config.getApplyNumber()).stream().findFirst().orElse(null));
	}
	
}
