package com.pwc.wechat.service;



import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Commit;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.pwc.wechat.config.spring.WebContext;
import com.pwc.wechat.dao.hr.IUserCrud;
import com.pwc.wechat.entity.user.Avatar;
import com.pwc.wechat.entity.user.User;
import com.pwc.wechat.service.hr.user.HrmService;
import com.pwc.wechat.service.hr.user.IUserHandler;




@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebContext.class)
@WebAppConfiguration

public class AddNewHrTest {
	
	@Autowired
	private IUserHandler iUserHandler;
	
	@Autowired
	private IUserCrud iUserCrud;
	
	@Test
	@Commit
	@Transactional(readOnly = false)	
    public void uploadImage() throws IOException{
    Avatar avatar = iUserHandler.getAvatar("user");
    DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    
    System.out.println("@@@"+sdf.format(avatar.getTimeStamp()));

	}
	


}
