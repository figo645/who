package com.pwc.wechat.excel;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.junit.Test;

public class TestDocx {
	
	@Test
	public void test() throws IOException {
		Path docFile = Paths.get("D:/PrettyDocxFormat.docx");
		XWPFDocument document = new XWPFDocument(Files.newInputStream(docFile));
		document.getTables().forEach(table->{
			System.out.println("---table---");
			table.getRows().forEach(row->{
				System.out.println("---row---");
				row.getTableCells().forEach(cell->{
					System.out.println("---cell---");
					System.out.println(cell.getText());
				});
			});
		});
		document.close();
	}

}
