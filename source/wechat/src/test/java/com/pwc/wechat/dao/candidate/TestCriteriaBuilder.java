package com.pwc.wechat.dao.candidate;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.pwc.wechat.config.spring.WebContext;
import com.pwc.wechat.entity.candidate.Candidate;
import com.pwc.wechat.entity.candidate.EducationExperience;
import com.pwc.wechat.util.JsonUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebContext.class)
@WebAppConfiguration
public class TestCriteriaBuilder {
	
	@Autowired
	private LocalContainerEntityManagerFactoryBean entityManagerFactory;
	
	@Test
	public void testCriteriaBuilder() {
		this.search();
	}
	
	private void search() {
		CriteriaBuilder builder = entityManagerFactory.getNativeEntityManagerFactory().getCriteriaBuilder();
		CriteriaQuery<Candidate> query = builder.createQuery(Candidate.class);
		Root<Candidate> candidate = query.from(Candidate.class);
		Join<Candidate, EducationExperience> join = candidate.join(candidate.getModel().getSet("educationExperiences", EducationExperience.class), JoinType.LEFT);
//		join1.on(builder.and(builder.equal(join1.get("lastExp"), true), builder.equal(candidate.get("nationalId"), join1.get("nationalId"))));
		query.where(builder.equal(candidate.get("nationalId"), "1005277412"));
		
		EntityManager entityManager = entityManagerFactory.getNativeEntityManagerFactory().createEntityManager();
		Query q = entityManager.createQuery(query);
		List<Candidate> res = q.getResultList();
		res.forEach(object -> {
			System.out.println("======");
			System.out.println(JsonUtil.toNormalJson(object));
		});
	}

}
