package com.pwc.wechat.excel;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;

public class TestImportExcel {

	@Test
	public void test() throws IOException {
		Path xlsxFile = Paths.get("D:/talent.xlsx");
		XSSFWorkbook workbook = new XSSFWorkbook(Files.newInputStream(xlsxFile));
		System.out.println(workbook.getNumberOfSheets());
		System.out.println(workbook.getSheetAt(0).getRow(1).getCell(0));
		workbook.close();
	}
	
	@Test
	public void testExcel() throws IOException{
		Path xlsxFile = Paths.get("C:/Users/wwang238/Downloads/Talent Pool.xlsx");
		XSSFWorkbook workbook = new XSSFWorkbook(Files.newInputStream(xlsxFile));
		System.out.println(workbook.getSheetAt(0).getRow(4).getCell(22).getNumericCellValue());
		Cell cell = workbook.getSheetAt(0).getRow(4).getCell(7);
//		System.out.println("toString" + cell.toString());
//		System.out.println("getRichStringCellValue" + cell.getRichStringCellValue().getString());
		String no = workbook.getSheetAt(0).getRow(4).getCell(0).getStringCellValue();// 20150101-001
		String[] dateAndId = no.split("-");
//		System.out.println(LocalDate.parse(dateAndId[0], DateTimeFormatter.BASIC_ISO_DATE).toString());
		workbook.close();
	}
	

}
