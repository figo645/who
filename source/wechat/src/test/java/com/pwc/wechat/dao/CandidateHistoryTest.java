package com.pwc.wechat.dao;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Commit;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.pwc.wechat.config.spring.WebContext;
import com.pwc.wechat.entity.candidate.CandidateHistory;
import com.pwc.wechat.util.DateUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebContext.class)
@WebAppConfiguration
@Transactional
public class CandidateHistoryTest {
	

	@Commit
	@Test
	public void testInsert() {
		CandidateHistory his = new CandidateHistory();
		his.setNationalId("31010520160101000X");
		his.setHashCode(1234567);
		his.setContent("{\"bala\": \"bala\"}");
		his.setOptTime(DateUtil.getLocalDateTime());
	}
	
	@Commit
	@Test
	public void testUpdate() {
		CandidateHistory his = new CandidateHistory();
		his.setNationalId("31010520160101000X");
		his.setHashCode(7654321);
		his.setContent("{\"bala\": \"bala\"}");
		his.setOptTime(DateUtil.getLocalDateTime());
	}
	
	@Commit
	@Test
	public void testFind() {
		CandidateHistory his = new CandidateHistory();
		his.setNationalId("31010520160101000X");
	}

}
