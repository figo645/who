package com.pwc.wechat.service.java;

import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BinaryOperator;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;

import com.pwc.wechat.dao.candidate.HybridDao;
import com.pwc.wechat.entity.candidate.AppStatus;
import com.pwc.wechat.entity.candidate.ApplicationPk;
import com.pwc.wechat.entity.hr.MultiCondition;
import com.pwc.wechat.entity.report.ReportColumn;
import com.pwc.wechat.entity.report.ReportConfig;
import com.pwc.wechat.service.hr.report.SearchReport;
import com.pwc.wechat.util.DateUtil;
import com.pwc.wechat.util.JsonUtil;

public class CommonTest {

	@Test
	public void test() {
		String reString = ""+null;
		System.out.println(reString.length());
	}
	
	@Test
	public void testDatecompare(){
		System.out.println(DateUtil.isEarlier(DateUtil.plusDays("2016-02-06", 30), DateUtil.getLocalDate()));
	}
	
	@Test
	public void testStringArray(){
		String[] strings = null;
		System.out.println(strings == null || strings.length == 0);
	}
	
	@Test
	public void testStream(){
		String[] strings = {"a","b","c"};
		Arrays.stream(strings).forEach((String string)->{
			string = string + "x";
			System.out.println(string);
		});
		Arrays.stream(strings).forEach((String string)->{
			System.out.println(string);
		});
	}
	
	
	@Test
	public void testJsonReportConfig(){
		String json = "{\"title\":\"Report for Signature\",\"configs\":[\"CANDIDATE_NAME\",\"CANDIDATE_NATIONAL_ID\",\"APPLICATION_APPLY_ID\",\"EDUCATION_EXPERIENCE_UNIVERSITY\",\"WORK_EXPERIENCE_COMPANY\"]}";
		ReportConfig conf = JsonUtil.toNormalObject(json, ReportConfig.class);
		System.out.println(conf.getConfigs().length);
	}
	
	@Test
	public void testDate(){
		String date = "2014-12-31T16:00:00.000Z";
		String zonedDateTime = LocalDateTime.parse(date, DateTimeFormatter.ISO_DATE_TIME).atOffset(ZoneOffset.UTC).atZoneSameInstant(ZoneOffset.systemDefault()).toLocalDate().toString();
		System.out.println(zonedDateTime);
	}
	
	@Test
	public void testJson(){
		System.out.println(JsonUtil.toNormalJson(null));
	}

	@Test
	public void testList(){
		List<String> strings = new ArrayList<String>();
		String[] aStrings = new String[strings.size()];
		aStrings = strings.toArray(aStrings);
	}
	
	@Test
	public void testSubString(){
		String string = "123,123,";
		System.out.println(string.substring(0, string.lastIndexOf(",")));
	}
	
	@Test
	public void testDateString(){
		NumberFormat nf = NumberFormat.getInstance();  
        nf.setGroupingUsed(false);  
        nf.setMaximumIntegerDigits(2);  
        nf.setMinimumIntegerDigits(2);  
		String string = "2015-01-01";
		System.out.println(nf.format(LocalDate.parse(string).getMonthValue()) + "-" + nf.format(LocalDate.parse(string).getDayOfMonth()));
	}
	
	@Test
	public void testEnum(){
		String string = "[\"ID_NUMBER\", \"BLANK_INTERVIEW_TIME\", \"CANDIDATE_NAME\", \"EDUCATION_EXPERIENCE_UNIVERSITY\", \"EDUCATION_EXPERIENCE_MAJOR\", \"EDUCATION_EXPERIENCE_DEGREE\", \"BLANK_PROPOSED_DOMAIN\", \"BLANK_LEVEL\", \"BLANK_MARK\"]";
		ReportColumn[] reportColumns = JsonUtil.toNormalObject(string, ReportColumn[].class);
		System.out.println(reportColumns.length);
	}
	
	@Test
	public void testApplicationPk(){
		ApplicationPk a = new ApplicationPk();
		a.setApplyDate("123");
		a.setApplyId(123);
		ApplicationPk b = new ApplicationPk();
		b.setApplyDate("123");
		b.setApplyId(111);
		System.out.println(a.equals(b));
	}
	
	@Test
	public void testReduce(){
		String[] strings = {"aaa","bbb","ccc"};
		System.out.println(Arrays.stream(strings).reduce((String a, String b) -> a.concat(",").concat(b)).get());
	}
	
	@Test
	public void testLambda(){
		List<MultiCondition> conditions = new ArrayList<>();
		MultiCondition a = new MultiCondition();
		a.setId("a");
		a.setCheck(true);
		MultiCondition b = new MultiCondition();
		b.setId("b");
		b.setCheck(null);
		MultiCondition c = new MultiCondition();
		c.setId("c");
		c.setCheck(false);
		conditions.add(a);
		conditions.add(b);
		conditions.add(c);
		conditions.stream().filter(MultiCondition::getCheck).forEach(one->{
			System.out.println(one.getId());
		});
	}
	
}
