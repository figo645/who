package com.pwc.wechat.dao.candidate;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.test.annotation.Commit;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.pwc.wechat.config.spring.WebContext;
import com.pwc.wechat.entity.candidate.Candidate;
import com.pwc.wechat.entity.candidate.CandidateHistory;
import com.pwc.wechat.entity.candidate.EducationExperience;
import com.pwc.wechat.entity.candidate.Questionnaire;
import com.pwc.wechat.entity.candidate.WorkExperience;
import com.pwc.wechat.util.ApplicationContextUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebContext.class)
@WebAppConfiguration
public class TestNewDao {
	
	@Autowired
	private LocalContainerEntityManagerFactoryBean entityManagerFactory;
	
	@Autowired
	private IEducationExpCrud iEducationExpCrud;
	
	@Autowired
	private ICandidateCrud iCandidateCrud;
	
	@Autowired
	private IQuestionnaireCrud iQuestionnaireCrud;
	
	@Autowired
	private ICandidateHistoryCrud iCandidateHistoryCrud;
	
	@Test
	@Commit
	public void testSave() {
		EducationExperience educationExperience = new EducationExperience();
		educationExperience.setNationalId("310105101010100001");
		educationExperience.setExperienceId(3);
		EducationExperience saved = iEducationExpCrud.save(educationExperience);
		System.out.println("saved" + saved.getNationalId());
	}
	
	@Test
	@Rollback
	@Transactional(readOnly = true)
	public void iQuestionnaireCrud() {
		Questionnaire questionnaire = iQuestionnaireCrud.findAllByNationalIdOrderByIdDesc("310105199001070100").get(0);
		System.out.println(questionnaire.getContent());
	}
	
	@Test
	@Rollback
	@Transactional(readOnly = true)
	public void iCandidateHistoryCrud() {
		CandidateHistory candidateHistory = iCandidateHistoryCrud.findAllByNationalId("125211920").get(0);
		System.out.println(candidateHistory.getContent());
	}
	
	@Test
	@Commit
	@Transactional
	public void testSaveCan() {
		Candidate can = new Candidate();
		can.setNationalId("310105101010100099");
		can.setName("OwenChen");
		iCandidateCrud.save(can);
	}
	
	@Test
	@Commit
	public void testDelete() {
		iEducationExpCrud.deleteAll();
	}
	
	@Test
	public void testFindAll() {
		iEducationExpCrud.findAll().forEach((EducationExperience eduExp)->{
			System.out.println(eduExp.getNationalId());
		});
	}
	
	@Test
	public void testCriteriaBuilder() {
		this.search();
	}
	
	private void search() {
		CriteriaBuilder builder = entityManagerFactory.getNativeEntityManagerFactory().getCriteriaBuilder();
		CriteriaQuery<Candidate> candidateQuery = builder.createQuery(Candidate.class);
		Root<Candidate> root = candidateQuery.from(Candidate.class);
		Join<Candidate, WorkExperience> join1 = root.join(root.getModel().getDeclaredSingularAttribute("nationalId", WorkExperience.class), JoinType.LEFT).on();
		join1.on(builder.and(builder.equal(join1.get("lastExp"), true), builder.equal(root.get("nationalId"), join1.get("nationalId"))));
		candidateQuery.where(builder.equal(join1.get("nationalId"), "1005277412"));
		
		EntityManager entityManager = ApplicationContextUtil.creatEntityManager();
		Query query = entityManager.createQuery(candidateQuery);
		List res = query.getResultList();
		
	}

}
