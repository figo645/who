package com.pwc.wechat.service.java;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.pwc.wechat.config.constant.BaseConst;
import com.pwc.wechat.util.HttpUtil;

public class HttpRequestTest {

	@Test
	public void testHttpRequest() {
		try {
			Map<String, String> params = new HashMap<>();
			params.put("grant_type", BaseConst.WEIXIN_TOKEN_GRANT_TYPE);
			params.put("appid", BaseConst.WEIXIN_APP_ID);
			params.put("secret", BaseConst.WEIXIN_APP_SECRET);
			System.out.println(HttpUtil.sendHttpsGet(BaseConst.WEIXIN_TOKEN_URL, params));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
