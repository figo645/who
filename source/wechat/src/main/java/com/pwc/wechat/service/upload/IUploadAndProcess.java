package com.pwc.wechat.service.upload;

import javax.servlet.http.HttpServletRequest;

import org.springframework.transaction.annotation.Transactional;

public interface IUploadAndProcess<T> {
	
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public T getUploadFile(HttpServletRequest request, String... attrs);

}
