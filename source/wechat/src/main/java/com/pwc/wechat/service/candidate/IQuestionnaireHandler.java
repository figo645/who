package com.pwc.wechat.service.candidate;

import org.springframework.transaction.annotation.Transactional;

import com.pwc.wechat.entity.candidate.QuestAndAnswer;

public interface IQuestionnaireHandler {

	/**
	 * save questions and answers from candidate
	 * @param questAndAnswer
	 */
	@Transactional(readOnly = false, rollbackFor = Throwable.class)
	public void saveQuestAndAnswer(QuestAndAnswer questAndAnswer);

	/**
	 * search questions and deliver it to web page
	 * @param language
	 * @return
	 */
	@Transactional(readOnly = true, rollbackFor = Throwable.class)
	public QuestAndAnswer searchQuestAndAnswer(String language);

}
