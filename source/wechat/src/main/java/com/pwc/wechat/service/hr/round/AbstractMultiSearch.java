package com.pwc.wechat.service.hr.round;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.pwc.wechat.entity.candidate.AppResult;
import com.pwc.wechat.entity.candidate.AppStatus;
import com.pwc.wechat.entity.hr.MultiCondition;
import com.pwc.wechat.entity.hr.OtherCondition;
import com.pwc.wechat.entity.hr.TalentPage;
import com.pwc.wechat.entity.report.ReportColumn;
import com.pwc.wechat.entity.report.ReportConfig;

public abstract class AbstractMultiSearch<T, M extends MultiCondition> implements IMultiSearch<T, M> {
	
	//columns collection
	protected ReportColumn[] columns = this.initReportColumn();
	
	protected Map<String, String> statusMap = new HashMap<>();
	
	/*
	 * 整个流程图如下
	 * Status Process:
	 * 		{status:STATUS_NEW,	result:RESULT_UNKNOWN}
	 * 		{status:STATUS_Q1,	result:RESULT_UNKNOWN}
	 * 		{status:STATUS_Q2,	result:RESULT_UNKNOWN}
	 * 		{status:STATUS_Q3,	result:RESULT_UNKNOWN}
	 * 		{status:STATUS_Q4,	result:RESULT_PASS}
	 * 		{status:STATUS_Q5,	result:RESULT_PASS}
	 * Status Trash:
	 * 		{					result:RESULT_FAIL}
	 * 
	 */
	public AbstractMultiSearch() {
		statusMap.put(AppStatus.STATUS_NEW.name(), ReportColumn.APPLICATION_STATUS.getColumnData() + " = " + AppStatus.STATUS_NEW.ordinal() + " AND " + ReportColumn.APPLICATION_RESULT.getColumnData() + " = " + AppResult.RESULT_UNKNOWN.ordinal());
		statusMap.put(AppStatus.STATUS_Q1.name(), ReportColumn.APPLICATION_STATUS.getColumnData() + " = " + AppStatus.STATUS_Q1.ordinal() + " AND " + ReportColumn.APPLICATION_RESULT.getColumnData() + " = " + AppResult.RESULT_UNKNOWN.ordinal());
		statusMap.put(AppStatus.STATUS_Q2.name(), ReportColumn.APPLICATION_STATUS.getColumnData() + " = " + AppStatus.STATUS_Q2.ordinal() + " AND " + ReportColumn.APPLICATION_RESULT.getColumnData() + " = " + AppResult.RESULT_UNKNOWN.ordinal());
		statusMap.put(AppStatus.STATUS_Q3.name(), ReportColumn.APPLICATION_STATUS.getColumnData() + " = " + AppStatus.STATUS_Q3.ordinal() + " AND " + ReportColumn.APPLICATION_RESULT.getColumnData() + " = " + AppResult.RESULT_UNKNOWN.ordinal());
		statusMap.put(AppStatus.STATUS_Q4.name(), ReportColumn.APPLICATION_STATUS.getColumnData() + " = " + AppStatus.STATUS_Q4.ordinal() + " AND " + ReportColumn.APPLICATION_RESULT.getColumnData() + " = " + AppResult.RESULT_PASS.ordinal());
		statusMap.put(AppStatus.STATUS_Q5.name(), ReportColumn.APPLICATION_STATUS.getColumnData() + " = " + AppStatus.STATUS_Q5.ordinal() + " AND " + ReportColumn.APPLICATION_RESULT.getColumnData() + " = " + AppResult.RESULT_PASS.ordinal());
		statusMap.put("STATUS_TRASH", 																														ReportColumn.APPLICATION_RESULT.getColumnData() + " = " + AppResult.RESULT_FAIL.ordinal());
		statusMap.put("STATUS_ALL", 																														ReportColumn.APPLICATION_RESULT.getColumnData() + " != " + AppResult.RESULT_FAIL.ordinal());
	}
	
	@Override
	public T searchByMultiCondition(M multiCondition) {
		return this.executeSearch(this.readCondition(multiCondition));
	}
	
	abstract protected T executeSearch(ReportConfig reportConfig);

	abstract protected ReportColumn[] initReportColumn();
	
	protected ReportConfig readCondition(M multiCondition) {
		TalentPage talentPage = multiCondition.getOtherCondition().getTalentPage();
		ReportConfig reportConfig = new ReportConfig();
		reportConfig.setConditions(this.readMultiCondition(multiCondition));
		reportConfig.setConfigs(this.columns);
		reportConfig.setTalentPage(talentPage == null ? new TalentPage() : talentPage);
		return reportConfig;
	}
	
	protected String[] readMultiCondition(M multiCondition) {
		List<String> conditions = new ArrayList<String>();
		conditions.addAll(this.readCustomize(multiCondition));
		conditions.addAll(this.readOtherCondition(multiCondition.getOtherCondition()));//Process OtherCondition
		conditions.addAll(this.readSelections(multiCondition.getSelections()));
		return conditions.stream().filter(con -> con != null).toArray(String[]::new);
	}
	
	protected List<String> readCustomize(M multiCondition) {
		return new ArrayList<>();
	}
	
	protected List<String> readOtherCondition(OtherCondition otherCondition) {
		return new ArrayList<>();
	}
	
	private List<String> readSelections(List<MultiCondition> multiConditions) {
		return multiConditions.stream().map(this::readSelectedCondition).collect(Collectors.toList());
	}
	
	protected String readSelectedCondition(MultiCondition condition) {
		return null;
	}
	
	protected String between(Integer a, Integer b) {
		return this.betweenDate(a == null ? null : String.valueOf(a), b == null ? null : String.valueOf(b));
	}
	
	protected String betweenDate(String a, String b) {
		if (a != null && b != null) {
			return " BETWEEN '" + a + "' AND '" + b + "' ";
		} else if (a != null && b == null) {
			return " >= '" + a + "' ";
		} else if (a == null && b != null) {
			return " <= '" + b + "' ";
		} else {
			return null;
		}
	}
	
	protected String inCollection(String colName, MultiCondition condition) {
		StringBuilder collection = new StringBuilder();
		condition.getSelections().stream().filter(one -> one.getCheck() != null && one.getCheck()).forEach(one -> {
			collection.append(collection.length() == 0 ? "'" + one.getTitle() + "'" : ",'" + one.getTitle() + "'");
		});
		return collection.length() == 0 ? null : colName + " in (" + collection.toString() + ")";
	}
	
	protected String regexp(String colName, MultiCondition condition) {
		StringBuilder collection = new StringBuilder();
		condition.getSelections().stream().filter(one -> one.getCheck() != null && one.getCheck()).forEach(con -> {
			collection.append(collection.length() == 0 ? con.getTitle() : "|".concat(con.getTitle()));
		});
		return collection.length() == 0 ? null : colName + " REGEXP '" + collection.toString() + "'";
	}
	
}
