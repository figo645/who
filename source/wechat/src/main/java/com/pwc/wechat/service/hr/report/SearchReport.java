package com.pwc.wechat.service.hr.report;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pwc.wechat.dao.candidate.HybridDao;
import com.pwc.wechat.entity.hr.MultiCondition;
import com.pwc.wechat.entity.hr.OtherCondition;
import com.pwc.wechat.entity.report.Report;
import com.pwc.wechat.entity.report.ReportColumn;
import com.pwc.wechat.entity.report.ReportConditionSet;
import com.pwc.wechat.entity.report.ReportConfig;
import com.pwc.wechat.service.hr.round.AbstractMultiSearch;

@Service("searchReport")
public class SearchReport extends AbstractMultiSearch<Report, ReportConditionSet> {
	
	@Autowired
	private HybridDao hybridDao;
	
	@Override
	protected Report executeSearch(ReportConfig reportConfig) {
		ReportColumn[] columns = reportConfig.getConfigs();
		Report report = new Report();		
		report.setTitle(reportConfig.getTitle());
		report.setTableHead(Arrays.stream(columns).map(ReportColumn::getColumnName).toArray(String[]::new));
		report.setTableBody(hybridDao.querySqlWithJpa(columns, reportConfig.getConditions(), (sqlResult, rowNum) -> {
			String[] row = new String[columns.length];
			for (int i = 0; i < columns.length; i++) {
				row[i] = columns[i].equals(ReportColumn.ID_NUMBER) ? String.valueOf(rowNum + 1) : String.valueOf(sqlResult[i]);
			}
			return row;
		}));
		return report;
	}
	
	@Override
	protected ReportConfig readCondition(ReportConditionSet reportConditionSet) {
		ReportConfig reportConfig = new ReportConfig();
		reportConfig.setConditions(this.readMultiCondition(reportConditionSet));
		reportConfig.setTitle(reportConditionSet.getReportConfigs().get(0).getTitle());//if no ReportConfig is selected, give the first one
		reportConfig.setConfigs(reportConditionSet.getReportConfigs().get(0).getConfigs());
		reportConditionSet.getReportConfigs().stream().filter(one->one.getCheck() != null && one.getCheck()).forEach(one -> {
			reportConfig.setTitle(one.getTitle());
			reportConfig.setConfigs(one.getConfigs());
		});
		return reportConfig;
	}
	
	@Override
	protected List<String> readOtherCondition(OtherCondition otherCondition) {
		List<String> other = new ArrayList<>();
		Optional.ofNullable(this.between(otherCondition.getRound1()[0], otherCondition.getRound1()[1]))
				.ifPresent(round1 -> other.add(ReportColumn.APPLICATION_ROUND_1_SCORE.getColumnData() + round1));
		Optional.ofNullable(this.between(otherCondition.getRound2()[0], otherCondition.getRound2()[1]))
				.ifPresent(round2 -> other.add(ReportColumn.APPLICATION_ROUND_2_SCORE.getColumnData() + round2));
		Optional.ofNullable(this.between(otherCondition.getRound3()[0], otherCondition.getRound3()[1]))
				.ifPresent(round3 -> other.add(ReportColumn.APPLICATION_ROUND_3_SCORE.getColumnData() + round3));
		return other;
	}
	
	@Override
	protected String readSelectedCondition(MultiCondition status) {
		if (status.getCheck() != null && status.getCheck()) {
			return status.getId().equals("RESULT_FAIL") ? statusMap.get("STATUS_TRASH") : statusMap.get(status.getTitle());
		}
		return null;
	}

	@Override
	protected ReportColumn[] initReportColumn() {
		// column is dynamic so don't need this method
		return null;
	}
	
}
