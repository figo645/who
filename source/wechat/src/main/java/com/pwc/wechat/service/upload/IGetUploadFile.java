package com.pwc.wechat.service.upload;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

public interface IGetUploadFile<T> {
	
	/**
	 * get upload files and process
	 * return the result list 
	 * as the process method being offered: IMultipartFileHandler<T>
	 * @param request
	 * @param handler
	 * @return
	 */
	public List<T> getUploadFile(HttpServletRequest request, IMultipartFileHandler<T> handler);

}
