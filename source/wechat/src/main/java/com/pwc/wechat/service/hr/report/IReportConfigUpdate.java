package com.pwc.wechat.service.hr.report;

import org.springframework.transaction.annotation.Transactional;

import com.pwc.wechat.entity.report.ReportConfig;

public interface IReportConfigUpdate {
	
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void updateReportConfig(ReportConfig reportConfig);

}
