package com.pwc.wechat.entity.hr;

import java.util.List;

public class MultiCondition {

	private String id;
	private String title;
	private Boolean check;
	private List<MultiCondition> selections;
	private OtherCondition otherCondition;

	public OtherCondition getOtherCondition() {
		return otherCondition;
	}

	public void setOtherCondition(OtherCondition otherCondition) {
		this.otherCondition = otherCondition;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Boolean getCheck() {
		return check;
	}

	public void setCheck(Boolean check) {
		this.check = check;
	}

	public List<MultiCondition> getSelections() {
		return selections;
	}

	public void setSelections(List<MultiCondition> selections) {
		this.selections = selections;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
