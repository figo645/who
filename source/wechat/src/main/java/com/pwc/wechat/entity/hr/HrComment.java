package com.pwc.wechat.entity.hr;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "hr_comment", schema = "wechat")
public class HrComment implements Serializable {

	private static final long serialVersionUID = 1902109951924666002L;

	@Id
	@Column(name = "national_id")
	private String nationalId;

	@Column(name = "appearance")
	private Integer appearance;

	@Column(name = "demeanour")
	private Integer demeanour;

	@Column(name = "english")
	private Integer english;

	@Column(name = "offer")
	private Boolean offer;

	@Column(name = "postgraduate")
	private Boolean postgraduate;

	@Column(name = "interests")
	private String interests;

	@Column(name = "technical_skill")
	private String technicalSkill;

	@Column(name = "senior_courses")
	private String seniorCourses;

	@Column(name = "hometown")
	private String hometown;

	@Column(name = "earliest_entry")
	private String earliestEntry;

	@Column(name = "parent_attitude")
	private String parentAttitude;

	@Column(name = "comment_question")
	private String commentQuestion;

	public String getNationalId() {
		return nationalId;
	}

	public void setNationalId(String nationalId) {
		this.nationalId = nationalId;
	}

	public Integer getAppearance() {
		return appearance;
	}

	public void setAppearance(Integer appearance) {
		this.appearance = appearance;
	}

	public Integer getDemeanour() {
		return demeanour;
	}

	public void setDemeanour(Integer demeanour) {
		this.demeanour = demeanour;
	}

	public Integer getEnglish() {
		return english;
	}

	public void setEnglish(Integer english) {
		this.english = english;
	}

	public Boolean getOffer() {
		return offer;
	}

	public void setOffer(Boolean offer) {
		this.offer = offer;
	}

	public Boolean getPostgraduate() {
		return postgraduate;
	}

	public void setPostgraduate(Boolean postgraduate) {
		this.postgraduate = postgraduate;
	}

	public String getInterests() {
		return interests;
	}

	public void setInterests(String interests) {
		this.interests = interests;
	}

	public String getTechnicalSkill() {
		return technicalSkill;
	}

	public void setTechnicalSkill(String technicalSkill) {
		this.technicalSkill = technicalSkill;
	}

	public String getSeniorCourses() {
		return seniorCourses;
	}

	public void setSeniorCourses(String seniorCourses) {
		this.seniorCourses = seniorCourses;
	}

	public String getHometown() {
		return hometown;
	}

	public void setHometown(String hometown) {
		this.hometown = hometown;
	}

	public String getEarliestEntry() {
		return earliestEntry;
	}

	public void setEarliestEntry(String earliestEntry) {
		this.earliestEntry = earliestEntry;
	}

	public String getParentAttitude() {
		return parentAttitude;
	}

	public void setParentAttitude(String parentAttitude) {
		this.parentAttitude = parentAttitude;
	}

	public String getCommentQuestion() {
		return commentQuestion;
	}

	public void setCommentQuestion(String commentQuestion) {
		this.commentQuestion = commentQuestion;
	}

}
