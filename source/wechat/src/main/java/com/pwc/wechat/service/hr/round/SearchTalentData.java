package com.pwc.wechat.service.hr.round;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pwc.wechat.dao.candidate.HybridDao;
import com.pwc.wechat.entity.candidate.AppResult;
import com.pwc.wechat.entity.candidate.AppStatus;
import com.pwc.wechat.entity.hr.MultiCondition;
import com.pwc.wechat.entity.hr.OtherCondition;
import com.pwc.wechat.entity.hr.TalentData;
import com.pwc.wechat.entity.hr.TalentDataSet;
import com.pwc.wechat.entity.report.ReportColumn;
import com.pwc.wechat.entity.report.ReportConfig;
import com.pwc.wechat.util.CommonUtil;

@Service("searchTalentData")
public class SearchTalentData extends AbstractMultiSearch<TalentDataSet, MultiCondition> {
	
	@Autowired
	private HybridDao hybridDao;
	
	@Override
	protected ReportColumn[] initReportColumn() {
		ReportColumn[] columns = {ReportColumn.CANDIDATE_NATIONAL_ID, ReportColumn.APPLICATION_APPLY_DATE, ReportColumn.APPLICATION_APPLY_ID, ReportColumn.CANDIDATE_NAME, ReportColumn.CANDIDATE_CELL_PHONE, ReportColumn.EDUCATION_EXPERIENCE_DEGREE, ReportColumn.CANDIDATE_MAIL_ADDRESS, ReportColumn.APPLICATION_ROUND_1_SCORE, ReportColumn.APPLICATION_ROUND_2_SCORE, ReportColumn.APPLICATION_ROUND_3_SCORE, ReportColumn.APPLICATION_STATUS, ReportColumn.APPLICATION_RESULT};
		return columns;
	}
	
	@Override
	protected TalentDataSet executeSearch(ReportConfig reportConfig) {
		TalentDataSet dataSet = new TalentDataSet();
		reportConfig.getTalentPage().setDataTotal(hybridDao.getDataTotal(columns, reportConfig.getConditions()));
		dataSet.setTalentPage(reportConfig.getTalentPage());
		dataSet.setTalentDatas(hybridDao.querySqlWithJpa(columns, reportConfig.getConditions(), (sqlResult, rowNum) -> {
			TalentData talentData = new TalentData();
			talentData.setNationalId(String.valueOf(sqlResult[0]));
			talentData.setDate(String.valueOf(sqlResult[1]));
			talentData.setAppId(CommonUtil.format4Digits(Integer.valueOf(String.valueOf(sqlResult[2]))));
			talentData.setName(String.valueOf(sqlResult[3]));
			talentData.setPhone(String.valueOf(sqlResult[4]));
			talentData.setDegree(String.valueOf(sqlResult[5]));
			talentData.setEmail(String.valueOf(sqlResult[6]));
			talentData.setQ1(Integer.valueOf(String.valueOf(sqlResult[7])));
			talentData.setQ2(Integer.valueOf(String.valueOf(sqlResult[8])));
			talentData.setQ3(Integer.valueOf(String.valueOf(sqlResult[9])));
			talentData.setStatus(AppStatus.values()[Integer.valueOf(String.valueOf(sqlResult[10]))]);
			talentData.setResult(AppResult.values()[Integer.valueOf(String.valueOf(sqlResult[11]))]);
			return talentData;
		}, reportConfig.getTalentPage().getDataIndex(), reportConfig.getTalentPage().getPageLines()));
		dataSet.setTalentPage(reportConfig.getTalentPage());
		return dataSet;
	}
	
	@Override
	protected List<String> readCustomize(MultiCondition multiCondition) {
		ArrayList<String> readCustomize = new ArrayList<>();
		Optional.ofNullable(statusMap.get(multiCondition.getTitle())).ifPresent(readCustomize::add);
		return readCustomize;
	}
	
	@Override
	protected List<String> readOtherCondition(OtherCondition otherCondition) {
		List<String> other = new ArrayList<>();
		Optional.ofNullable(this.readArray(otherCondition.getUniversity()))
				.ifPresent(university -> other.add(ReportColumn.EDUCATION_EXPERIENCE_UNIVERSITY.getColumnData() + university));
		Optional.ofNullable(this.readArray(otherCondition.getMajor()))
				.ifPresent(major -> other.add(ReportColumn.EDUCATION_EXPERIENCE_MAJOR.getColumnData() + major));
		Optional.ofNullable(otherCondition.getCandidateName().isEmpty() ? null : otherCondition.getCandidateName())
				.ifPresent(candidateName -> other.add(ReportColumn.CANDIDATE_NAME.getColumnData() + " LIKE '%" + candidateName + "%'"));
		Optional.ofNullable(this.between(otherCondition.getRound1()[0], otherCondition.getRound1()[1]))
				.ifPresent(round1 -> other.add(ReportColumn.APPLICATION_ROUND_1_SCORE.getColumnData() + round1));
		Optional.ofNullable(this.between(otherCondition.getRound2()[0], otherCondition.getRound2()[1]))
				.ifPresent(round2 -> other.add(ReportColumn.APPLICATION_ROUND_2_SCORE.getColumnData() + round2));
		Optional.ofNullable(this.between(otherCondition.getRound3()[0], otherCondition.getRound3()[1]))
				.ifPresent(round3 -> other.add(ReportColumn.APPLICATION_ROUND_3_SCORE.getColumnData() + round3));
		Optional.ofNullable(this.betweenDate(otherCondition.getGraduateDate()[0], otherCondition.getGraduateDate()[1]))
				.ifPresent(graduateDate -> other.add(ReportColumn.CANDIDATE_GRADUATE_DATE.getColumnData() + graduateDate));
		return other;
	}
	
	@Override
	protected String readSelectedCondition(MultiCondition condition) {
		String conditonName = condition.getId();
		if (conditonName.equals("selectedPosition")) {
			return this.inCollection(ReportColumn.APPLICATION_APPLY_POSITION.getColumnData(), condition);
		} else if (conditonName.equals("availableDay")) {
			this.processSchedual(condition);
			return this.inCollection(ReportColumn.CANDIDATE_AVAILABLE_SCHEDULE.getColumnData(), condition);
		} else if (conditonName.equals("graduatePlan")) {
			return this.inCollection(ReportColumn.CANDIDATE_GRADUATE_PLAN.getColumnData(), condition);
		} else if (conditonName.equals("languageProficiency")) {
			return this.regexp(ReportColumn.CANDIDATE_LANGUAGE_LEVEL.getColumnData(), condition);
		} else if (conditonName.equals("gender")) {
			return this.inCollection(ReportColumn.CANDIDATE_GENDER.getColumnData(), condition);
		} else if (conditonName.equals("degree")) {
			return this.inCollection(ReportColumn.EDUCATION_EXPERIENCE_DEGREE.getColumnData(), condition);
		} else {
			return null;
		}
	}
	
	private String readArray(String[] array) {
		if (array[1] != null) {
			if (array[0].equals("其他")) {
				return " LIKE '%" + array[1] + "%'";
			} else {
				return " = '" + array[0] + "," + array[1] + "'";
			}
		} else if (array[0] != null) {
			return " LIKE '" + array[0] + ",%'";
		} else {
			return null;
		}
	}
	
	private void processSchedual (MultiCondition condition) {
		condition.getSelections().forEach(one -> one.setTitle(one.getId()));
	}
	
}
