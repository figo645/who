package com.pwc.wechat.entity.report;

import java.util.List;

public class Report {

	private String title;
	private String[] tableHead;
	private List<String[]> tableBody;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String[] getTableHead() {
		return tableHead;
	}

	public void setTableHead(String[] tableHead) {
		this.tableHead = tableHead;
	}

	public List<String[]> getTableBody() {
		return tableBody;
	}

	public void setTableBody(List<String[]> tableBody) {
		this.tableBody = tableBody;
	}

}
