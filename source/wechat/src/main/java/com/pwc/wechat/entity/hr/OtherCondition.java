package com.pwc.wechat.entity.hr;

public class OtherCondition {

	private String[] university;
	private String[] major;
	private String candidateName;
	private Integer[] round1;
	private Integer[] round2;
	private Integer[] round3;
	private String[] graduateDate;
	private TalentPage talentPage;

	public String getCandidateName() {
		return candidateName;
	}

	public String[] getGraduateDate() {
		return graduateDate;
	}

	public void setGraduateDate(String[] graduateDate) {
		this.graduateDate = graduateDate;
	}

	public void setCandidateName(String candidateName) {
		this.candidateName = candidateName;
	}

	public TalentPage getTalentPage() {
		return talentPage;
	}

	public void setTalentPage(TalentPage talentPage) {
		this.talentPage = talentPage;
	}

	public String[] getUniversity() {
		return university;
	}

	public void setUniversity(String[] university) {
		this.university = university;
	}

	public String[] getMajor() {
		return major;
	}

	public void setMajor(String[] major) {
		this.major = major;
	}

	public Integer[] getRound1() {
		return round1;
	}

	public void setRound1(Integer[] round1) {
		this.round1 = round1;
	}

	public Integer[] getRound2() {
		return round2;
	}

	public void setRound2(Integer[] round2) {
		this.round2 = round2;
	}

	public Integer[] getRound3() {
		return round3;
	}

	public void setRound3(Integer[] round3) {
		this.round3 = round3;
	}

}
