package com.pwc.wechat.dao.candidate;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pwc.wechat.entity.candidate.WorkExperience;
import com.pwc.wechat.entity.candidate.WorkExperiencePk;

public interface IWorkExpCrud extends JpaRepository<WorkExperience, WorkExperiencePk> {

	public List<WorkExperience> findAllByNationalId(String nationalId);
	
	public void deleteAllByNationalId(String nationalId);

}
