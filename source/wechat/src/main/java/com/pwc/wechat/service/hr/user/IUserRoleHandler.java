package com.pwc.wechat.service.hr.user;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.pwc.wechat.entity.user.UserRole;

public interface IUserRoleHandler {
	
	@Transactional(readOnly = true, rollbackFor = Throwable.class)
	public List<UserRole> searchUserRoles();

	@Transactional(readOnly = false, rollbackFor = Throwable.class)
	public void updateUserRole(UserRole userRole);
	
}
