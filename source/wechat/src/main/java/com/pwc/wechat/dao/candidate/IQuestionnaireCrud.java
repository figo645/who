package com.pwc.wechat.dao.candidate;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pwc.wechat.entity.candidate.Questionnaire;

public interface IQuestionnaireCrud extends JpaRepository<Questionnaire, Integer> {
	
	public List<Questionnaire> findAllByNationalIdOrderByIdDesc(String nationalId);

}
