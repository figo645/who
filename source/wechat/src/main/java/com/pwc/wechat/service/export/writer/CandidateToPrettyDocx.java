package com.pwc.wechat.service.export.writer;

import java.util.HashMap;
import java.util.Map;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pwc.wechat.dao.candidate.HybridDao;
import com.pwc.wechat.entity.candidate.Application;
import com.pwc.wechat.entity.candidate.Candidate;
import com.pwc.wechat.entity.candidate.QuestAndAnswer;
import com.pwc.wechat.entity.report.ReportColumn;

@Service("candidateToPrettyDocx")
public class CandidateToPrettyDocx extends AbstractToPrettyDocx<Candidate> {
	
	private Map<String, String> candidateMap = new HashMap<>();
	
	@Autowired
	private HybridDao hybridDao;
	
	@Override
	protected void generateDocx(XWPFDocument workbook, Candidate candidate, int index) {
		this.initCandidateMap(candidate);
		workbook.getTables().forEach(table -> {
			table.getRows().forEach(row -> {
				row.getTableCells().forEach(cell -> {
					String key = cell.getText().trim();
					if (candidateMap.containsKey(key)) {
						cell.removeParagraph(0);
						XWPFParagraph para = cell.addParagraph();
						XWPFRun run = para.createRun();
						run.setText(candidateMap.getOrDefault(key, " "));
					}
				});
			});
		});
		QuestAndAnswer questAndAnswer = hybridDao.findLatestQuestAndAnswer(candidate.getNationalId());
		questAndAnswer.getQuestAndAnswerList().forEach(one -> {
			this.addParagraph(workbook, " ");
			this.addParagraph(workbook, one.getQuestion());
			this.addParagraph(workbook, one.getAnswer());
		});
	}
	
	private void initCandidateMap(Candidate candidate) {
		candidateMap.clear();
		candidateMap.put(ReportColumn.CANDIDATE_NAME.name(), candidate.getName());
		candidateMap.put(ReportColumn.CANDIDATE_GENDER.name(), candidate.getGender());
		candidateMap.put(ReportColumn.CANDIDATE_CURRENT_PROVINCE.name(), candidate.getCurrentProvince());
		candidateMap.put(ReportColumn.CANDIDATE_BIRTH_PROVINCE.name(), candidate.getBirthProvince());
		candidateMap.put(ReportColumn.CANDIDATE_BIRTHDAY.name(), candidate.getBirthday());
		candidateMap.put(ReportColumn.CANDIDATE_PASSPORT_NUMBER.name(), candidate.getPassportNumber());
		candidateMap.put(ReportColumn.CANDIDATE_GRADUATE_PLAN.name(), candidate.getGraduatePlan());
		candidateMap.put(ReportColumn.CANDIDATE_AVAILABLE_SCHEDULE.name(), candidate.getAvailableSchedule());
		candidateMap.put(ReportColumn.CANDIDATE_LANGUAGE_LEVEL.name(), candidate.getLanguageLevel());
		candidateMap.put(ReportColumn.CANDIDATE_GRADUATE_DATE.name(), candidate.getGraduateDate());
		candidateMap.put(ReportColumn.CANDIDATE_NATIONALITY.name(), candidate.getNationality());
		candidateMap.put(ReportColumn.CANDIDATE_NATIONAL_ID.name(), candidate.getNationalId());
		candidateMap.put(ReportColumn.CANDIDATE_HOME_PHONE.name(), candidate.getHomePhone());
		candidateMap.put(ReportColumn.CANDIDATE_CELL_PHONE.name(), candidate.getCellPhone());
		candidateMap.put(ReportColumn.CANDIDATE_MAIL_ADDRESS.name(), candidate.getMailAddress());
		candidateMap.put(ReportColumn.CANDIDATE_PERSONAL_PAGE.name(), candidate.getPersonalPage());
		candidateMap.put(ReportColumn.CANDIDATE_SKILLS_DESCRIBE.name(), candidate.getSkillsDescribe());
		for (int i = 0; i < 2; i++) { // initial
			candidateMap.put(ReportColumn.EDUCATION_EXPERIENCE_UNIVERSITY.name() + i, "");
			candidateMap.put(ReportColumn.EDUCATION_EXPERIENCE_DEGREE.name() + i, "");
			candidateMap.put(ReportColumn.EDUCATION_EXPERIENCE_MAJOR.name() + i, "");
			candidateMap.put(ReportColumn.EDUCATION_EXPERIENCE_PERIOD.name() + i, "");
			candidateMap.put(ReportColumn.EDUCATION_EXPERIENCE_GPA.name() + i, "");
			candidateMap.put(ReportColumn.WORK_EXPERIENCE_COMPANY.name() + i, "");
			candidateMap.put(ReportColumn.WORK_EXPERIENCE_POSITION.name() + i, "");
			candidateMap.put(ReportColumn.WORK_EXPERIENCE_PERIOD.name() + i, "");
			candidateMap.put(ReportColumn.WORK_EXPERIENCE_DETAILS.name() + i, "");
		}
		if (candidate.getEducationExperiences() != null) {
			for (int i = 0; i < candidate.getEducationExperiences().size(); i++) {
				candidateMap.put(ReportColumn.EDUCATION_EXPERIENCE_UNIVERSITY.name() + i, candidate.getEducationExperiences().get(candidate.getEducationExperiences().size() - i - 1).getUniversity());
				candidateMap.put(ReportColumn.EDUCATION_EXPERIENCE_DEGREE.name() + i, candidate.getEducationExperiences().get(candidate.getEducationExperiences().size() - i - 1).getDegree());
				candidateMap.put(ReportColumn.EDUCATION_EXPERIENCE_MAJOR.name() + i, candidate.getEducationExperiences().get(candidate.getEducationExperiences().size() - i - 1).getMajor());
				candidateMap.put(ReportColumn.EDUCATION_EXPERIENCE_PERIOD.name() + i, candidate.getEducationExperiences().get(candidate.getEducationExperiences().size() - i - 1).getPeriod());
				candidateMap.put(ReportColumn.EDUCATION_EXPERIENCE_GPA.name() + i, candidate.getEducationExperiences().get(candidate.getEducationExperiences().size() - i - 1).getGpa());
			}
		}
		if (candidate.getWorkExperiences() != null) {
			for (int i = 0; i < candidate.getWorkExperiences().size(); i++) {
				candidateMap.put(ReportColumn.WORK_EXPERIENCE_COMPANY.name() + i, candidate.getWorkExperiences().get(candidate.getWorkExperiences().size() - i - 1).getCompany());
				candidateMap.put(ReportColumn.WORK_EXPERIENCE_POSITION.name() + i, candidate.getWorkExperiences().get(candidate.getWorkExperiences().size() - i - 1).getPosition());
				candidateMap.put(ReportColumn.WORK_EXPERIENCE_PERIOD.name() + i, candidate.getWorkExperiences().get(candidate.getWorkExperiences().size() - i - 1).getPeriod());
				candidateMap.put(ReportColumn.WORK_EXPERIENCE_DETAILS.name() + i, candidate.getWorkExperiences().get(candidate.getWorkExperiences().size() - i - 1).getDetails());
			}
		}
		Application application = hybridDao.findLatestApplication(candidate.getNationalId());
		candidateMap.put(ReportColumn.APPLICATION_APPLY_DATE.name(), application.getApplyDate());
		candidateMap.put(ReportColumn.APPLICATION_APPLY_ID.name(), String.valueOf(application.getApplyId()));
		candidateMap.put(ReportColumn.APPLICATION_APPLY_POSITION.name(), application.getApplyPosition());
		candidateMap.put(ReportColumn.APPLICATION_STATUS.name(), application.getStatus().name());
		candidateMap.put(ReportColumn.APPLICATION_RESULT.name(), application.getResult().name());
		candidateMap.put(ReportColumn.APPLICATION_ROUND_1_SCORE.name(), String.valueOf(application.getRound1Score()));
		candidateMap.put(ReportColumn.APPLICATION_ROUND_2_SCORE.name(), String.valueOf(application.getRound2Score()));
		candidateMap.put(ReportColumn.APPLICATION_ROUND_3_SCORE.name(), String.valueOf(application.getRound3Score()));
	}

}
