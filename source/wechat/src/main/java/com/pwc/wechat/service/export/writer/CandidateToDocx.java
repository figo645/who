package com.pwc.wechat.service.export.writer;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.stereotype.Service;

import com.pwc.wechat.entity.candidate.Candidate;

/**
 * 
 * generate CV file to the path
 * DOCX implement
 * 
 * @author OwenChen
 *
 */
@Service("candidateToDocx")
public class CandidateToDocx extends AbstractToDocx<Candidate> {

	@Override
	protected void generateDocx(XWPFDocument workbook, Candidate candidate, int index) {
		this.addParagraph(workbook, candidate.getName());
		this.addParagraph(workbook, candidate.getGender());
		this.addParagraph(workbook, candidate.getCurrentProvince());
		this.addParagraph(workbook, candidate.getNationality());
		this.addParagraph(workbook, "NI:" + candidate.getNationalId());
		this.addParagraph(workbook, " ");
		this.addParagraph(workbook, "Home phone:" + candidate.getHomePhone());
		this.addParagraph(workbook, "Cell phone:" + candidate.getCellPhone());
		this.addParagraph(workbook, "E-mail:" + candidate.getMailAddress());
		this.addParagraph(workbook, "URL:" + candidate.getPersonalPage());
		this.addParagraph(workbook, "Skill:" + candidate.getSkillsDescribe());
		this.addParagraph(workbook, " ");
		this.addParagraph(workbook, "Education experience:");
		if (candidate.getEducationExperiences() != null) {
			for (int i = 0; i < candidate.getEducationExperiences().size(); i++) {
				this.addParagraph(workbook, candidate.getEducationExperiences().get(i).getUniversity());
				this.addParagraph(workbook, "GPA:" + candidate.getEducationExperiences().get(i).getDegree());
				this.addParagraph(workbook, "Degree:" + candidate.getEducationExperiences().get(i).getMajor());
				this.addParagraph(workbook, "Period:" + candidate.getEducationExperiences().get(i).getPeriod());
				this.addParagraph(workbook, "GPA:" + candidate.getEducationExperiences().get(i).getGpa());
				this.addParagraph(workbook, " ");
			}
		}
		this.addParagraph(workbook, "Work experience:");
		if (candidate.getWorkExperiences() != null) {
			for (int i = 0; i < candidate.getWorkExperiences().size(); i++) {
				this.addParagraph(workbook, candidate.getWorkExperiences().get(i).getCompany());
				this.addParagraph(workbook, "Position:" + candidate.getWorkExperiences().get(i).getPosition());
				this.addParagraph(workbook, "Period:" + candidate.getWorkExperiences().get(i).getPeriod());
				this.addParagraph(workbook, "Details:" + candidate.getWorkExperiences().get(i).getDetails());
				this.addParagraph(workbook, " ");
			}
		}
	}
	

}
