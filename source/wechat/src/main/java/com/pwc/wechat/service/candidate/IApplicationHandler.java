package com.pwc.wechat.service.candidate;

import org.springframework.transaction.annotation.Transactional;

import com.pwc.wechat.entity.candidate.Application;

public interface IApplicationHandler {
	
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public boolean saveApplication(Application application);

}
