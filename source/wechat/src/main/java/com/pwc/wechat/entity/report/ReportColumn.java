package com.pwc.wechat.entity.report;

public enum ReportColumn {
	
	ID_NUMBER("No", "'myId'"),
	
	BLANK_SIGNATURE("Signature", "''"),
	BLANK_PROPOSED_DEPARTMENT("Proposed Department", "''"),
	BLANK_LEVEL("Level", "''"),
	BLANK_MARK("Mark", "''"),
	BLANK_INTERVIEW_TIME("Interview Time", "''"),
	
	CANDIDATE_NATIONAL_ID("National ID", "candidate.national_id"),
	CANDIDATE_NAME("Name", "candidate.name"),
	CANDIDATE_GENDER("Gender", "candidate.gender"),
	CANDIDATE_CURRENT_PROVINCE("Position", "candidate.current_province"),
	CANDIDATE_BIRTH_PROVINCE("Birth Place", "candidate.birth_province"),
	CANDIDATE_BIRTHDAY("Birth Day", "candidate.birthday"),
	CANDIDATE_NATIONALITY("Name", "candidate.nationality"),
	CANDIDATE_PASSPORT_NUMBER("Passport", "candidate.passport_number"),
	CANDIDATE_HOME_PHONE("Home Phone", "candidate.home_phone"),
	CANDIDATE_CELL_PHONE("Cell Phone", "candidate.cell_phone"),
	CANDIDATE_MAIL_ADDRESS("E-mail", "candidate.mail_address"),
	CANDIDATE_PERSONAL_PAGE("Personal Page", "candidate.personal_page"),
	CANDIDATE_SKILLS_DESCRIBE("Skills Describe", "candidate.skills_describe"),
	CANDIDATE_GRADUATE_PLAN("Graduate Plan", "candidate.graduate_plan"),
	CANDIDATE_AVAILABLE_SCHEDULE("Schedule", "candidate.available_schedule"),
	CANDIDATE_LANGUAGE_LEVEL("Language", "candidate.language_level"),
	CANDIDATE_GRADUATE_DATE("Graduate Date", "candidate.graduate_date"),
	
	WORK_EXPERIENCE_COMPANY("Company", "work_experience.company"),
	WORK_EXPERIENCE_POSITION("Position", "work_experience.position"),
	WORK_EXPERIENCE_PERIOD("Period", "work_experience.period"),
	WORK_EXPERIENCE_DETAILS("Details", "work_experience.details"),
	
	EDUCATION_EXPERIENCE_UNIVERSITY("University", "education_experience.university"),
	EDUCATION_EXPERIENCE_MAJOR("Major", "education_experience.major"),
	EDUCATION_EXPERIENCE_DEGREE("Degree", "education_experience.degree"),
	EDUCATION_EXPERIENCE_PERIOD("Period", "education_experience.period"),
	EDUCATION_EXPERIENCE_GPA("GPA", "education_experience.gpa"),
	
	APPLICATION_APPLY_DATE("Apply Date", "application.apply_date"),
	APPLICATION_APPLY_ID("Apply ID", "application.apply_id"),
	APPLICATION_APPLY_POSITION("Apply Position", "application.apply_position"),
	APPLICATION_STATUS("Status", "application.status"),
	APPLICATION_RESULT("Result", "application.result"),
	APPLICATION_ROUND_1_SCORE("Round 1", "application.round_1_score"),
	APPLICATION_ROUND_2_SCORE("Round 2", "application.round_2_score"),
	APPLICATION_ROUND_3_SCORE("Round 3", "application.round_3_score");
	
	private String columnName;
	private String columnData;
	
	private ReportColumn(String columnName, String columnData) {
		this.columnName = columnName;
		this.columnData = columnData;
	}

	public String getColumnName() {
		return columnName;
	}

	public String getColumnData() {
		return columnData;
	}

}
