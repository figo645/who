package com.pwc.wechat.service.candidate;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pwc.wechat.entity.candidate.Candidate;
import com.pwc.wechat.entity.candidate.EducationExperience;
import com.pwc.wechat.entity.candidate.QuestAndAnswer;
import com.pwc.wechat.entity.candidate.WorkExperience;
import com.pwc.wechat.util.CommonUtil;

@Service("insertCandidateTestData")
public class InsertCandidateTestData {
	
	@Autowired
	private ICandidateHandler candidateService;
	
	@Autowired
	private IQuestionnaireHandler questionnaireService;
	
	private String[] names = {"Aaron","Abbott","Abel","Abner","Abraham","Adair","Adam","Addison","Adolph","Adonis","Adrian","Ahern","Alan","Albert","Aldrich","Alexander","Alfred","Alger","Algernon","Allen","Alston","Alva","Alvin","Alvis","Amos","Andre","Andrew","Andy","Angelo","Augus","Ansel","Antony","Antoine 安托万 欧洲","Antonio 安东尼奥","Archer","Archibald","Aries","Arlen","Armand","Armstrong","Arno","Arnold","Arthur","Arvin","Asa","Ashburn 亚希伯恩","Atwood","Aubrey","August","Augustine","Avery","Baldwin 柏得温 条顿","Bancroft","Bard","Barlow","Barnett 巴奈特 英国","Baron","Barrette","Barry","Bartholomew","Bart","Barton","Bartley 巴特莱 英国","Basil","Bleacher 比其尔 英国","Beau","Beck","Ben","Benedict","Benjamin","Bennett 班奈特 拉丁","Benson","Berg","Berger","Bernard 格纳","Bernie","Bert","Breton","Bertram 柏特莱姆","Bevis","Bill","Bing","Bishop","Blair","Blake","Blithe","Bob","Booth","Borg","Boris","Bowen","Boyce","Boyd","Bradley 布拉德利","Brady","Brandon 布兰登 英国","Brian","Broderick","Brook","Bruce","Bruno","Buck","Burgess 伯骑士 英国","Burke","Burn ell 布尼尔 法国","Burton","Byron","Caesar","Calvin","Carey","Carl","Carr","Carter","Cash","Cecil","Cedric","Chad","Chinning","Chapman 契布曼 英国","Charles 查尔斯 拉丁－条顿","Chisel","Chester 切斯特 罗马","Christ","Christian","Christopher","Clare","Clarence","Clark","Claude","Clement 克雷孟特","Cleveland","Cliff","Clifford","Clyde","Colbert 考伯特 英国","Colby","Colin","Conrad","Corey","Cornelius","Cornell 康奈尔 法国","Craig","Curitis 柯蒂斯 法国","Cyril","Dana","Daniel","Darcy","Darnell","Darren","Dave","David","Dean","Dempsey 邓普斯 盖尔","Dennis","Derrick 戴里克 德国","Devin","Dick","Dominic 多米尼克","Don","Donahue 唐纳修 爱尔兰 红褐色的战士","Donald","Douglas 道格拉斯","Drew","Duke","Duncan","Dunn","Dwight","Dylan","Earl","Ed","Eden","Edgar","Edmund","Edison","Edward","Edwiin","Egbert","Eli","Elijah","Elliot","Ellis","Elmer","Elroy","Elton","Elvis","Emmanuel","Enoch","Eric","Ernest","Eugene","Evan","Everley 伊夫力 英国","Fabian","Felix","Ferdinand","Fitch","Fitzgerald","Ford","Francis 弗朗西斯","Frank","Franklin","Frederic","Gabriel 加布力尔","Gale","Gary","Gavin","Gene","Geoffrey","Geoff","George","Gerald","Gilbert 吉尔伯特 条顿","Giles","Glenn","Goddard 哥达","Godfery 高德佛里","Gordon","Greg","Gregary 格雷戈里","Griffith","Grover","Gustave 古斯塔夫","Guy","Page","Parker","Paddy","Patrick  拉丁","Paul","Payne","Perry","Pete","Peter","Phil","Philip","Porter","Prescott","Primo","Quentin 昆特","Quennel 昆尼尔 法国","Quincy","Quinn","Quintion","Rachel","Ralap","Randolph","Raymond ","Reg","Regan","Reginald","Reuben","Rex","Richard 理查德","Robert","Robin","Rock","Rod","Roderick","Rodney","Ron","Ronald","Rory","Roy","Rudolf","Rupert","Ryan","Sam","Sampson ","Samuel","Sandy","Saxon","Scott","Sean","Sebastian","Sid","Sidney","Silvester","Simon","Solomon 所罗门 希伯来 和平","Spencer 史宾杜 英国","Stan","Stanford","Stanley 史丹尼 英国","Steven","Stev","Steward 史都华德","Tab","Taylor","Ted","Ternence","Theobald","Theodore","Thomas","Tiffany 帝福尼 法国","Tim","Timothy 帝摩斯 希腊","Tobias","Toby","Todd","Tom","Tony","Tracy","Troy","Truman","Tyler","Tyrone","Ulysses 尤里西斯","Upton","Uriah","Valentine","Valentine","Verne","Vic","Victor","Vincent 文森","Virgil","Vito","Vivian","Wade","Walker","Walter","Ward","Warner","Wayne","Webb","Webster 韦伯斯特","Wendell 温德尔 条顿","Werner","Wilbur","Will","William 威廉","Willie","Winfred 威弗列德","Winston 温士顿 英国","Woodrow 伍德洛 挪威","Wordsworth","Wright","Wythe","Xavier","Yale","Yehudi","York","Yves","Zachary ","Zebulon","Ziv","Hale","Haley","Hamiltion","Hardy","Harlan","Harley","Harold","Harriet 哈丽雅特 英国","Harry","Harvey","Hayden","Heather 海拾兹 英国","Henry","Herbert 赫伯特 德国","Herman","Hilary","Hiram","Hobart","Hogan","Horace","Howar","Hubery","Hugh","Hugo","Humphrey","Hunter","Hyman ","Ian","Ingram","Ira","Isaac","Isidore 伊西多 希腊","Ivan","Ives","Jack","Jacob","Jared","Jason","Jay","Jeff","Jeffrey 杰弗里 法国","Jeremy","Jerome","Jerry","Jesse","Jim","Jo","John","Jonas","Jonathan","Joseph","Joshua","Joyce","Julian","Julius","Justin","Keith","Kelly","Ken","Kennedy","Kenneth","Kent","Kerr","Kevin","Kim","King","Kirk","Kyle","Lambert","Lance","Larry","Lawrence","Leif","Len","Lennon","Leo","Leonard 伦纳德 条顿","Leopold 利奥波德","Les","Lester","Levi","Lewis","Lionel","Lou","Louis","Lucien","Luther","Lyle","Lyndon","Lynn","Magee","Malcolm 麦尔肯 苏格兰 指传道者","Mandel","Marcus","Marico","Mark","Marlon","Marsh","Marshall","Martin","Marvin","Matt","Matthew 马休","Maurice 摩里斯 拉丁","Max","Maximilian","Maxwell 马克斯韦尔","Meredith","Merle","Michael 麦克","Michell 米契尔 英国","Mick","Mike","Miles","Milo","Monroe","Montague","Moore","Morgan","Mortimer","Morton","Moses","Murphy","Murray","Myron","Nat","Nathan","Nathaniel","Neil","Nelson","Newman","Nicholas","Nick","Nigel","Noah","Noel","Norman","Norton","Ogden","Oliver","Omar","orville 奥利尔 法国","Osborn","Oscar","Osmond","Oswald","Otis","Otto","Owen"};
	private String[] province = {"华盛顿","纽约","费城","班戈","罗利","梅肯","坦帕","多森","温莎","苏城","门罗","丹佛","夏延","海特","稀宾","图森","银城","钦利","金曼","奇科","雷丁","加里","麦凯","古丁","楠帕","尤金","坎顿","弗农","长滩波兰特","伍斯特","布法罗","泽西城","阿伦敦","匹兹堡","特伦顿","纽黑文","里土满","诺福克","夏洛特","底特律","扬斯敦","哥伦布","托莱多","韦恩堡","南本德","麦迪逊","芝加哥","温斯顿","塞勒姆","萨凡纳","奥兰多","莫比尔","迈阿密","杰克逊","孟菲斯","小石城","格林贝","圣任罗","托皮卡","塔尔萨","达拉斯","休斯敦","奥马哈","奥斯汀","博蒙特","拉伯克","格里利","比灵斯","华雷斯","卡斯珀","奥格登","盐湖城","普罗沃","博伊西","圣迭戈","蒂华纳","洛杉矶","奥克兰","伯克利","圣何塞","波特兰","亚基马","塔科马","西雅图","贝克斯","尤蒂卡","得梅因","威奇托","斯波坎哈特福德","奥尔巴尼","罗切斯特","宾厄姆顿","阿尔图纳","哈里斯堡","巴尔的摩","克利夫兰","辛辛那提","密尔沃基","圣路易斯","堪萨斯城","纳什维尔","查塔努加","彭萨科拉","塔拉哈西","亚特兰大","奥古斯塔","查尔斯顿","新奥尔良","达文波特","科罗拉多","普韦布洛","巴吞鲁曰","埃尔帕索","诺加莱斯","菲尼克斯","墨西卡利","帕萨迪纳","圣巴巴拉","斯托克顿","皮奥里亚","圣安吉洛","卡特班克普罗维登斯","杰克逊维尔","什里夫波特","圣安东尼奥","阿尔伯克基","锡达拉皮兹","拉斯维加斯","派恩布拉夫","路易斯维尔","萨克拉门托","阿库卢拉克","费尔班克斯"};
	private String[] gender = {"male","female"};
	private String[] graduatePlan = {"Go abroad","Civil servant","Graduate Studies","Work","Other"};
	private String[] nationality = {"China","USA","India"};
	private String[] languageLevel = {"CET-4","CET-6","TEM-4","TEM-6","TEM-8","JPT-1","JPT-2","JPT-3","Other"};
	private String[] applyPosition = {"Associate Software Engineer","Associate Testing Engineer","Associate Analyst","Associate UI Designer","PQA Intern","Admin Intern","HR Intern","Finance Intern"};
	private String[] degree = {"Bachelor","Master","Doctor"};
	private String[] major = {"哲学,哲学类","经济学,经济学类","管理学,管理科学与工程类","管理学,工商管理类","管理学,行政管理、公共管理类","管理学,图书档案学类","文学,语言文学类","文学,新闻传播学类","文学,艺术类","工学,电气信息类","工学,计算机科学与技术类","工学,机械类","工学,土建类","工学,生物医学工程类","工学,仪器仪表类","工学,能源动力类","工学,水利类","工学,材料类","工学,制药工程类","工学,交通运输类","工学,船舶与海洋工程类","工学,测绘类","工学,轻工纺织食品类","工学,武器类","工学,公安技术类","工学,航空航天类","历史学,历史学类","法学,法学类","理学,数学类","理学,物理学类","理学,化学类及化学工程类","理学,生物科学及生物技术类","理学,天文地质地理类","理学,力学类","理学,电子信息科学类","理学,系统科学类","理学,环境科学与安全类","教育学,教育学类","医学,医学类","理学,心理学类","农学,农业类"};
	private String[] company = {"JiadiMirrorCo.,Ltd.","佳迪镜业有限公司","JiahuiCast-Iron&Sanitary-WareCo.,Ltd.","嘉辉卫浴铸造有限公司","JialeGlasswareManufactoryCo.,Ltd.(OuerSanitarywares)","东莞市家乐玻璃制品有限公司欧尔卫浴","JodenInc.","乔登卫浴有限公司","JodenInc.-ShanghaiOffice","乔登卫浴有限公司上海办事处","HaerbinSayyasWindowsCo.,Ltd.","哈尔滨森鹰窗业股份有限公司","HainingHainingchaoDoorCo.,Ltd.","海宁市海宁潮门业有限公司","HanddierCo.,Ltd.","汉迪科莱福特有限公司","HangzhouBirchwoodsLeisureProductsCo.,Ltd.","杭州白桦林休闲用品有限公司","HangzhouCleanDellSanitaryWareCo.,Ltd.","杭州康利达卫浴洁具有限公司","HangzhouGaodegaoSanitarywareCo.,Ltd.","杭州高得高洁具有限公司","HangzhouHeheGlassInduatrialCo.,Ltd.","杭州和合玻璃工业有限公司","HangzhouHiland","杭州海兰科技有限公司","HangzhouHuangguanSanitarywareCo.,Ltd.","杭州皇冠卫浴洁具有限公司","HangzhouHuashenMirrorCo.,Ltd.","杭州华申镜业有限公司","HangzhouIdealityGarageDoorsFactory","杭州理想自动门厂","HangzhouJietaiKitchen&SanitaryEquipmentCo.,Ltd.-ShengzhouBranch","杭州洁太厨卫设备有限公司嵊州分公司","HangzhouJindiFurniture&DecortionCo.,Ltd.","杭州金迪家私装饰有限公司","HangzhouJiudaIndustrial&TradeCo.,Ltd.","杭州久达工贸有限公司","HangzhouYilangHygienismboshCleantoolCo.,Ltd.","杭州镱朗卫浴洁具有限公司","HangzhouYingbuStairCo.,Ltd.","杭州盈步楼梯有限公司","HangzhouYiyiHardwareCo.,Ltd.","杭州依依五金有限公司","HangzhouYuxingElectricalSanitarywareCo.,Ltd.","杭州裕兴电器卫浴有限公司","HansaMetallwerkeAg","德国汉莎龙头公司","HansgroheAg","德国汉斯格雅股份公司","HansgroheAG-BeijingRep.Office","德国汉斯格雅有限公司北京代表处","HansgroheAG-GuangzhouRep.Office","德国汉斯格雅有限公司广州代表处","HansgroheAG-ShanghaiRep.Office","德国汉斯格雅股份公司上海代表处","HansgroheSanitaryProducts(Shanghai)Co.,Ltd.","汉斯格雅卫浴产品上海有限公司","HanyoungInternationalCo.","韩荣国际技术公司","HarbinGoldenRichFurniture&DecorationCo.,Ltd.","哈尔滨金豪家具有限公司","HasconEngineeringSrl","意大利海斯康公司","HebeiShuleiCo.,Ltd.","河北舒蕾股份有限公司","HebeiShuleiCo.,Ltd.-ShanghaiOffice","河北舒蕾股份有限公司上海办事处","HedingMirrorIndustryCo.,Ltd.","合鼎镜业有限公司","Hengguo(Xiamen)MachineryCo.,Ltd.-ShanghaiBranch","亨国厦门精机工业有限公司上海分公司","HeraeusKulzerDentalLtd.","贺利氏古莎齿科有限公司","HeraeusKulzerGmbH&Co.KG","贺利氏古莎齿科有限公司","HeshanKangliyuanSanitaryWareIndustryCo.,Ltd.","鹤山市康立源卫浴实业有限公司","HeshanKangliyuanSanitaryWareIndustryCo.,Ltd.-GuangzhouSalesCenter","鹤山市康立源卫浴实业有限公司广州营销中心","HighTechDesignProductsAg","德国佛拉公司","Hi-TekTradingCompany","凯达贸易公司","HangzhouLiduTechnologyGlassCo.,Ltd.","杭州丽都工艺玻璃有限公司","HangzhouOnbayElectricApplianceCo.,Ltd.","杭州昂拜电器有限公司","HangzhouOudiyaSanitaryWaresCo.,Ltd.","杭州欧帝亚卫浴有限公司","HangzhouOulangSanitarywareEquipmentCo.,Ltd.","杭州欧浪卫浴设备有限公司","HangzhouOuyiluSanitarywareCo.,Ltd.","杭州欧伊露卫浴洁具有限公司","HangzhouShuangdaSanitarywareCo.,Ltd.","杭州双达洁具有限公司","HangzhouShuanghuaAutomaticControlsDoorsCo.,Ltd.","杭州双华自控门业有限公司","Hormann(Beijing)DoorProductionCo.,Ltd.-ShanghaiOffice","霍曼北京门业有限公司上海办事处","HoySunEnterpriseCo.,Ltd.","台湾和易欣兴业有限公司","HuberSpa","意大利琥波公司","Hufcor(HongKong)Co.,Ltd.","赫福高香港有限公司","HufcorPartition&DecorationProducts(Shanghai)Co.,Ltd.","赫福高隔断装饰制品上海有限公司","HulinMetalProducts(Suzhou)Co.,Ltd.","湖林金属制品苏州有限公司","HumeCemboardIndustriesCo.,Ltd.-ShanghaiRepresentativeOffice","马来西亚谦洋灰板工程有限公司上海代表处","HunanShinilionScience&TechnologyCo.,Ltd.","湖南湘联科技有限公司","HunanShinilionScience&TechnologyCo.,Ltd.-ShanghaiOffice","湖南湘联科技有限公司华东办事处","HuppeGmbH&Co.KG,OHG","德国虎玻公司","HuzhouYueqiuMortorCo.,Ltd.","湖州越球电机有限公司","HuzhouYueqiuMortorCo.,Ltd.-ShanghaiOffice","湖州越球电机有限公司上海办事处","IMIIndoorClimateTrading(Shanghai)Co.,Ltd.","埃迈贸易上海有限公司","Imposing(Shanghai)GlassIvationCo.,Ltd.","丽堂上海玻璃有限公司","InfotheMediaGroup","韩国信息媒体集团","Interbath(Shanghai)CeramicsCo.,Ltd.","英特贝斯上海陶瓷有限公司","InternationalInformation","InternationalWoodIndustry","《国际木业》编辑部","IntertekTestingServices(Guangzhou)Ltd.","HangzhouSunlightMirrorCo.,Ltd.","中外合资杭州桑莱特镜业有限公司","HangzhouXinliTradeCo.,Ltd.","杭州新丽工贸有限公司","HongKongHanghinghong(International)TradingCo.,Ltd.","香港恒兴行国际贸易有限公司","HongkongZhenhuaIndustrial(International)Co.,Ltd.","香港振华实业国际有限公司","HongkongZhenhuaIndustrial(International)Co.,Ltd.-Showroom","香港振华实业国际有限公司展厅","HongkongZhenhuaIndustry(International)Co.,Ltd.","香港振华实业国际有限公司","HongyidaIndustryCo.,Ltd.","上海,上海宏艺达建材有限公司","HonyGlassFactoryCo.,Ltd.","宏益玻璃厂股份有限公司","JiangmenMorrisSanitaryWareCo.,Ltd.","江门摩立斯洁具有限公司","JiangmenXinhuiJiehePlumbingFittingsCo.,Ltd.","江门市新会区洁和水暖器材有限公司","JiangsuJingjiangCeramicFactory","江苏省京江陶瓷厂","JiangxiBaishengDoorControlEquipmentsCo.,Ltd.","江西百胜门控设备有限公司","JiangyinBetterAutodoorCo.,Ltd.","江阴贝特自动门制造有限公司","JiejiaIndustryJoint-StockCo.,Ltd.","洁佳实业股份有限公司","jieju.com","中华洁具网","JinLiYiHardwareEquipmentCo.,Ltd.","深圳市金利怡五金制品有限公司","JingMeiIndustrialLtd.","精美工业有限公司","JinnisiCeramicSanitaryWaresCo.,Ltd.","金尼斯陶瓷洁具有限公司","JinxinHardwareManufacturingCo.,Ltd.","金鑫五金制造有限公司","JinxinHardwareManufacturingCo.,Ltd.-ShanghaiBranch","金鑫五金制造有限公司上海公司","JiuhMenEnterprisesCo.,Ltd.","钜门企业股份有限公司","JiuhMenEnterprisesCo.,Ltd.","精博钜门企业股有限公司","JiujiangCaiyiHardwareCo.,Ltd.","九江市财怡五金制品有限公司","HonyGlassFactoryCo.,Ltd.-ReifangFactory","宏益玻璃厂股份有限公司瑞芳厂","Hormann(Beijing)DoorProductionCo.,Ltd.","霍曼北京门业有限公司","天祥广州技术服务有限公司","Intradin(Ningbo)HardwareCo.,Ltd.","宁波九龙五金有限公司","J.CIndustrial","韩国JC工业","JeilWallInteriorTechCo.,Ltd.","韩国活动隔墙板技术有限公司","JetAirS.R.L.","意大利捷亚公司","JetWinIndustriesInc.","世引国际有限公司","JointstarLtd.","上海,上海仲星建材有限公司"};
	private String[] position = {"首席技术执行官 CTO/VP Engineering","技术总监/经理 Technical Director/Manager","信息技术经理 IT Manager","信息技术主管 IT Supervisor","信息技术专员 IT Specialist","项目经理/主管 Project Manager/Supervisor","项目执行/协调人员 Project Specialist / Coordinator","系统分析员 System Analyst","高级软件工程师 Senior Software Engineer","软件工程师 Software Engineer","系统工程师 System Engineer","高级硬件工程师 Senior Hardware Engineer","硬件工程师 Hardware Engineer","通信技术工程师 Communications Engineer","ERP技术/应用顾问 ERP Technical/Application Consultant","数据库工程师 Database Engineer","技术支持经理 Technical Support Manager","技术支持工程师 Technical Support Engineer","品质经理 QA Manager","信息安全工程师 Information Security Engineer","软件测试工程师 Software QA Engineer","硬件测试工程师 Hardware QA Engineer","测试员 Test Engineer","网站营运经理/主管 Web Operations Manager/Supervisor","网络工程师 Network Engineer","系统管理员/网管 System Manager/Webmaster","网页设计/制作 Web Designer/Production","技术文员/助理 Technical Clerk/Assistant"};
	private String[] university = {"上海,上海中医药大学","上海,上海交通大学","上海,上海商学院","上海,上海大学","上海,上海对外经贸大学","上海,上海工程技术大学","上海,上海师范大学","上海,上海应用技术大学","上海,上海政法学院","上海,上海海事大学","上海,上海海关学院","上海,上海海洋大学","上海,上海理工大学","上海,上海电力学院","上海,上海电机学院","上海,上海立信会计学院","上海,上海第二工业大学","上海,上海财经大学","上海,上海金融学院","上海,东华大学","上海,华东师范大学","上海,华东政法大学","上海,华东理工大学","上海,同济大学","上海,复旦大学","上海,上海纽约大学","大连,东北财经大学","大连,大连海事大学","大连,大连理工大学","大连,大连外国语大学","大连,大连交通大学","南京,东南大学","南京,中国药科大学","南京,南京中医院大学","南京,南京信息工程大学","南京,南京农业大学","南京,南京医科大学","南京,南京大学","南京,南京审计学院","南京,南京工业大学","南京,南京师范大学","南京,南京林业大学","南京,南京航空航天大学","南京,南京财经大学","南京,南京邮电大学","南京,河海大学","苏州,苏州大学","苏州,西交利物浦大学","杭州,中国美术学院","杭州,中国计量学院","杭州,杭州师范大学","杭州,杭州电子科技大学","杭州,浙江大学","杭州,浙江工业大学","杭州,浙江工商大学","杭州,浙江理工大学","武汉,中南民族大学","武汉,中南财经政法大学","武汉,中国地质大学(武汉)","武汉,华中农业大学","武汉,华中师范大学","武汉,华中科技大学","武汉,武汉大学","武汉,武汉工程大学","武汉,武汉理工大学","武汉,武汉科技大学","湖北,湖北中医药大学","湖北,湖北大学","湖北,湖北工业大学","宁波,宁波大学","宁波,宁波诺丁汉大学"};

	public String insert(int num) {
		for (int i = 0; i < num; i++) {
			Candidate candidate = new Candidate();
			candidate.setName(this.randomObjFromArray(names));
			String nationalId = this.randomNumber(99999999999L);
			candidate.setNationalId(nationalId);
			candidate.setAvailableSchedule(this.randomNumber(6L));
			candidate.setBirthday("1992-02-02");
			candidate.setBirthProvince(this.randomObjFromArray(province));
			candidate.setCellPhone(this.randomNumber(99999999L));
			candidate.setCurrentProvince(this.randomObjFromArray(province));
			candidate.setGender(this.randomObjFromArray(gender));
			candidate.setHomePhone(this.randomNumber(99999999L));
			candidate.setLanguageLevel(this.randomObjFromArray(languageLevel));
			candidate.setGraduatePlan(this.randomObjFromArray(graduatePlan));
			candidate.setMailAddress(this.randomNumber(99999L) + "@qq.com");
			candidate.setNationality(this.randomObjFromArray(nationality));
			candidate.setPassportNumber(this.randomNumber(999999L));
			candidate.setPersonalPage("qzone.com/" + this.randomNumber(99999L));
			candidate.setSkillsDescribe(this.getRandomString(10));
			candidate.setWechatUid(this.getRandomString(8));
			candidate.setEducationExperiences(this.randomEduExp(this.randomNum(5) + 1));
			candidate.setGraduateDate("2016-0" + (this.randomNum(8) + 1) + "-01");
			candidate.setWorkExperiences(this.randomWorkExp(this.randomNum(5) + 1));
			candidateService.saveCandidateFromPage(candidate, this.randomObjFromArray(applyPosition));
			QuestAndAnswer questAndAnswer = questionnaireService.searchQuestAndAnswer("chinese");
			questAndAnswer.setAnswer(nationalId);
			questAndAnswer.getQuestAndAnswerList().forEach(one->one.setAnswer(this.getRandomString(20)));
			questionnaireService.saveQuestAndAnswer(questAndAnswer);
		}
		return "success toal:" + num;
	}
	
	private List<EducationExperience> randomEduExp (int num) {
		List<EducationExperience> educationExperiences = new ArrayList<>(num);
		for (int i = 0; i < num; i++) {
			EducationExperience edu = new EducationExperience();
			edu.setDegree(this.randomObjFromArray(degree));
			edu.setGpa(String.valueOf(this.randomNum(5)));
			edu.setMajor(this.randomObjFromArray(major));
			edu.setPeriod(this.randomNumber(9999L));
			edu.setUniversity(this.randomObjFromArray(university));
			educationExperiences.add(edu);
		}
		return educationExperiences;
	}
	
	private List<WorkExperience> randomWorkExp (int num) {
		List<WorkExperience> workExperiences = new ArrayList<>(num);
		for (int i = 0; i < num; i++) {
			WorkExperience work = new WorkExperience();
			work.setCompany(this.randomObjFromArray(company));
			work.setDetails(this.getRandomString(30));
			work.setPeriod(this.randomNumber(9999L));
			work.setPosition(this.randomObjFromArray(position));
			workExperiences.add(work);
		}
		return workExperiences;
	}
	
	private String randomNumber(Long num) {
		return String.valueOf(CommonUtil.random().nextInt(num.intValue()));
	}
	
	private <T> T randomObjFromArray (T[] objs) {
		return objs[CommonUtil.random().nextInt(objs.length)];
	}
	
	private int randomNum (int max) {
		return CommonUtil.random().nextInt(max);
	}
	
	private String getRandomString(int length) {
		String str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < length; ++i) {
			int number = CommonUtil.random().nextInt(62);// [0,62)
			sb.append(str.charAt(number));
		}
		return sb.toString();
	} 

}
