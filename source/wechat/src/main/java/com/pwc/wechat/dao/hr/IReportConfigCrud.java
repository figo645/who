package com.pwc.wechat.dao.hr;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pwc.wechat.entity.report.ReportConfig;

public interface IReportConfigCrud extends JpaRepository<ReportConfig, Integer> {
	
}
