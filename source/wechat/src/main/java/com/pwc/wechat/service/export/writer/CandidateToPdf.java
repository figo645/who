package com.pwc.wechat.service.export.writer;

import java.io.IOException;

import org.springframework.stereotype.Service;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.pwc.wechat.entity.candidate.Candidate;

/**
 * 
 * generate CV file to the path
 * PDF implement
 * 
 * @author OwenChen
 *
 */
@Service("candidateToPdf")
public class CandidateToPdf extends AbstractToPdf<Candidate> {
	
	public CandidateToPdf() throws DocumentException, IOException {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void generatePdf(Document pdfDocument, Candidate candidate) throws DocumentException {
		this.addParagraph(pdfDocument, candidate.getName());
		this.addParagraph(pdfDocument, candidate.getGender());
		this.addParagraph(pdfDocument, candidate.getCurrentProvince());
		this.addParagraph(pdfDocument, candidate.getNationality());
		this.addParagraph(pdfDocument, "NI:" + candidate.getNationalId());
		this.addParagraph(pdfDocument, " ");
		this.addParagraph(pdfDocument, "Home phone:" + candidate.getHomePhone());
		this.addParagraph(pdfDocument, "Cell phone:" + candidate.getCellPhone());
		this.addParagraph(pdfDocument, "E-mail:" + candidate.getMailAddress());
		this.addParagraph(pdfDocument, "URL:" + candidate.getPersonalPage());
		this.addParagraph(pdfDocument, "Skill:" + candidate.getSkillsDescribe());
		this.addParagraph(pdfDocument, " ");
		this.addParagraph(pdfDocument, "Education experience:");
		if (candidate.getEducationExperiences() != null) {
			for (int i = 0; i < candidate.getEducationExperiences().size(); i++) {
				this.addParagraph(pdfDocument, candidate.getEducationExperiences().get(i).getUniversity());
				this.addParagraph(pdfDocument, "GPA:" + candidate.getEducationExperiences().get(i).getDegree());
				this.addParagraph(pdfDocument, "Degree:" + candidate.getEducationExperiences().get(i).getMajor());
				this.addParagraph(pdfDocument, "Period:" + candidate.getEducationExperiences().get(i).getPeriod());
				this.addParagraph(pdfDocument, "GPA:" + candidate.getEducationExperiences().get(i).getGpa());
				this.addParagraph(pdfDocument, " ");
			}
		}
		this.addParagraph(pdfDocument, "Work experience:");
		if (candidate.getWorkExperiences() != null) {
			for (int i = 0; i < candidate.getWorkExperiences().size(); i++) {
				this.addParagraph(pdfDocument, candidate.getWorkExperiences().get(i).getCompany());
				this.addParagraph(pdfDocument, "Position:" + candidate.getWorkExperiences().get(i).getPosition());
				this.addParagraph(pdfDocument, "Period:" + candidate.getWorkExperiences().get(i).getPeriod());
				this.addParagraph(pdfDocument, "Details:" + candidate.getWorkExperiences().get(i).getDetails());
				this.addParagraph(pdfDocument, " ");
			}
		}
	}

}
