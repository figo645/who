package com.pwc.wechat.service.hr.round;

import org.springframework.transaction.annotation.Transactional;

import com.pwc.wechat.entity.hr.TalentData;

public interface ITalentDataHandler {
	
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public String moveToTrash(TalentData talentData);
	
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public String recoverFromTrash(TalentData talentData);

}
