package com.pwc.wechat.dao.candidate;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pwc.wechat.entity.candidate.Candidate;

public interface ICandidateCrud extends JpaRepository<Candidate, String> {
	
	public Candidate findOneByWechatUid(String wechatUid);

}
