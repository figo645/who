package com.pwc.wechat.controller.hr;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pwc.wechat.service.export.IExportByNationalIds;
import com.pwc.wechat.service.upload.IUploadAndProcess;

@RestController
public class ExportResumeController {
	
	@Autowired
	private IExportByNationalIds exportZipAsCandidate;
	
	@Autowired
	private IExportByNationalIds exportZipAsPrettyCandidate;
	
	@Autowired
	private IExportByNationalIds exportZipAsQuestAndAnswer;
	
	@Autowired
	private IExportByNationalIds exportFileAsArray;
	
	@Autowired
	private IUploadAndProcess<String> importFromExcel;
	
	@RequestMapping(value = "/exportCandidateZip", produces = "text/html; charset=utf-8")
	public void exportCandidateZip(@RequestParam String[] nationalIds, HttpServletResponse response) throws Exception {
		exportZipAsCandidate.executeFlush(nationalIds, response);
	}
	
	@RequestMapping(value = "/exportPrettyCandidateZip", produces = "text/html; charset=utf-8")
	public void exportPrettyCandidateZip(@RequestParam String[] nationalIds, HttpServletResponse response) throws Exception {
		exportZipAsPrettyCandidate.executeFlush(nationalIds, response);
	}
	
	@RequestMapping(value = "/exportQuestionnaireZip", produces = "text/html; charset=utf-8")
	public void exportQuestionnaireZip(@RequestParam String[] nationalIds, HttpServletResponse response) throws Exception {
		exportZipAsQuestAndAnswer.executeFlush(nationalIds, response);
	}
	
	@RequestMapping(value = "/exportExcel", produces = "text/html; charset=utf-8")
	public void exportExcel(@RequestParam String[] nationalIds, HttpServletResponse response) throws Exception {
		exportFileAsArray.executeFlush(nationalIds, response);
	}

	@RequestMapping(value = "/uploadExcel", produces = "text/html; charset=utf-8")
	public String uploadExcel(HttpServletRequest request) {
		return importFromExcel.getUploadFile(request);
	}
	
}
