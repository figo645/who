package com.pwc.wechat.entity.hr;

import com.pwc.wechat.entity.candidate.AppResult;
import com.pwc.wechat.entity.candidate.AppStatus;

public class TalentData {

	private String nationalId;
	private String date;
	private String appId;
	private String name;
	private String phone;
	private String degree;
	private String email;
	private Integer q1 = 0;
	private Integer q2 = 0;
	private Integer q3 = 0;
	private AppStatus status;
	private AppResult result;
	private Boolean check;
	private String roundmoveflag;
	private String movebackflag;
	private String sendemailflag;
	
	public String getRoundmoveflag() {
		return roundmoveflag;
	}

	public void setRoundmoveflag(String roundmoveflag) {
		this.roundmoveflag = roundmoveflag;
	}

	public String getMovebackflag() {
		return movebackflag;
	}

	public void setMovebackflag(String movebackflag) {
		this.movebackflag = movebackflag;
	}

	public String getSendemailflag() {
		return sendemailflag;
	}

	public void setSendemailflag(String sendemailflag) {
		this.sendemailflag = sendemailflag;
	}

	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNationalId() {
		return nationalId;
	}

	public void setNationalId(String nationalId) {
		this.nationalId = nationalId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public AppStatus getStatus() {
		return status;
	}

	public void setStatus(AppStatus status) {
		this.status = status;
	}

	public AppResult getResult() {
		return result;
	}

	public void setResult(AppResult result) {
		this.result = result;
	}

	public Boolean getCheck() {
		return check;
	}

	public void setCheck(Boolean check) {
		this.check = check;
	}

	public String getDegree() {
		return degree;
	}

	public void setDegree(String degree) {
		this.degree = degree;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public Integer getQ1() {
		return q1;
	}

	public void setQ1(Integer q1) {
		this.q1 = q1;
	}

	public Integer getQ2() {
		return q2;
	}

	public void setQ2(Integer q2) {
		this.q2 = q2;
	}

	public Integer getQ3() {
		return q3;
	}

	public void setQ3(Integer q3) {
		this.q3 = q3;
	}

}
