package com.pwc.wechat.service.hr.round;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mysql.fabric.xmlrpc.base.Array;
import com.pwc.wechat.dao.candidate.HybridDao;
import com.pwc.wechat.dao.hr.IEmailCrud;
import com.pwc.wechat.entity.candidate.AppResult;
import com.pwc.wechat.entity.candidate.AppStatus;
import com.pwc.wechat.entity.hr.Config;
import com.pwc.wechat.entity.hr.TalentData;
import com.pwc.wechat.entity.hr.TalentDataSet;

@Service("updateTalentData")
public class UpdateTalentData implements ITalentDataHandler, ITalentDataSetHandler {
	
	/*
	 * 整个流程图如下
	 * Status Process:
	 * 		{status:STATUS_NEW,	result:RESULT_UNKNOWN}
	 * 		{status:STATUS_Q1,	result:RESULT_UNKNOWN}
	 * 		{status:STATUS_Q2,	result:RESULT_UNKNOWN}
	 * 		{status:STATUS_Q3,	result:RESULT_UNKNOWN}
	 * 		{status:STATUS_Q4,	result:RESULT_PASS}
	 * 		{status:STATUS_Q5,	result:RESULT_PASS}
	 * Status Trash:
	 * 		{					result:RESULT_FAIL}
	 * 
	 */
	@Autowired
	private IEmailCrud iEmailCrud;
	@Autowired
	private HybridDao hybridDao;
	
	@Override
	public String updateTalentDataSet(TalentDataSet talentDataSet) {
		talentDataSet.getTalentDatas().forEach((TalentData talentData) -> {
			hybridDao.updateTalentData(talentData);
		});
		return "Save Success";
	}

	@Override
	public String roundMoveTalentDataSet(TalentDataSet talentDataSet) {
		talentDataSet.getTalentDatas().forEach((TalentData talentData) -> {
			if (talentData.getCheck() != null && talentData.getCheck()) {
				AppStatus nextStatus = AppStatus.valueOf(talentData.getStatus().getNext());
				talentData.setStatus(nextStatus);
				if (nextStatus.equals(AppStatus.STATUS_Q4)) {
					talentData.setResult(AppResult.RESULT_PASS);
				}
				hybridDao.updateTalentData(talentData);
				//by dww moveflag
				Config config = new Config();
				config.setApplyNumber(talentData.getDate().replaceAll("-", "")+"-"+talentData.getAppId());
				config.setRoundMoveFlag("true");
				this.addmoveflag(config);
			}
		});
		return "RoundMove Success";
	}

	@Override
	public String moveBackTalentDataSet(TalentDataSet talentDataSet) {
		talentDataSet.getTalentDatas().forEach((TalentData talentData) -> {
			if (talentData.getCheck() != null && talentData.getCheck()) {
				AppStatus previousStatus = AppStatus.valueOf(talentData.getStatus().getPrevious());
				talentData.setStatus(previousStatus);
				if (previousStatus.equals(AppStatus.STATUS_Q3)) {
					talentData.setResult(AppResult.RESULT_UNKNOWN);
				}
				hybridDao.updateTalentData(talentData);
			}
		});
		return "MoveBack Success";
	}

	@Override
	public String moveToTrash(TalentData talentData) {
		talentData.setResult(AppResult.RESULT_FAIL);
		hybridDao.updateTalentData(talentData);
		return "RoundToTrash Success";
	}
	
	@Override
	public String recoverFromTrash(TalentData talentData) {
		if (talentData.getStatus().equals(AppStatus.STATUS_Q4) || talentData.getStatus().equals(AppStatus.STATUS_Q5)) {
			talentData.setResult(AppResult.RESULT_PASS);
		} else {
			talentData.setResult(AppResult.RESULT_UNKNOWN);
		}
		hybridDao.updateTalentData(talentData);
		return "RecoverFromTrash Success";
	}

	@Override
	public String addmoveflag(Config config) {
//		Config configtemplate = new Config();
//		configtemplate.setMoveBackFlag(config.getMoveBackFlag());
//		configtemplate.setRoundMoveFlag(config.getRoundMoveFlag());
//		configtemplate.setSendEmailFlag(config.getSendEmailFlag());
//		configtemplate.setNationalId(config.getNationalId());
//		configtemplate.setId(config.getId());
		iEmailCrud.save(config);
		return iEmailCrud.findAllByApplyNumber(config.getApplyNumber()).stream().findFirst().orElse(null).getMoveBackFlag();
	}
	
	
	@Override
	public void delmoveflag(Config config) {
		iEmailCrud.delete(config);
	}


}
