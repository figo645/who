package com.pwc.wechat.dao.hr;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pwc.wechat.entity.user.User;

public interface IUserCrud extends JpaRepository<User, String> {
	
}
