package com.pwc.wechat.controller.candidate;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.pwc.wechat.entity.candidate.Candidate;
import com.pwc.wechat.entity.candidate.QuestAndAnswer;
import com.pwc.wechat.service.candidate.ICandidateHandler;
import com.pwc.wechat.service.candidate.IQuestionnaireHandler;

@RestController
@RequestMapping(value = "/forCandidate")
public class ForCandidateController {
	
	private static final Logger logger = LogManager.getLogger(ForCandidateController.class);
	
	@Autowired
	private ICandidateHandler candidateService;
	
	@Autowired
	private IQuestionnaireHandler questionnaireService;
	
	@RequestMapping(value = { "", "/" }, produces = "text/html; charset=utf-8")
	public ModelAndView visitCandidatePage(@RequestParam(defaultValue = "") String code) throws IOException {
		logger.info("Welcome");
		logger.info("code:{}", code);
		ModelAndView candidatePage = new ModelAndView("candidate/candidate");
		return candidatePage;
	}
	
	
	@RequestMapping(value = "/showQuestionnaire", produces = "text/html; charset=utf-8")
	public ModelAndView showQuestionnaire(@RequestParam String nationalId) throws IOException {
		logger.info("Welcome Questionnaire Page");
		ModelAndView questionnairePage = new ModelAndView("hr/questionnaireWechat");
		questionnairePage.addObject("nationalId", nationalId);
		return questionnairePage;
	}
	
	
	@RequestMapping(value = "/getQuestions", produces = "application/json; charset=utf-8")
	public QuestAndAnswer getQuestions(@RequestParam String language){
		logger.info("get question");
		return questionnaireService.searchQuestAndAnswer(language);
	}
	
	@RequestMapping(value = "/insertQuestionnaire", produces = "text/html; charset=utf-8")
	public String insertQuestionnaire(@RequestBody QuestAndAnswer questAndAnswer){
		questionnaireService.saveQuestAndAnswer(questAndAnswer);
		return "success";
	}
	
	@RequestMapping(value = "/sendInfo", produces = "text/html; charset=utf-8")
	public String insertApplicationInfo(@RequestBody Candidate candidate, @RequestParam(required = false) String positon, @RequestParam(required = false) String source) {
		candidate.setSource(source);
		return String.valueOf(candidateService.saveCandidateFromPage(candidate, positon));
	} 

	
	@RequestMapping(value = "/searchWechatUid", produces = "application/json; charset=utf-8")
	public Candidate searchWechatUid(@RequestParam(required = false) String wechatUid) {
		return candidateService.findCandidateByWechatUid(wechatUid);
	}
	
}
