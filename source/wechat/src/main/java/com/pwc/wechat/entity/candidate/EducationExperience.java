package com.pwc.wechat.entity.candidate;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "education_experience", schema = "wechat")
@IdClass(EducationExperiencePk.class)
public class EducationExperience implements Serializable {

	private static final long serialVersionUID = 7686003554332857163L;

	@Id
	@Column(name = "national_id")
	private String nationalId;

	@Id
	@Column(name = "experience_id")
	private Integer experienceId;

	@Column(name = "university")
	private String university;

	@Column(name = "major")
	private String major;

	@Column(name = "degree")
	private String degree;

	@Column(name = "period")
	private String period;

	@Column(name = "gpa")
	private String gpa;

	@Column(name = "last_exp")
	private Boolean lastExp;

	@Column(name = "opt_time")
	private String optTime;

	public String getNationalId() {
		return nationalId;
	}

	public String getDegree() {
		return degree;
	}

	public void setDegree(String degree) {
		this.degree = degree;
	}

	public void setNationalId(String nationalId) {
		this.nationalId = nationalId;
	}

	public Integer getExperienceId() {
		return experienceId;
	}

	public void setExperienceId(Integer experienceId) {
		this.experienceId = experienceId;
	}

	public String getUniversity() {
		return university;
	}

	public void setUniversity(String university) {
		this.university = university;
	}

	public String getMajor() {
		return major;
	}

	public void setMajor(String major) {
		this.major = major;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String getGpa() {
		return gpa;
	}

	public void setGpa(String gpa) {
		this.gpa = gpa;
	}

	public Boolean getLastExp() {
		return lastExp;
	}

	public void setLastExp(Boolean lastExp) {
		this.lastExp = lastExp;
	}

	public String getOptTime() {
		return optTime;
	}

	public void setOptTime(String optTime) {
		this.optTime = optTime;
	}

}
