package com.pwc.wechat.dao.hr;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pwc.wechat.entity.hr.Config;


public interface IEmailCrud extends JpaRepository<Config, String> {
	
	public List<Config> findAllByEmailStatusTemplate(String status);
	public List<Config> findAllByApplyNumber(String ApplyNumber);
	//public String updateEmailTemplate(String emailStatusTemplate);
	//public List<Config> 
}
