package com.pwc.wechat.service.hr.user;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.transaction.annotation.Transactional;

public interface IAvatarHandler {
 
	@Transactional(readOnly = true, rollbackFor = Exception.class)
	public String createAvatar(String userName, HttpServletRequest request, HttpServletResponse response);
	
}
