package com.pwc.wechat.util;

import static java.time.temporal.ChronoField.DAY_OF_MONTH;
import static java.time.temporal.ChronoField.HOUR_OF_DAY;
import static java.time.temporal.ChronoField.MINUTE_OF_HOUR;
import static java.time.temporal.ChronoField.MONTH_OF_YEAR;
import static java.time.temporal.ChronoField.YEAR;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.SignStyle;
import java.time.temporal.TemporalAdjusters;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * 日期工具
 * @author ochen033
 *
 */
public class DateUtil {
	
	private DateUtil() {
		
	}

	private static Clock clock = Clock.systemDefaultZone();
	
	/**
	 * yyyy-MM-dd 加一天
	 * 
	 * @param date
	 * @param num
	 * @return
	 */
	public static String plusDays(String date, int num) {
		return LocalDate.parse(date).plusDays(num).toString();
	}

	/**
	 * 获取当前时间 yyyy-MM-dd HH:mm:ss
	 * 
	 * @return
	 */
	public static String getLocalDateTime() {
		return LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
	}

	/**
	 * 获取当前日期 yyyy-MM-dd
	 * 
	 * @return
	 */
	public static String getLocalDate() {
		return LocalDate.now().toString();
	}
	
	/**
	 * 获取距离今天num天的日期
	 * yyyy-MM-dd
	 * @return
	 */
	public static String getSomeDate(long num) {
		return LocalDate.now().plusDays(num).toString();
	}

	/**
	 * 获取当前是几点钟 24小时制
	 * 
	 * @return
	 */
	public static int getHour() {
		return LocalTime.now().getHour();
	}

	/**
	 * 获取Date类型的时间
	 * 不建议使用Date类型
	 * @return
	 */
	public static Date getTime() {
		Instant instant = clock.instant();
		return Date.from(instant);
	}

	/**
	 * 获取Epoch小时
	 * from the epoch of 1970-01-01T00:00:00Z
	 * @return
	 */
	public static Long getEpochHour() {
		return clock.instant().getEpochSecond() / 3600;
	}
	

	/**
	 * 获取Epoch秒
	 * from the epoch of 1970-01-01T00:00:00Z
	 * @return
	 */
	public static Long getEpochSecond() {
		return clock.instant().getEpochSecond();
	}
	
	/**
	 * 比较日期先后
	 * @param a yyyy-MM-dd
	 * @param b yyyy-MM-dd
	 * @return
	 */
	public static Boolean isEarlier(String a, String b) {
		return LocalDate.parse(a).isBefore(LocalDate.parse(b));
	}
	
	/**
	 * 日期操作的例子
	 * 返回当月的最后一天是星期几
	 * 
	 * @return
	 */
	public static String lastDay() {
		return LocalDate.now().with(TemporalAdjusters.lastDayOfMonth()).getDayOfWeek().toString();
	}
	/**
	 * long转date类型
	 * @return
	 */
	public static Date longToDate(Long time) {
		Instant instant = Instant.ofEpochMilli(time - 8 * 3600 * 1000);
		return Date.from(instant);
	}
	
	/**
	 * long转LocalDate
	 * @return
	 */
	public static String longToLocalDate(Long time) {
		return LocalDate.ofEpochDay(time / 1000 / 3600 / 24).toString();
	}
	
	public static String longToLocalDateTime(Long time) {
		Instant instant = Instant.ofEpochMilli(time);
		return LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).format(DateTimeFormatter.ofPattern("ddMMMyyHHmm").withLocale(Locale.US)).toUpperCase();
	}
	/**
	 * 日期控件的日期转yyyy-MM-dd
	 * @param dateString
	 * @return
	 */
	public static String transferDate(String dateString) {
		return LocalDate.parse(dateString, SNAP_TIME).toString();
	}
	
	
	/**
	 * 恶心的bootstrap日期控件的日期格式
	 */
	private static final DateTimeFormatter SNAP_TIME;
    static {
        // manually code maps to ensure correct data always used
        // (locale data can be changed by application code)
      
        Map<Long, String> moy = new HashMap<>();
		moy.put(1L, "JAN");
        moy.put(2L, "FEB");
        moy.put(3L, "MAR");
        moy.put(4L, "APR");
        moy.put(5L, "MAY");
        moy.put(6L, "JUN");
        moy.put(7L, "JUL");
        moy.put(8L, "AUG");
        moy.put(9L, "SEP");
        moy.put(10L, "OCT");
        moy.put(11L, "NOV");
        moy.put(12L, "DEC");
        SNAP_TIME = new DateTimeFormatterBuilder()
                .parseCaseInsensitive()
                .parseLenient()
                .appendValue(DAY_OF_MONTH, 1, 2, SignStyle.NOT_NEGATIVE)
                .appendText(MONTH_OF_YEAR, moy)
                .appendValueReduced(YEAR,2,4,2000)  // 2 digit year not handled
                .appendValue(HOUR_OF_DAY, 2)
                .appendValue(MINUTE_OF_HOUR, 2)
                .toFormatter();
    }

}
