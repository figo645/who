package com.pwc.wechat.dao.candidate;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pwc.wechat.entity.candidate.CandidateHistory;

public interface ICandidateHistoryCrud extends JpaRepository<CandidateHistory, Long> {
	
	public List<CandidateHistory> findAllByNationalId(String nationalId);

}
