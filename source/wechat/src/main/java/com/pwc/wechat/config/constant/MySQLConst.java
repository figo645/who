package com.pwc.wechat.config.constant;

import java.io.IOException;
import java.util.Properties;
import org.springframework.core.io.ClassPathResource;

public class MySQLConst {
	
	private MySQLConst () {
		
	}	

	public static String MYSQL_URL = GetValueByKey("MYSQL_URL");
	public static String MYSQL_DRIVER = GetValueByKey("MYSQL_DRIVER");
	public static String MYSQL_USER_NAME = GetValueByKey("MYSQL_USER_NAME");
	public static String MYSQL_PASSWORD = GetValueByKey("MYSQL_PASSWORD");

	public static String GetValueByKey(String key) {
		Properties pps = new Properties();
		try {
			ClassPathResource cr = new ClassPathResource("application.properties");
			pps.load(cr.getInputStream());
			String value = pps.getProperty(key);
			System.out.println(key + " = " + value);
			return value;

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

}
