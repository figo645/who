package com.pwc.wechat.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

public class XmlUtil {

	private static final Logger logger = LogManager.getLogger(XmlUtil.class);

	private static final XmlMapper xmlMapper = new XmlMapper();

	private XmlUtil() {

	}

	/**
	 * 输出全部属性 如果xml中存在，对象中没有，则自动忽略该属性 失败返回null
	 * 
	 * @param xmlContent
	 * @param clazz
	 * @return
	 */
	public static <T> T toNormalObject(String xmlContent, Class<T> clazz) {
		return xmlToObject(xmlContent, clazz);
	}
	
	/**
	 * 输出全部属性 
	 * 如果xml中存在，对象中没有，则自动忽略该属性 
	 * 失败返回null
	 * @param inputStream
	 * @param clazz
	 * @return
	 */
	public static <T> T toNormalObject(byte[] bytes, Class<T> clazz) {
		return xmlToObject(bytes, clazz);
	}

	/**
	 * 输出non null属性 失败返回""
	 * 
	 * @param object
	 * @return
	 */
	public static byte[] toNormalXml(Object object) {
		try {
			return xmlMapper.setSerializationInclusion(Include.NON_NULL).writerWithDefaultPrettyPrinter().writeValueAsBytes(object);
		} catch (JsonProcessingException e) {
			logger.info("ObjToXml failed:", e);
		}
		return null;
	}
	
	/**
	 * 输出non null属性 失败返回""
	 * 
	 * @param object
	 * @return
	 */
	public static String toNormalXmlString(Object object) {
		try {
			return xmlMapper.setSerializationInclusion(Include.NON_NULL).writerWithDefaultPrettyPrinter().writeValueAsString(object);
		} catch (JsonProcessingException e) {
			logger.info("ObjToXml failed:", e);
		}
		return "";
	}

	private static <T> T xmlToObject(byte[] bytes, Class<T> clazz) {
		try {
			return xmlMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES).readValue(bytes, clazz);
		} catch (Exception e) {
			logger.info("XmlToObject failed:", e);
		}
		return null;
	}
	
	private static <T> T xmlToObject(String xmlContent, Class<T> clazz) {
		try {
			return xmlMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES).readValue(xmlContent, clazz);
		} catch (Exception e) {
			logger.info("XmlToObject failed:", e);
		}
		return null;
	}
	
}
