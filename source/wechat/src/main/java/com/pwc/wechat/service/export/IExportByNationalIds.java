package com.pwc.wechat.service.export;

import javax.servlet.http.HttpServletResponse;

import org.springframework.transaction.annotation.Transactional;

public interface IExportByNationalIds {
	
	@Transactional(readOnly = true)
	public void executeFlush(String[] nationalIds, HttpServletResponse response) throws Exception;
	
}
