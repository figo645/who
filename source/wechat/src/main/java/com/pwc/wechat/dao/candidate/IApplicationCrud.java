package com.pwc.wechat.dao.candidate;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pwc.wechat.entity.candidate.Application;
import com.pwc.wechat.entity.candidate.ApplicationPk;

public interface IApplicationCrud extends JpaRepository<Application, ApplicationPk> {
	
	public List<Application> findAllByNationalIdOrderByApplyDateDesc(String nationalId);
	
	public List<Application> findAllByNationalIdAndApplyDateAfter(String nationalId, String afterDate);
	
}
