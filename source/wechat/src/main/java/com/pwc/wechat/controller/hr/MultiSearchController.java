package com.pwc.wechat.controller.hr;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pwc.wechat.entity.candidate.CandidateAllInfo;
import com.pwc.wechat.entity.hr.Config;
import com.pwc.wechat.entity.hr.HrComment;
import com.pwc.wechat.entity.hr.MultiCondition;
import com.pwc.wechat.entity.hr.TalentData;
import com.pwc.wechat.entity.hr.TalentDataSet;
import com.pwc.wechat.entity.hr.TalentEmail;
import com.pwc.wechat.entity.hr.TalentFile;
import com.pwc.wechat.service.candidate.ICandidateHandler;
import com.pwc.wechat.service.hr.round.IHrCommentHandler;
import com.pwc.wechat.service.hr.round.IMultiSearch;
import com.pwc.wechat.service.hr.round.ITalentDataHandler;
import com.pwc.wechat.service.hr.round.ITalentDataSetHandler;
import com.pwc.wechat.service.mail.IEmailHandler;
import com.pwc.wechat.service.upload.IGetUploadFile;
import com.pwc.wechat.service.upload.IUploadAndProcess;

@RestController
@RequestMapping(value = "/forHr")
public class MultiSearchController {

	@Autowired
	private IMultiSearch<TalentDataSet, MultiCondition> searchTalentData;
	
	@Autowired
	private ITalentDataHandler iTalentDataHandler;
	
	@Autowired
	private ITalentDataSetHandler iTalentDataSetHandler;
	
	@Autowired
	private IHrCommentHandler iHrCommentHandler;
	
	@Autowired
	private ICandidateHandler candidateService;
	
	@Autowired
	private IEmailHandler emailService;
	
	@Autowired
	private IGetUploadFile<TalentFile> iGetUploadFile;
	
	@RequestMapping(value = "/multiSearch", produces = "application/json; charset=utf-8")
	public TalentDataSet multiSearch(@RequestBody MultiCondition multiCondition) {
		TalentDataSet set = searchTalentData.searchByMultiCondition(multiCondition);
		
		return set;
	}
	
	@RequestMapping(value = "/saveTalentData", produces = "text/html; charset=utf-8")
	public String saveTalentData(@RequestBody TalentDataSet talentDataSet) {
		return iTalentDataSetHandler.updateTalentDataSet(talentDataSet);
	}
	
	@RequestMapping(value = "/roundMoveTalentData", produces = "text/html; charset=utf-8")
	public String roundMoveTalentData(@RequestBody TalentDataSet talentDataSet) {
		return iTalentDataSetHandler.roundMoveTalentDataSet(talentDataSet);
	}
	
	@RequestMapping(value = "/moveBackTalentData", produces = "text/html; charset=utf-8")
	public String moveBackTalentData(@RequestBody TalentDataSet talentDataSet) {
		return iTalentDataSetHandler.moveBackTalentDataSet(talentDataSet);
	}
	
	@RequestMapping(value = "/toTrash", produces = "text/html; charset=utf-8")
	public String toTrash(@RequestBody TalentData talentData) {
		return iTalentDataHandler.moveToTrash(talentData);
	}
	
	@RequestMapping(value = "/batchTotrash", produces = "text/html; charset=utf-8")
	public void batchTotrash(@RequestBody TalentDataSet talentDataSet) {
		talentDataSet.getTalentDatas().forEach((TalentData talentData)->{
			if(talentData.getCheck() != null && talentData.getCheck()){
				iTalentDataHandler.moveToTrash(talentData);
			}
		});
	}
	
	@RequestMapping(value = "/recoverFromTrash", produces = "text/html; charset=utf-8")
	public String recoverFromTrash(@RequestBody TalentData talentData) {
		return iTalentDataHandler.recoverFromTrash(talentData);
	}

	@RequestMapping(value = "/getHrComment", produces = "application/json; charset=utf-8")
	public HrComment getHrComment(@RequestBody TalentData talentData) {
		return iHrCommentHandler.searchHrComment(talentData.getNationalId());
	}
	
	@RequestMapping(value = "/saveHrComment", produces = "text/html; charset=utf-8")
	public String saveHrComment(@RequestBody HrComment hrComment) {
		iHrCommentHandler.updateHrComment(hrComment);
		return "success";
	}
	
	@RequestMapping(value = "/getCandidateAllInfo", produces = "application/json; charset=utf-8")
	public CandidateAllInfo getCandidateInfo(@RequestBody TalentData talentData) {
		return candidateService.findCandidateAllInfoByNationalId(talentData.getNationalId());
	}
//get tanlentEmail subject and context template	
	@RequestMapping(value = "/getEmailTemplate", produces = "application/json; charset=utf-8")
	public Config getEmailTemplate(@RequestBody String emailStatusTemplate) {
		return emailService.getEmailTemplate(emailStatusTemplate);
	}
	@RequestMapping(value = "/updateEmailTemplate", produces = "text/html; charset=utf-8")
	public String updateEmailTemplate(@RequestBody Config emailconfig) {
		return emailService.updateEmailTemplate(emailconfig);
		
	}
	
	@RequestMapping(value = "/sendemail", produces = "text/html; charset=utf-8")
	public String sendemail(@RequestBody TalentEmail talentemail) throws IOException{
		File folder = new File("C:\\Email");    
		  if(folder.exists() && folder.isDirectory()) {
			  
		  }else{
			  folder.mkdirs(); 
		  }
		
		
		Path path = Files.createTempDirectory(Paths.get("C:/Email"), "TempAttach");
		List<Path> files =talentemail.getAttachment().stream().map(talentfile->{
			Path filePath = path.resolve(talentfile.getFilename());
			try {
				OutputStream outputStream = Files.newOutputStream(filePath);
				outputStream.write(talentfile.getFilecontexnt());
				outputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return filePath;
		}).collect(Collectors.toList());
		int error = emailService.sendEmail(talentemail, files);

		files.forEach(file-> {
			try {
				Files.delete(file);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		Files.delete(path);
		if(error ==0){
			return "success";
		}else{
			return "error";
		}
		
		
	}
	
	@RequestMapping(value = "/sendattachment", produces = "application/json; charset=utf-8")
	public List<TalentFile> sendattachment(HttpServletRequest request) {
		IUploadAndProcess<List<TalentFile>> uploadAttachment = (req, attrs) -> 
		iGetUploadFile.getUploadFile(request, file -> {
			TalentFile talentfile = new TalentFile();
			try {
				talentfile.setFilecontexnt(file.getBytes());
				talentfile.setFilename(file.getOriginalFilename());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return talentfile;
		});
		return uploadAttachment.getUploadFile(request);
	}
	
	
	@RequestMapping(value = "/addroundmoveflag", produces = "text/html; charset=utf-8")
	public String addroundmoveflag(@RequestBody Config config) {
		
		return iTalentDataSetHandler.addmoveflag(config);
		
	}
}
