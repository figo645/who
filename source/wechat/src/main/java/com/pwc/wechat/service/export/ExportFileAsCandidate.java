package com.pwc.wechat.service.export;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pwc.wechat.entity.candidate.Candidate;
import com.pwc.wechat.service.candidate.ICandidateHandler;
import com.pwc.wechat.service.export.writer.IEntityWriter;

@Service("exportFileAsCandidate")
public class ExportFileAsCandidate extends AbstractExportFile<Candidate> {
	
	@Autowired
	private ICandidateHandler candidateService;
	
	@Autowired
	private IEntityWriter<Candidate> candidateToPdf;

	@Override
	protected Candidate searchByNationalId(String nationalId) {
		return candidateService.findCandidateByNationalId(nationalId);
	}

	@Override
	protected IEntityWriter<Candidate> initWriter() {
		return candidateToPdf;
	}

}
