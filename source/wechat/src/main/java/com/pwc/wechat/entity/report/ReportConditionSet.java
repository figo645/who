package com.pwc.wechat.entity.report;

import java.util.List;

import com.pwc.wechat.entity.hr.MultiCondition;

public class ReportConditionSet extends MultiCondition {

	List<ReportConfig> reportConfigs;

	public List<ReportConfig> getReportConfigs() {
		return reportConfigs;
	}

	public void setReportConfigs(List<ReportConfig> reportConfigs) {
		this.reportConfigs = reportConfigs;
	}

}
