package com.pwc.wechat.service.hr.round;

import org.springframework.transaction.annotation.Transactional;

import com.pwc.wechat.entity.hr.MultiCondition;

public interface IMultiSearch<T, M extends MultiCondition> {

	@Transactional(readOnly = true)
	public T searchByMultiCondition(M multiCondition);

}
