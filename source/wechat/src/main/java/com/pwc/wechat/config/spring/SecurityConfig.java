package com.pwc.wechat.config.spring;

import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private DataSource dataSourceMySQL;
  
	
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		 auth.jdbcAuthentication()
		.dataSource(dataSourceMySQL)
		.usersByUsernameQuery(this.getUserQuery())
     	.passwordEncoder(new Md5PasswordEncoder())
	    .authoritiesByUsernameQuery(this.getAuthoritiesQuery());
	}
	


	/**
	 * when authentication is required, redirect the browser to /login
	 * 
	 * we are in charge of rendering the login page when /login is requested
	 * 
	 * when authentication attempt fails, redirect the browser to /login?error
	 * (since we have not specified otherwise)
	 * 
	 * we are in charge of rendering a failure page when /login?error is
	 * requested
	 * 
	 * when we successfully logout, redirect the browser to /login?logout (since
	 * we have not specified otherwise)
	 * 
	 * we are in charge of rendering a logout confirmation page when
	 * /login?logout is requested
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		http
			.csrf()
				.disable()
			.authorizeRequests()
				.antMatchers("/resources/**", "/html/**", "/forCandidate/*", "/forCandidate*", "/talk/*")
				.permitAll()
				.and()
			.authorizeRequests()
				.antMatchers("/forHr/*")
				.hasRole("HR")
				.and()
			.authorizeRequests()
			    .antMatchers("/forHrm/*")
			    .hasRole("HRM")
			.anyRequest()
				.authenticated()
				.and()
			.exceptionHandling()
				.accessDeniedPage("/html/error/accessDenied.html")
				.and()
			.formLogin()
				.loginPage("/login")
				.permitAll()
				.and()
			.logout()
				.permitAll();
				
//			http
//				.headers()
//					.frameOptions().sameOrigin()
//					.httpStrictTransportSecurity().disable();
		
	
	}

	private String getUserQuery() {
		return "SELECT t.user_name as 'username', t.pass_word as 'password', t.enabled as 'enabled' FROM wechat.users t WHERE t.user_name = ?";
	}

	private String getAuthoritiesQuery() {
		return "SELECT t.user_name as 'username', t.user_auth as 'authority' FROM wechat.authorities t WHERE t.enabled = '1' and t.user_name = ?";
	}
		

}
