package com.pwc.wechat.service.hr.report;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pwc.wechat.dao.candidate.HybridDao;
import com.pwc.wechat.entity.report.Report;
import com.pwc.wechat.entity.report.ReportColumn;
import com.pwc.wechat.entity.report.ReportConfig;

@Service("searchGroupEvalReport")
public class SearchGroupEvalReport extends SearchReport {
	
	@Autowired
	private HybridDao hybridDao;
	
	@Override
	protected Report executeSearch(ReportConfig reportConfig) {
		ReportColumn[] columns = reportConfig.getConfigs();
		Report report = new Report();		
		report.setTitle(reportConfig.getTitle());
		report.setTableHead(Arrays.stream(columns).map(t -> t.getColumnName()).toArray(String[]::new));
		report.setTableBody(hybridDao.querySqlWithJpa(columns, reportConfig.getConditions(), (sqlResult, rowNum) -> {
			String[] row = new String[columns.length];
			row[2] = (rowNum + 1) + "-" + String.valueOf(sqlResult[1]) + "-" + String.valueOf(sqlResult[2]) + "-" + String.valueOf(sqlResult[3]);
			return row;
		}));
		return report;
	}
	
}
