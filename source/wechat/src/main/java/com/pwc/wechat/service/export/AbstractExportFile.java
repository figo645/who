package com.pwc.wechat.service.export;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.pwc.wechat.service.export.writer.IEntityWriter;

public abstract class AbstractExportFile<T> extends AbstractExport<T, OutputStream> {
	
	@Override
	protected OutputStream getOutputStream(HttpServletResponse response) throws IOException {
		return response.getOutputStream();
	}
	
	@Override
	protected void writeAll(OutputStream outputStream, List<T> entityList) throws Exception {
		this.initWriter().writeEntity(outputStream, entityList);
	}
	
	@Override
	protected void initResponse(HttpServletResponse response) {
		IEntityWriter<T> writer = initWriter();
		writer.initResponse(response);
	}
	
}
