var app = angular.module('offerModule',['ngAnimate', 'ui.bootstrap','ui.router','ui.tinymce']);
	app.config(function($stateProvider, $urlRouterProvider,$locationProvider) {
	    $urlRouterProvider.otherwise('/');
	    $stateProvider
	        .state('process', {
	            url: '/',
	            templateUrl: 'html/process.html'
	        })
	        .state('sheets', {
	            url: '/sheets',
	            templateUrl: 'forHr/reportlist',
	            controller: 'report'
	        })
	        .state('newhr', {
	            url: '/newhr',
	            templateUrl: 'forHrm/newHr',
	            controller: 'hrManager'
	        })
	        .state('help', {
	            url: '/help',
	            template: '<h3>If you have any questions, Please call Wilson Xu.<br><br>TEL:13636693701</h3><script>$(".on").removeClass("on");$("#help").addClass("on");</script>'
	        })
	        .state('addcandidate',{
	        	url: '/addcandidate',
	        	templateUrl: '/wechat/html/candidate/candidate_hr.html',
	        })
	        .state('addcandidate.basic',{
	        	url: '/basic',
	        	templateUrl: '/wechat/html/candidate/basicInformation.html',
	        })
	        .state('addcandidate.education',{
	        	url: '/education',
	        	templateUrl: '/wechat/html/candidate/educationSkill.html',
	        })
	        .state('addcandidate.survey',{
	        	url: '/survey',
	        	templateUrl: '/wechat/html/candidate/survey.html',
	        })
	        .state('addcandidate.preview',{
	        	url: '/preivew',
	        	templateUrl: '/wechat/html/candidate/preview.html',
	        })
	});