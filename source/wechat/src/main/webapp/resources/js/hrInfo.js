app.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs, ngModel) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });      
            });
        }
    };
}]);


app.controller('hrInfoController',function($scope, $http,$rootScope){
	var lastPassword='';
	$scope.user = {};
    $scope.user.userName = name;

 	$scope.verifyPassword = function(){	
        $http.post('verifyPassword',$scope.user).success(function(data) { 	
        	console.log(data);
		if(data=="valid"){
		  $scope.showValid = true;
		  $scope.validLabel = "";
		  $scope.verifyPw = true;
		  lastPassword=$scope.user.password;
		  $scope.newpwdstyle = {
				"border": "1px solid #a9a9a9"
			};
		}
		else {
		  $scope.showValid = true;
		  $scope.validLabel = "password is invalid";
		  $scope.newpwdstyle = {
				"border": "2px solid red"
			};
		  $scope.verifyPw = false;
		}
	    }).error(function(data, status, headers, config) {
		   console.log("error...");
	    });
 	};
 	

 	$scope.initNext = function(){
 		$scope.user.password =''; 
 		$scope.confirmpw = '';
 	};
 	$scope.changePassword = function(){
 		$scope.user.password=$scope.user.passwordNew
 		$http.post('changePassword',$scope.user).success(function(data){	
 		  console.log($scope.user.userName);
 		  console.log($scope.user.password);
 		  }).error(function(data, status, headers, config) {
 		   console.log("error...");
 		});
 	};
 	$scope.checkPassword = function() {			
    	$scope.showpw = true;
        $scope.pwlabel = "";
		var password = $scope.user.passwordNew;
		var passwordLen = $scope.user.passwordNew.length;
		var reg1 = new RegExp("[a-z]");
		var reg2 = new RegExp("[A-Z]");
		var reg3 = new RegExp("[0-9]");
		var reg4 = new RegExp("[-`~!@#$%^&*()+{}|;:'\"<,.?/>_+=]");

		/* 判断密码必须包含小写字母 */
		if (!reg1.exec(password) || !reg2.exec(password) || !reg3.exec(password)) {
			$scope.pwstyle = {
				"border": "2px solid red"
			};
			$scope.checkPw = false;
			$scope.pwlabel = "password is invalid";
		}
		/* 判断密码必须包含特殊字符 */
		else if (!reg4.exec(password) && password.indexOf("\\") == -1 && password.indexOf("\[") == -1 && password.indexOf("\]") == -1) {
			$scope.pwstyle = {
				"border": "2px solid red"
			};
			$scope.checkPw = false;
			$scope.pwlabel = "password is invalid";
		}
		/* 判断密码长度在8到16位 */
		else if (passwordLen < 8 || passwordLen > 16) {
			$scope.pwstyle = {
				"border": "2px solid red"
			};
			$scope.checkPw = false;
			$scope.pwlabel = "password is invalid";
		} 
		/* 判断密码是不是和老密码一样 */
		else if (password==lastPassword){
			$scope.checkPw = false;
			$scope.pwlabel = "This password is same to the last one";
			$scope.pwstyle = {
				"border": "2px solid red"
			};
		}
		else {
			$scope.checkPw = true;
			$scope.pwstyle = {
				"border": "1px solid #a9a9a9"
			};
			$scope.pwlabel = "";
		}
	};
	
	$scope.confirmPw = function() {
		if ($scope.confirmpw != $scope.user.passwordNew) {
			$scope.checkConfirm = false;
			$scope.showconfirm = true;
			$scope.confirmstyle = {
				"border": "2px solid red"
			};
		} else {
			$scope.checkConfirm = true;
			$scope.showconfirm = false;
			$scope.confirmstyle = {
				"border": "1px solid #a9a9a9"
			}
		}
	};
    
	$scope.hidepassword = function() {
		$scope.showpw = false;
	};

	$scope.hideconfirmpw = function() {
		$scope.showconfirm = false;
	};
	

   initAvatar = function(){
		$http.get('getAvatar',{
			params: {'userName': $scope.user.userName}
		}).success(function(data){
			if (data=='avatar_not_exists')
				angular.element('#avatar').attr('src','/wechat/resources/images/default.jpg');
			else
				angular.element('#avatar').attr('src','getAvatar?userName='+$scope.user.userName);
		}).error(function(data){
			console.log("Init Avatar failed");
		});
		
   }		
	initAvatar();
	$scope.uploadAvatar = function(){
	   var file = $scope.myFile;
	   if((file.size/1024)>1024){
	   	change_dialog_error("The picture is more than 1M!");
	  	show_dialog_error();
	   }else{
             var fd = new FormData();
             fd.append('file',file);   
             $http.post('uploadAvatar',fd,{
    	     transformRequest:angular.identity,
    	     headers:{'content-type':undefined},
             params: {'userName': $scope.user.userName}
             })
             .success(function(data){
    	       angular.element('#avatar').attr('src','getAvatar?userName='+$scope.user.userName);
             console.log("upload avatar success");
             }).error(function(){
    	     console.log("upload avatar error");
           });
	    }
	};
});

function changepasswordInit(){
	var scope = angular.element('[ng-controller=hrInfoController]').scope();
	scope.$apply(function($scope) {
 		$scope.user.password =''; 
 		$scope.user.passwordNew="";
 		$scope.confirmpw="";
 		$scope.verifyPw=false;
 		$scope.showpw = false;
 		$scope.showconfirm = false;
 		$scope.showValid=false;
 		$scope.confirmstyle = {
			"border": "1px solid #a9a9a9"
		}
		$scope.pwstyle = {
			"border": "1px solid #a9a9a9"
		};
 		$scope.newpwdstyle = {
			"border": "1px solid #a9a9a9"
		};
	});
}