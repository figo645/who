// var myModule=angular.module("MyModule",[]);
app.controller('report', function($scope, $http, $timeout){
	var datalist = [];
	$scope.groupData = [];
	$scope.all = {};
	$scope.all.print = false;
	$scope.reportTitle = null;
	$scope.condition = {
		id:'Select Status',
		title:'Select Status:',
		selections:[
		            {id:"RESULT_UNKNOWN",title:'STATUS_NEW',check:null},
		            {id:"RESULT_UNKNOWN",title:'STATUS_Q1',check:null},
		            {id:"RESULT_UNKNOWN",title:'STATUS_Q2',check:null},
		            {id:"RESULT_UNKNOWN",title:'STATUS_Q3',check:null},
		            {id:"RESULT_PASS",title:'STATUS_Q4',check:null},
		            {id:"RESULT_PASS",title:'STATUS_Q5',check:null},
		            {id:"RESULT_FAIL",title:'STATUS_Trash',check:null}
		            ],
		otherCondition:{
			round1:[null,null],
			round2:[null,null],
			round3:[null,null]
		}
	};
	
	getReportColumn();
	
	function getReportColumn() {
		$http.post('forHr/getReportColumn', null)
      	.success(function(redata){
      		$scope.condition.reportConfigs = redata;
      	});
	};
	
	$scope.selectTable = function(table) {
		var tablelist = $scope.condition.reportConfigs;
		for (var i in tablelist){
			tablelist[i].check = null;
		}
		table.check = true;
		switch(table.title){
			case 'Paper Test Attendance Sheet':
				$scope.reportTitle = 'Paper Test';
				break;
			case 'Group Interview Attendance Sheet':
				$scope.reportTitle = 'Group Interview';
				break;
			case 'Final Interview Attendance Sheet':
				$scope.reportTitle = 'Final Interview';
				break;
			case 'Final Interview Evaluation Sheet':
				$scope.reportTitle = 'Final Interview';
				break;
		}
		defaultStatus();
		makeReport();
	}
	
	$scope.confirmReportConfig = function(reportConfig) {
		console.log(reportConfig);
		$http.post('forHr/confirmReportConfig', reportConfig)
      	.success(function(redata){
      		console.log(redata);
      	});
	}
	
	$scope.changeScore = function(){
		makeReport();
	}
	
	$scope.selectStatus = function(select){
		var statuslist = $scope.condition.selections;
		for (var i in statuslist){
			statuslist[i].check = null;
		}
		select.check = true;
		makeReport();
	}
	
	// shuffle
	$scope.shuffleData = function() {
		$scope.reportdata.tableBody = shuffle($scope.reportdata.tableBody);
	}
	
	$scope.print = function() {
		if($scope.reportdata.title != 'Group Interview Evaluation Sheet'){
			datalist = $scope.reportdata.tableBody;					
			for (var i in datalist){
				datalist[i].show = datalist[i].check ?  {} : {"display":"none"};
				$scope.checkeach = {"display":"none"}
				$scope.checkall = {"display":"none"}
				$scope.checkall1 = {"display":"none"}
				$scope.checkeach1 = {"display":"none"}
				console.log(datalist[i].show);
			}
			
			var timer = $timeout(function() {
				angular.element('#printContainer').jqprint();
				// angular.element(document.querySelector('#printContainer')).removeClass('table-bordered')
				for (var i in datalist){
					datalist[i].show = {};
				}
				$scope.checkeach = {};
				$scope.checkall = {};
				$scope.checkeach1 = {};
				$scope.checkall1 = {};
			}, 500);
		}else{
			$scope.all.print = true;
			if(datalist.length == 0){
				datalist = $scope.reportdata.tableBody;
			}
			console.log(datalist);
			// var datalist = $scope.reportdata.tableBody;
			var tempData = [];
			$scope.groupData = [];			
			var begin = 0;
			var end = 9;
					
			for (var i in datalist){
				if(datalist[i].check){
					tempData.push(datalist[i]);
				}
			}

			var len = Math.ceil(tempData.length/9);
			for(var i=0;i<len;i++){
				begin = 9*i;
				end = 9*i+9;
				$scope.groupData.push(tempData.slice(begin,end));
			}
			console.log(tempData);
			console.log($scope.groupData);
			
			var timer = $timeout(function() {
				angular.element('#printContainer').jqprint();
			}, 500);
		}
		
	}

	

	$scope.editGroupReport = function(){
		$scope.all.print = false;
	}
	
	$scope.getAllColumns = function() {
		$http.post('forHr/getAllColumns', null)
      	.success(function(redata){
      		$scope.leftColumns = redata;
      	});
	}
	
	$scope.moveUp = function(){
		datalist = $scope.reportdata.tableBody;
		for(var i in datalist){
			if(i>0){
				if(datalist[i].check == true){
					swap = datalist[i-1];
					datalist[i-1] = datalist[i];
					datalist[i] = swap;
				}
			}else{
				if(datalist[i].check == true){
					break;
				}
			}
		}
	}
	
	$scope.moveDown = function(){
		datalist = $scope.reportdata.tableBody;
		for(i=datalist.length;i>0;i--){
			if(i<datalist.length){
				if(datalist[i-1].check == true){
					swap = datalist[i];
					datalist[i] = datalist[i-1];
					datalist[i-1] = swap;
				}
			}else{
				if(datalist[i-1].check == true){
					break;
				}
			}
			
		}
	}
	
	$scope.selectAll = function(all){
		datalist = $scope.reportdata.tableBody;
		for (var i in datalist){
			datalist[i].check = $scope.all.check;
		}
	}
	
	$scope.allToRight = function(){
		var allColumns = $scope.leftColumns;
		for (var i in allColumns) {
			$scope.selectReportConfig.configs.push(allColumns[i]);
		}
	}
	
	$scope.allToLeft = function(){
		var num = $scope.selectReportConfig.configs.length;
		for(var i = 0;i < num; i++) {
			$scope.selectReportConfig.configs.pop();
		}
	}
	
	$scope.someToRight = function() {
		var leftSelected = $scope.leftSelected;
		for (var i in leftSelected) {
			$scope.selectReportConfig.configs.push(leftSelected[i]);
		}
	}
	
	$scope.someToLeft = function() {
		var newRight = new Array();
		var nowRight = $scope.selectReportConfig.configs;
		var rightSelected = $scope.rightSelected
		for(var i in nowRight) {
			var hasThis = false;
			for(var j in rightSelected){
				if (nowRight[i] == rightSelected[j]){
					hasThis = true;
				}
			}
			if (!hasThis){
				newRight.push(nowRight[i]);
			}
		}
		$scope.selectReportConfig.configs = newRight;
	}
	
	function makeReport(){
		$http.post('forHr/genReport', $scope.condition)
      	.success(function(redata){
      		$scope.reportdata = redata;
      		$scope.reportdata.theadLength = $scope.reportdata.tableHead.length-1;
      		$scope.showHead = true;
      		$scope.all.check = false;
      		console.log($scope.showHead);
      		console.log($scope.reportdata);
      	});
	}
	
	function shuffle(talentData){ //v1.0
		for(var j, x, i = talentData.length; i; j = parseInt(Math.random() * i), x = talentData[--i], talentData[i] = talentData[j], talentData[j] = x);
		return talentData;
	};	
	
	function defaultStatus(){		
		var tablelist = $scope.condition.reportConfigs;
		for (var i in tablelist){
			if (tablelist[i].check) {	
				clearStatus()
				if (i<=2){					
					$scope.condition.selections[i-(-1)].check = true;
				}else{
					$scope.condition.selections[1].check = true;
				}
			}
		}
	}
	
	function clearStatus(){
		var statuslist = $scope.condition.selections;
		for (var j in statuslist){
			statuslist[j].check = null;
		}
	}
	
});
