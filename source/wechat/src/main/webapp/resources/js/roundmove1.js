var myModule = angular.module("MyModule", []);

myModule.directive("edit", function($document) {
	return {
		restrict : 'AE',
		require : 'ngModel',
		link : function(scope, element, attrs, ngModel) {
			element.bind("click", function() {
//				alert("ngmodelValueId:" + ngModel.$modelValue.nationalId+ngModel.$modelValue.applyId)
				var id = "txt_name_" + ngModel.$modelValue.nationalId+ngModel.$modelValue.applyId;
				scope.$apply(function() {
					angular.copy(ngModel.$modelValue, scope.master);
//					alert("scopemaster:" + scope.master);
				})
				var obj = $('#' + id);
				obj.removeAttr("readOnly");
			});
		}
	}
})



myModule.controller('roundmove', function($scope, $http) {
	 $scope.selections=[{id:1,name:'Positions',tags:[{id:1,name:'Java Intern'},{id:2,name:'Guidwire Intern'},{id:3,name:'HR Intern'},{id:4,name:'PQA Intern'},{id:5,name:'Admin Intern'},{id:6,name:'Orcale Intern'}]},{id:2,name:'Days',tags:[{id:7,name:'one day'},{id:8,name:'two days'},{id:9,name:'Three days'},{id:10,name:'Four days'},{id:11,name:'Five days'}]},{id:3,name:'Intentions',tags:[{id:12,name:'Work'},{id:13,name:'Study abroad'},{id:14,name:'Candidate for master degree'}]}]
	 $(document).ready(function(){
			 $scope.selected = [];
			    $scope.selectedTags = [];

			    var updateSelected = function(action,id,name){
			        if(action == 'add' && $scope.selected.indexOf(id) == -1){
			            $scope.selected.push(id);
			            $scope.selectedTags.push(name);
			        }
			        if(action == 'remove' && $scope.selected.indexOf(id)!=-1){
			            var idx = $scope.selected.indexOf(id);
			            $scope.selected.splice(idx,1);
			            $scope.selectedTags.splice(idx,1);
			        }
			    }

			    $scope.updateSelection = function($event, id){
			    	var checkbox = $event.target;
					if ($(checkbox).parent().parent().hasClass("tat")) {
						$(checkbox).parent().parent().removeClass("tat");
					} else {
						$(checkbox).parent().parent().addClass("tat");
					}
			        var action = (checkbox.checked?'add':'remove');
			        updateSelected(action,id,checkbox.name);
			        var sendcondition = $scope.selections;
			        var config = {params: {dataStr:'R1'}};
			        $http.post('multiSearchOld', sendcondition,config).success(function(redata){
			        	
			        });
			    }

			    $scope.isSelected = function(id){
			        return $scope.selected.indexOf(id)>=0;
			    }
			
		

			
			
	 })
	 
	 

	

	$scope.roundmove = function() {
		var postData = $scope.items;
		 $('input:checked').each(function(){//找到所有选中的 复选框
	            $(this.parentNode.parentNode).remove();//复选框上一层
	        });
		$http.post('roundmove1c', postData).success(function(data) {
//			alert("success");
		}).error(function(data, status, headers, config) {
			console.log("error...");
		});
	};
	
	$scope.save = function() {
		var postData = $scope.items;
		$http.post('roundmove1s', postData).success(function(data) {
//			alert("success");
		}).error(function(data, status, headers, config) {
			console.log("error...");
		});
	};
	
	$scope.datachange = function(scope, element, attrs, ngModel) {
		var id = "txt_name_" + ngModel.$modelValue.nationalId;
		var obj = $('#' + id).value = document.write();
	}
	$scope.AllEdit = function() {
		$(".RS2").removeAttr("readOnly");
	}
	
	$scope.reportall = function() {
		var apps = $scope.items;
		var nationalIds = new Array(apps.length);
		for ( var i in apps) {
			nationalIds[i] = apps[i].nationalId;
		}
		window.location.href = "/wechat/exportResume?nationalIds=" + nationalIds;
	};

	$scope.sendemail = function() {
		var postData = $scope.items;
		$http.post('sendemail', postData)
				.success(function(data) {
//					alert("success");
				}).error(function(data, status, headers, config) {
					console.log("error...");
				});
	};

	$scope.report = function() {
		var postData = $scope.items;
		var nationalIds = new Array(postData.length);
		for ( var i in postData) {
			nationalIds[i] = postData[i].nationalId;
		}
		window.location.href = "/wechat/exportResume?nationalIds=" + nationalIds;
	};
	
	
	
	$scope.getCandidate = function(item) {
//		alert(item);
//		alert(item.nationalId);
    	var config = {params:{nationalId:item.nationalId}};
    	var data = {};
    	$http.post('../forCandidate/getCandidateInfo',data,config
    	).success(function(data, status, headers, config) {
//    		alert(data)
   		$scope.candidate = data;
    	}).error(function(data, status, headers, config) {
//    		alert("error");
    	});
    };
    
   
	var config = {params: {dataStr:'R1'}};
	var sendcondition = $scope.selections;
	$http.post('multiSearchOld', sendcondition, config
	).success(function(data, status, headers, config) {
		$scope.items = data;
	}).error(function(data, status, headers, config) {
//		alert("error");
	});
  
});
