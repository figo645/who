		app.controller('offerController', function($scope,$http,$filter,$rootScope) {
			$scope.showSelection=false;
			$scope.showSubnav = true;
			$scope.processName = "Talent Pool";
			$scope.selectUniversity = true;
			$scope.talentEmail = {};
			$scope.name = name.split("@",1);
			$scope.page = {
				allcheck:false,
				maxSize:5,//after 5 pages the pagination will be ellipsis
				bigCurrentPage:1// current page
			};

			$scope.tinymceOptions = {
                    inline: false,
                    statusbar: false,
                    menubar: true,
                    height:200,
                    // plugins: [
                    //     'advlist lists charmap anchor',
                    //     'searchreplace visualblocks',
                    //     'insertdatetime table paste code'
                    // ],
                    toolbar: [
                        'undo redo | bold italic underline | bullist numlist outdent indent'
                    ]
            };
			
  			
  			
			/*
			 * 整个流程图如下
			 * Status Process:
			 * 		{status:STATUS_NEW,	result:RESULT_UNKNOWN}
			 * 		{status:STATUS_Q1,	result:RESULT_UNKNOWN}
			 * 		{status:STATUS_Q2,	result:RESULT_UNKNOWN}
			 * 		{status:STATUS_Q3,	result:RESULT_UNKNOWN}
			 * 		{status:STATUS_Q4,	result:RESULT_PASS}
			 * 		{status:STATUS_Q5,	result:RESULT_PASS}
			 * Status Trash:
			 * 		{					result:RESULT_FAIL}
			 * 
  			 */
			$scope.status = {
				list:[
				    {name:'STATUS_ALL',clazz:'on',iclazz:'fa fa-angle-right fa-fw',title:'Talent Pool'},
					{name:'STATUS_NEW',clazz:'',iclazz:'fa fa-angle-right fa-fw',title:'Q0-Campus Talk'},
					{name:'STATUS_Q1',clazz:'',iclazz:'fa fa-angle-right fa-fw',title:'Q1-Paper Test'},
					{name:'STATUS_Q2',clazz:'',iclazz:'fa fa-angle-right fa-fw',title:'Q2-Group'},
					{name:'STATUS_Q3',clazz:'',iclazz:'fa fa-angle-right fa-fw',title:'Q3-Final Interview'},
					{name:'STATUS_Q4',clazz:'',iclazz:'fa fa-angle-right fa-fw',title:'Q4-Offer'},
					{name:'STATUS_Q5',clazz:'',iclazz:'fa fa-angle-right fa-fw',title:'Q5-Intern Orientation'},
					{name:'STATUS_TRASH',clazz:'',iclazz:'fa fa-trash fa-fw',title:'Trash'}
				]
			};
			// All Conditions
			$scope.condition = {
					id:'scope.condition',
					title:'STATUS_ALL',//Candidate status
					otherCondition: {
						university:[null,null],//null is essential here
						major:[null,null],
						candidateName:'',
						round1:[null,null],//null is essential here
						round2:[null,null],
						round3:[null,null],
						graduateDate:[null,null],
						talentPage:{
							dataIndex:0,
							dataTotal:194,
							pageIndex:1,
							pageLines:20,
							pageTotal:10
						}
					}
			}

			$scope.university_pro = [
			    {cities:'上海',universities:['上海中医药大学','上海交通大学','上海商学院','上海大学','上海对外经贸大学','上海工程技术大学','上海师范大学','上海应用技术大学','上海政法学院','上海海事大学','上海海关学院','上海海洋大学','上海理工大学','上海电力学院','上海电机学院','上海立信会计学院','上海第二工业大学','上海财经大学','上海金融学院','东华大学','华东师范大学','华东政法大学','华东理工大学','同济大学','复旦大学','上海纽约大学','上海外国语大学']},
			    {cities:'大连',universities:['东北财经大学','大连海事大学','大连理工大学','大连外国语大学','大连交通大学']},
			    {cities:'南京',universities:['东南大学','中国药科大学','南京中医院大学','南京信息工程大学','南京农业大学','南京医科大学','南京大学','南京审计学院','南京工业大学','南京师范大学','南京林业大学','南京航空航天大学','南京财经大学','南京邮电大学','河海大学']},
			    {cities:'苏州',universities:['苏州大学','西交利物浦大学']},
			    {cities:'杭州',universities:['中国美术学院','中国计量学院','杭州师范大学','杭州电子科技大学','浙江大学','浙江工业大学','浙江工商大学','浙江理工大学']},
			    {cities:'武汉',universities:['中南民族大学','中南财经政法大学','中国地质大学(武汉)','华中农业大学','华中师范大学','华中科技大学','武汉大学','武汉工程大学','武汉理工大学','武汉科技大学','湖北中医药大学','湖北大学','湖北工业大学']},
			    {cities:'宁波',universities:['宁波大学','宁波诺丁汉大学']},
			    {cities:'其他',universities:[]}
			];

			$scope.major = [
			    {division:'哲学',sections:['哲学类']},
			    {division:'经济学',sections:['经济学类']},
			    {division:'管理学',sections:['管理科学与工程类','工商管理类','行政管理、公共管理类','图书档案学类']},
			    {division:'文学',sections:['语言文学类','新闻传播学类','艺术类']},
			    {division:'工学',sections:['电气信息类','计算机科学与技术类','机械类','土建类','生物医学工程类','仪器仪表类','能源动力类','水利类','材料类','制药工程类','交通运输类','船舶与海洋工程类','测绘类','轻工纺织食品类','武器类','公安技术类','航空航天类']},
			    {division:'历史学',sections:['历史学类']},
			    {division:'法学',sections:['法学类']},
			    {division:'理学',sections:['数学类','物理学类','化学类及化学工程类','生物科学及生物技术类','天文地质地理类','力学类','电子信息科学类','系统科学类','环境科学与安全类']},
			    {division:'教育学',sections:['教育学类']},
			    {division:'医学',sections:['医学类','心理学类']},
			    {division:'农学',sections:['农业类']}
			];
			
			//mutiCondition
			$scope.condition.selections=[{
			 	id:'selectedPosition',
			 	title:'Selected position:',
			 	showinfo:true,
			 	selections:[
				 	{id:1,title:'Associate Software Engineer',check:null},
				 	{id:2,title:'Associate Testing Engineer',check:null},
				 	{id:3,title:'Associate Analyst',check:null},
				 	{id:4,title:'Associate UI Designer',check:null},
				 	{id:5,title:'PQA Intern',check:null},
				 	{id:6,title:'Admin Intern',check:null},
				 	{id:7,title:'HR Intern',check:null},
				 	{id:8,title:'Finance Intern',check:null}
			 	]
			},{
			 	id:'availableDay',
			 	title:'Availability:',
			 	showinfo:true,
			 	selections:[
			 		{id:'3',title:'Three days',check:null},
			 		{id:'4',title:'Four days',check:null},
			 		{id:'5',title:'Five days',check:null}
			 	]
			},{
			 	id:'graduatePlan',
			 	title:'Graduate Plan:',
			 	showinfo:false,
			 	selections:[
			 		{id:31,title:'Work',check:null}, 
			 		{id:12,title:'Go abroad',check:null},
			 		{id:13,title:'Civil servant',check:null},
			 		{id:14,title:'Graduate Studies',check:null},
			 		{id:32,title:'Other',check:null}
			 	]
			},{
			 	id:'languageProficiency',
			 	title:'Language proficiency:',
			 	showinfo:false,
			 	selections:[
			 		{id:15,title:'CET-4',check:null},
			 		{id:16,title:'CET-6',check:null},
			 		{id:17,title:'TEM-4',check:null},
			 		{id:18,title:'TEM-6',check:null},
			 		{id:19,title:'TEM-8',check:null},
			 		{id:20,title:'JPT-1',check:null},
			 		{id:21,title:'JPT-2',check:null},
			 		{id:22,title:'JPT-3',check:null},
			 		{id:23,title:'Other',check:null}
			 	]
			},{
			 	id:'gender',
			 	title:'Gender:',
			 	showinfo:false,
			 	selections:[
			 		{id:26,title:'Male',check:null},
			 		{id:27,title:'Female',check:null}
			 	]
			},{
			 	id:'degree',
			 	title:'Degree:',
			 	showinfo:false,
			 	selections:[
			 		{id:28,title:'Bachelor',check:null},
			 		{id:29,title:'Master',check:null},
			 		{id:30,title:'Doctor',check:null}
			 	]
			}];

			getTalent();

			$scope.updateState = function(name,title){
				$scope.condition.title = name;
				if(title == 'Trash'||title == 'Talent Pool'){
					$scope.processName = title;
				}else{
					$scope.processName = title.substring(3);
				}			
			    getTalent();
			}
			

			$scope.changeCandidateName = function(candidateName){
				$scope.condition.otherCondition.candidateName = candidateName;
				getTalent();			
			}
			
			$scope.changeGraduateDate = function(start,end){
				start = $filter('date')(start, 'yyyy-MM-dd');
				end = $filter('date')(end, 'yyyy-MM-dd');
				console.log(start,end);
				$scope.condition.otherCondition.graduateDate = [start, end];
				getTalent();			
			}
			
			$scope.changeScore1 = function(start,end){
				$scope.condition.otherCondition.round1 = [start, end];
				getTalent();			
			}
			$scope.changeScore2 = function(start,end){
				$scope.condition.otherCondition.round2 = [start, end];
				getTalent();			
			}
			$scope.changeScore3 = function(start,end){
				$scope.condition.otherCondition.round3 = [start, end];
				getTalent();			
			}

			//edit each talent
			$scope.edit = function(talent){
				talent.isEdit = !talent.isEdit;
				if(talent.isEdit == true){
					talent.oldQ1 = talent.q1;
					talent.oldQ2 = talent.q2;
					talent.oldQ3 = talent.q3;
				}else{
					talent.oldQ1 = null;
					talent.oldQ2 = null;
					talent.oldQ3 = null;
				}
				console.log($scope.talentDataSet);				
			}

			// select mutiCondition
			$scope.updateSelection = function(selection){
			    selection.check = !selection.check;
			    getTalent();
			}
			// show subnav on left side
			$scope.toggle = function() {
			    $scope.showSubnav = !$scope.showSubnav;
			}

			$scope.isCheck = function(talent){
			    var num = 0;
			    for(var i = 0;i<$scope.talentDataSet.talentDatas.length;i++){
			        if($scope.talentDataSet.talentDatas[i].check == true){
			            num++;
			        }                                    
			    }
			    if(num == $scope.talentDataSet.talentDatas.length){
			        $scope.page.allcheck = true;
			        
			    }else{
			        $scope.page.allcheck = false;
			       
			    }                
			}
			
			$scope.selectAll = function(){
				for(var i = 0;i<$scope.talentDataSet.talentDatas.length;i++){
					$scope.talentDataSet.talentDatas[i].check = $scope.page.allcheck == true ? true : false;										
				}
			}

			$scope.editAll = function(){
				for(var i = 0;i<$scope.talentDataSet.talentDatas.length;i++){
					$scope.talentDataSet.talentDatas[i].isEdit = true;
					$scope.talentDataSet.talentDatas[i].oldQ1 = $scope.talentDataSet.talentDatas[i].q1; 
					$scope.talentDataSet.talentDatas[i].oldQ2 = $scope.talentDataSet.talentDatas[i].q2;
					$scope.talentDataSet.talentDatas[i].oldQ3 = $scope.talentDataSet.talentDatas[i].q3;								
				}
			}
			
			$scope.deleteSelected = function(){
				$http.post('forHr/batchTotrash', $scope.talentDataSet)
        		.success(function(redata){
        			// alert(redata);
        			getTalent();
        	});	 
			}
//			
//			var Email_subject = new Array();
//			var Email_context = new Array();
//			Email_subject[0] ='dww';
//			Email_context[0] ='dwww';
//			
//			Email_subject[1] = 'Paper Test Invitation from PwC SDC Shanghai- 6/8/2016 (Wednesday) 14:45 p.m.';
//			Email_context[1] = '<div><p style="font-family: arial, sans-serif; font-size: 17.5px; color: #222222;" data-mce-style="font-family: arial, sans-serif; font-size: 17.5px; color: #222222;">Dear&nbsp;xxx<br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;">Thank you for your interest in PwC China Service Delivery Center.</span><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">&nbsp;</span><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;">We would like to invite you to attend the Paper Tset Interview for the position of&nbsp;<strong>Admin Intern</strong></span><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;">&nbsp;in Shanghai office. Below are the details for the interview:</span><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">&nbsp;</span><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;"><strong>Date:</strong> 6/8/2016 (Wednesday)</span><span style="font-family: Arial; font-size: small;" data-mce-style="font-family: Arial; font-size: small;">&nbsp;</span><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;">Time: 14:45 p.m.</span><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;"><strong>Venue:</strong> 上海市浦东新区金科路2889号长泰广场办公楼B座6楼PwC</span><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">&nbsp;</span><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;"><strong>Transportation:</strong> 地铁二号线至金科路站 4 号出口左转直走100米，上电梯即到。</span><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">&nbsp;</span><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><strong><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;">To Do List:</span><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">&nbsp;</span></strong><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;">1) Please reply to this email by stating "<u>&nbsp;I confirm that I will attend the interview at the PwC SDC office</u>".</span><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">&nbsp;</span></p><div style="font-family: arial, sans-serif; font-size: 17.5px; color: #222222;" data-mce-style="font-family: arial, sans-serif; font-size: 17.5px; color: #222222;"><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">2) Please send me an English CV along with the confirmation before June 4th 13:00 p.m.</span></div><div style="font-family: arial, sans-serif; color: #222222; font-size: 12.8px;" data-mce-style="font-family: arial, sans-serif; color: #222222; font-size: 12.8px;"><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;"><strong>You can find our location in the map below.</strong></span><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">&nbsp;</span></div></div>';
//			
//			Email_subject[2] ='Group Invitation from PwC SDC Shanghai- 6/8/2016 (Wednesday) 14:45 p.m.';
//			Email_context[2] ='<div><p style="font-family: arial, sans-serif; font-size: 17.5px; color: #222222;" data-mce-style="font-family: arial, sans-serif; font-size: 17.5px; color: #222222;">Dear&nbsp;xxx<br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;">Thank you for your interest in PwC China Service Delivery Center.</span><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">&nbsp;</span><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;">We would like to invite you to attend the Group Interview for the position of&nbsp;<strong>Admin Intern</strong></span><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;">&nbsp;in Shanghai office. Below are the details for the interview:</span><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">&nbsp;</span><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;"><strong>Date:</strong> 6/8/2016 (Wednesday)</span><span style="font-family: Arial; font-size: small;" data-mce-style="font-family: Arial; font-size: small;">&nbsp;</span><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;">Time: 14:45 p.m.</span><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;"><strong>Venue:</strong> 上海市浦东新区金科路2889号长泰广场办公楼B座6楼PwC</span><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">&nbsp;</span><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;"><strong>Transportation:</strong> 地铁二号线至金科路站 4 号出口左转直走100米，上电梯即到。</span><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">&nbsp;</span><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><strong><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;">To Do List:</span><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">&nbsp;</span></strong><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;">1) Please reply to this email by stating "<u>&nbsp;I confirm that I will attend the interview at the PwC SDC office</u>".</span><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">&nbsp;</span></p><div style="font-family: arial, sans-serif; font-size: 17.5px; color: #222222;" data-mce-style="font-family: arial, sans-serif; font-size: 17.5px; color: #222222;"><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">2) Please send me an English CV along with the confirmation before June 4th 13:00 p.m.</span></div><div style="font-family: arial, sans-serif; color: #222222; font-size: 12.8px;" data-mce-style="font-family: arial, sans-serif; color: #222222; font-size: 12.8px;"><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;"><strong>You can find our location in the map below.</strong></span><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">&nbsp;</span></div></div>';
//			
//			Email_subject[3] ='Group Invitation from PwC SDC Shanghai- 6/8/2016 (Wednesday) 14:45 p.m.';
//			Email_context[3] ='<div><p style="font-family: arial, sans-serif; font-size: 17.5px; color: #222222;" data-mce-style="font-family: arial, sans-serif; font-size: 17.5px; color: #222222;">Dear&nbsp;xxx<br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;">Thank you for your interest in PwC China Service Delivery Center.</span><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">&nbsp;</span><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;">We would like to invite you to attend the Final Interview for the position of&nbsp;<strong>Admin Intern</strong></span><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;">&nbsp;in Shanghai office. Below are the details for the interview:</span><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">&nbsp;</span><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;"><strong>Date:</strong> 6/8/2016 (Wednesday)</span><span style="font-family: Arial; font-size: small;" data-mce-style="font-family: Arial; font-size: small;">&nbsp;</span><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;">Time: 14:45 p.m.</span><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;"><strong>Venue:</strong> 上海市浦东新区金科路2889号长泰广场办公楼B座6楼PwC</span><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">&nbsp;</span><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;"><strong>Transportation:</strong> 地铁二号线至金科路站 4 号出口左转直走100米，上电梯即到。</span><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">&nbsp;</span><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><strong><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;">To Do List:</span><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">&nbsp;</span></strong><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;">1) Please reply to this email by stating "<u>&nbsp;I confirm that I will attend the interview at the PwC SDC office</u>".</span><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">&nbsp;</span></p><div style="font-family: arial, sans-serif; font-size: 17.5px; color: #222222;" data-mce-style="font-family: arial, sans-serif; font-size: 17.5px; color: #222222;"><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">2) Please send me an English CV along with the confirmation before June 4th 13:00 p.m.</span></div><div style="font-family: arial, sans-serif; color: #222222; font-size: 12.8px;" data-mce-style="font-family: arial, sans-serif; color: #222222; font-size: 12.8px;"><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;"><strong>You can find our location in the map below.</strong></span><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">&nbsp;</span></div></div>';
//			
//			Email_subject[4] ='Offer Invitation from PwC SDC Shanghai- 6/8/2016 (Wednesday) 14:45 p.m.';
//			Email_context[4] ='<div><p style="font-family: arial, sans-serif; font-size: 17.5px; color: #222222;" data-mce-style="font-family: arial, sans-serif; font-size: 17.5px; color: #222222;">Dear&nbsp;xxx<br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;">Thank you for your interest in PwC China Service Delivery Center.</span><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">&nbsp;</span><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;">We would like to invite you to attend the Offer Interview for the position of&nbsp;<strong>Admin Intern</strong></span><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;">&nbsp;in Shanghai office. Below are the details for the interview:</span><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">&nbsp;</span><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;"><strong>Date:</strong> 6/8/2016 (Wednesday)</span><span style="font-family: Arial; font-size: small;" data-mce-style="font-family: Arial; font-size: small;">&nbsp;</span><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;">Time: 14:45 p.m.</span><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;"><strong>Venue:</strong> 上海市浦东新区金科路2889号长泰广场办公楼B座6楼PwC</span><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">&nbsp;</span><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;"><strong>Transportation:</strong> 地铁二号线至金科路站 4 号出口左转直走100米，上电梯即到。</span><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">&nbsp;</span><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><strong><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;">To Do List:</span><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">&nbsp;</span></strong><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;">1) Please reply to this email by stating "<u>&nbsp;I confirm that I will attend the interview at the PwC SDC office</u>".</span><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">&nbsp;</span></p><div style="font-family: arial, sans-serif; font-size: 17.5px; color: #222222;" data-mce-style="font-family: arial, sans-serif; font-size: 17.5px; color: #222222;"><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">2) Please send me an English CV along with the confirmation before June 4th 13:00 p.m.</span></div><div style="font-family: arial, sans-serif; color: #222222; font-size: 12.8px;" data-mce-style="font-family: arial, sans-serif; color: #222222; font-size: 12.8px;"><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;"><strong>You can find our location in the map below.</strong></span><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">&nbsp;</span></div></div>';
//			
//			Email_subject[5] ='Intern Orientation Invitation from PwC SDC Shanghai- 6/8/2016 (Wednesday) 14:45 p.m.';
//			Email_context[5] ='<div><p style="font-family: arial, sans-serif; font-size: 17.5px; color: #222222;" data-mce-style="font-family: arial, sans-serif; font-size: 17.5px; color: #222222;">Dear&nbsp;xxx<br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;">Thank you for your interest in PwC China Service Delivery Center.</span><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">&nbsp;</span><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;">We would like to invite you to attend the Intern Orientation for the position of&nbsp;<strong>Admin Intern</strong></span><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;">&nbsp;in Shanghai office. Below are the details for the interview:</span><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">&nbsp;</span><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;"><strong>Date:</strong> 6/8/2016 (Wednesday)</span><span style="font-family: Arial; font-size: small;" data-mce-style="font-family: Arial; font-size: small;">&nbsp;</span><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;">Time: 14:45 p.m.</span><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;"><strong>Venue:</strong> 上海市浦东新区金科路2889号长泰广场办公楼B座6楼PwC</span><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">&nbsp;</span><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;"><strong>Transportation:</strong> 地铁二号线至金科路站 4 号出口左转直走100米，上电梯即到。</span><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">&nbsp;</span><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><strong><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;">To Do List:</span><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">&nbsp;</span></strong><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;">1) Please reply to this email by stating "<u>&nbsp;I confirm that I will attend the interview at the PwC SDC office</u>".</span><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">&nbsp;</span></p><div style="font-family: arial, sans-serif; font-size: 17.5px; color: #222222;" data-mce-style="font-family: arial, sans-serif; font-size: 17.5px; color: #222222;"><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">2) Please send me an English CV along with the confirmation before June 4th 13:00 p.m.</span></div><div style="font-family: arial, sans-serif; color: #222222; font-size: 12.8px;" data-mce-style="font-family: arial, sans-serif; color: #222222; font-size: 12.8px;"><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;"><strong>You can find our location in the map below.</strong></span><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">&nbsp;</span></div></div>';
//			
//			Email_subject[6] =' Application Update from PwC SDC Shanghai';
//			Email_context[6] ='<div><!--block-->Dear xxx <br><br>Thank you for your interest in exploring a career opportunity with PwC SDC, Shanghai. We appreciate the time you have devoted to our recruitment process.<br><br>Unfortunately, at this time we will not be pursuing your application further. However, we will retain your application details and contact you in the future as suitable openings arise.&nbsp;<br><br>Thank you once again for the interest shown in a career with PwC SDC, Shanghai and we wish you success for the future.<br><br>Campus Recruitment Team<br>PwC SDC, Shanghai&nbsp;</div><div><!--block--><br></div>';
//			
//			Email_subject[7] ='Inv- 6/8/2016 (Wednesday) 14:45 p.m.';
//			Email_context[7] ='<div><p style="font-family: arial, sans-serif; font-size: 17.5px; color: #222222;" data-mce-style="font-family: arial, sans-serif; font-size: 17.5px; color: #222222;">Dear&nbsp;xxx<br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;">Thank you for your interest in PwC China Service Delivery Center.</span><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">&nbsp;</span><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;">We would like to invite you to attend the Face-to-face Interview for the position of&nbsp;<strong>Admin Intern</strong></span><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;">&nbsp;in Shanghai office. Below are the details for the interview:</span><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">&nbsp;</span><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;"><strong>Date:</strong> 6/8/2016 (Wednesday)</span><span style="font-family: Arial; font-size: small;" data-mce-style="font-family: Arial; font-size: small;">&nbsp;</span><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;">Time: 14:45 p.m.</span><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;"><strong>Venue:</strong> 上海市浦东新区金科路2889号长泰广场办公楼B座6楼PwC</span><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">&nbsp;</span><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;"><strong>Transportation:</strong> 地铁二号线至金科路站 4 号出口左转直走100米，上电梯即到。</span><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">&nbsp;</span><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><strong><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;">To Do List:</span><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">&nbsp;</span></strong><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;">1) Please reply to this email by stating "<u>&nbsp;I confirm that I will attend the interview at the PwC SDC office</u>".</span><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">&nbsp;</span></p><div style="font-family: arial, sans-serif; font-size: 17.5px; color: #222222;" data-mce-style="font-family: arial, sans-serif; font-size: 17.5px; color: #222222;"><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">2) Please send me an English CV along with the confirmation before June 4th 13:00 p.m.</span></div><div style="font-family: arial, sans-serif; color: #222222; font-size: 12.8px;" data-mce-style="font-family: arial, sans-serif; color: #222222; font-size: 12.8px;"><br style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;"><span style="font-family: sans-serif; font-size: small;" data-mce-style="font-family: sans-serif; font-size: small;"><strong>You can find our location in the map below.</strong></span><span style="font-size: 12.8px;" data-mce-style="font-size: 12.8px;">&nbsp;</span></div></div>';
//			
//			
			
			
			$scope.saveemail=function(talentEmail){
				var emailconfig ={};
				console.log(talentEmail);
				emailconfig.emailContextTemplate= talentEmail.context;
				emailconfig.emailSubjectTemplate = talentEmail.subject;
				emailconfig.emailStatusTemplate = $scope.condition.title;
				
				$http.post('forHr/updateEmailTemplate',emailconfig)
        		.success(function(redata){
        			if(redata== "success"){
        				change_dialog_error('Save success');
						show_dialog_error();
        			}
        		})
        		.error(function(){
        			change_dialog_error('Save failed');
					show_dialog_error();
    				}
    			);
			}
//			
			$scope.InitEmail = function(){
				var talentDatas = $scope.talentDataSet.talentDatas;
				var selected = false;
				for ( var i in talentDatas) {
					if (talentDatas[i].check == true) {
						selected = true;						
					}
				}
				if (selected) {					
					$scope.config={};
					console.log($scope.condition);
					var emailStatusTemplate = $scope.condition.title;
					$http.post('forHr/getEmailTemplate',emailStatusTemplate )
	        		.success(function(redata){
	        			$scope.talentEmail.subject = redata.emailSubjectTemplate;
						$scope.talentEmail.context = redata.emailContextTemplate;
	        	});	
					$('#exampleModal').modal('show');
				} else {
					change_dialog_error('At least one record is needed');
					show_dialog_error();
				}
				

			}
			
			$scope.sendmodelemail = function(emailSubject) {
								
				// $scope.talentEmail.context = $("#trix-input-1").val();
				
				$scope.talentEmail.talentDatas = $scope.talentDataSet.talentDatas;
				//$scope.talentEmail.subject = emailSubject;
//				console.log($scope.test);
//				$scope.attachments = {};
				$scope.talentEmail.attachment = new FormData($("#uploadattachment")[0]);
				console.log($scope.talentEmail.attachment);
//				alert($("#uploadattachment"));
				console.log($scope.talentEmail);
				show_dialog_loading();
				
				$http({
					method:'POST',
					url:'forHr/sendattachment',
					//file:$scope.talentEmail.attachment,

					  data: $scope.talentEmail.attachment,
					  headers: {'Content-Type':undefined}
				}).success(function(redata) {
					$scope.talentEmail.attachment = redata;
					$http.post('forHr/sendemail', $scope.talentEmail)
	        		.success(function(redata){
	        			if(redata == "success"){
	        				hide_dialog_loading();
	        				change_dialog_error('send success');
							//show_dialog_error();
							show_dialog_error();
	        			}else{
	        				hide_dialog_loading();
	        				change_dialog_error('send error');
							show_dialog_error();
	        			}
	        			
		        	}).error(function(){
		        		hide_dialog_loading();
		        		change_dialog_error('send error');
						show_dialog_error();
		        	});
					console.log(redata);
					  });
				//$('#exampleModal').modal('hide');
			}
			
			
			$scope.pageChanged = function() {
				$scope.condition.otherCondition.talentPage.pageIndex = angular.copy($scope.page.bigCurrentPage);			
				$http.post('forHr/multiSearch', $scope.condition)
	        		.success(function(redata){
	        			getTalent();
	        	});
			};
			
			// shuffle
			$scope.shuffleData = function() {
				$scope.talentDataSet.talentDatas = shuffle($scope.talentDataSet);
			}
			// export
			$scope.exportCandidateZip = function() {
				exportTo("/wechat/exportCandidateZip?nationalIds=");
			}
			$scope.exportPrettyCandidateZip = function() {
				exportTo("/wechat/exportPrettyCandidateZip?nationalIds=");
			}
			$scope.exportExcel = function() {
				exportTo("/wechat/exportExcel?nationalIds=");
			}
			$scope.exportQuestionnaireZip = function() {
				exportTo("/wechat/exportQuestionnaireZip?nationalIds=");
			}
			// save
			$scope.saveData = function() {
				for(var i = 0;i<$scope.talentDataSet.talentDatas.length;i++){
				if($scope.talentDataSet.talentDatas[i].isEdit == true){
					$scope.talentDataSet.talentDatas[i].isEdit = false;
					$scope.talentDataSet.talentDatas[i].q1 = $scope.talentDataSet.talentDatas[i].oldQ1;
					$scope.talentDataSet.talentDatas[i].q2 = $scope.talentDataSet.talentDatas[i].oldQ2;
					$scope.talentDataSet.talentDatas[i].q3 = $scope.talentDataSet.talentDatas[i].oldQ3;			
				}
				}
				console.log($scope.talentDataSet);
				$http.post('forHr/saveTalentData', $scope.talentDataSet)
	        		.success(function(redata){
	        			// alert(redata);
	        			getTalent();
	        	});
			}
			// roundMove
			$scope.roundMove = function() {
				$http.post('forHr/roundMoveTalentData', $scope.talentDataSet)
	        	.success(function(redata){
	        		console.log(redata);
	        		getTalent();
	        	});

			}
			$scope.moveBack = function(){
				$http.post('forHr/moveBackTalentData', $scope.talentDataSet)
	        	.success(function(redata){
	        		 // alert(redata);
	        		getTalent();
	        	});
			}
			//delete
			$scope.deleteData = function(talent){
				$http.post('forHr/toTrash', talent)
	        		.success(function(redata){
	        			// alert(redata);
	        			getTalent();
	        	});	        	
			}
			
			//recover
			$scope.recoverData = function(talent){
				$http.post('forHr/recoverFromTrash', talent)
					.success(function(redata){
						// alert(redata);
	        			getTalent();
					})
			}

			$scope.submitHrComment = function(){
				getStarLevel();
				$scope.hrComment = angular.copy($scope.historyHrComment);
				$scope.hrComment.earliestEntry = $filter('date')($scope.hrComment.earliestEntry, 'yyyy-MM-dd');
				console.log($scope.hrComment);
				$http.post('forHr/saveHrComment', $scope.hrComment)
					.success(function(data) {
						// alert("success");
						$('#myModal').modal('hide');
					})
				console.log($scope.hrComment);
			}

			$scope.cancelComment = function(){
				$scope.historyHrComment = {};
			}
			$scope.changeCity = function(){
				if($scope.university_pro.cities==null){
					$scope.selectUniversity = true;
					$scope.condition.otherCondition.university = [null,null];
					getTalent();
				}else if($scope.university_pro.cities.cities=='其他'){
					$scope.selectUniversity = false;
					$scope.condition.otherCondition.university = [$scope.university_pro.cities.cities,null];
				}else{
					$scope.selectUniversity = true;
					$scope.condition.otherCondition.university = [$scope.university_pro.cities.cities,null];
				}
				getTalent();
			}
			$scope.searchUniversity = function(){
				console.log($scope.condition.otherCondition.universitytmp);
				if($scope.condition.otherCondition.universitytmp == ""){
					$scope.condition.otherCondition.university = [$scope.university_pro.cities.cities,null];
				}else{
					$scope.condition.otherCondition.university = [$scope.university_pro.cities.cities,$scope.condition.otherCondition.universitytmp];
				}
				getTalent();
			}
			$scope.changeClass = function(){
				if($scope.major.division == null){
					$scope.condition.otherCondition.major = [null,null];
				}else{
					$scope.condition.otherCondition.major = [$scope.major.division.division,null];
				}
				getTalent();
			}
			$scope.changeUniversity = function(){
				if($scope.condition.otherCondition.universitytmp==null){
					$scope.condition.otherCondition.university = [$scope.university_pro.cities.cities,null];
				}else{
					$scope.condition.otherCondition.university = [$scope.university_pro.cities.cities,$scope.condition.otherCondition.universitytmp];
				}
				getTalent();
			}
			$scope.changeSection = function(){
				if($scope.condition.otherCondition.majortmp==null){
					$scope.condition.otherCondition.major = [$scope.major.division.division,null];
				}else{
					$scope.condition.otherCondition.major = [$scope.major.division.division,$scope.condition.otherCondition.majortmp];
				}
				getTalent();			
			}
			$scope.commentData = function(talent){
				angular.element('.stars li a').removeClass('active');
				$http.post('forHr/getHrComment', talent)
					.success(function(data) {
						console.log(data);
						data.offer = data.offer == null ? null : data.offer ? 'true' : 'false';
						data.postgraduate = data.postgraduate == null ? null : data.postgraduate ? 'true' : 'false';
						$scope.historyHrComment = data;
						setStarLevel($scope.historyHrComment.appearance,"Appearance");
						setStarLevel($scope.historyHrComment.demeanour,"Demeanour");
						setStarLevel($scope.historyHrComment.english,"English");
					})						
			}
			
			$scope.importExcel = function() {
				checkFileFormat();
			}
			$scope.ToggleSelection = function(){
				$scope.showSelection = !$scope.showSelection;
				$scope.condition.selections[2].showinfo = !$scope.condition.selections[2].showinfo;
				$scope.condition.selections[3].showinfo = !$scope.condition.selections[3].showinfo;
				$scope.condition.selections[4].showinfo = !$scope.condition.selections[4].showinfo;
				$scope.condition.selections[5].showinfo = !$scope.condition.selections[5].showinfo;
			}
			$scope.showcandidateInfo = function(talent){
				$scope.infoState = 1;
				$scope.languageLevelPreview = [];
				$http.post('forHr/getCandidateAllInfo', talent)
					.success(function(data) {
						$scope.candidateinfo = data;
						$scope.languageLevel = $scope.candidateinfo.candidate.languageLevel.split(",");
						for(var i=0;i<=$scope.languageLevel.length-1;i++){
							if($scope.languageLevel[i] != "") {
								$scope.languageLevelPreview.push($scope.languageLevel[i]);
							};
						}
						if($scope.languageLevel[7]!='' && $scope.languageLevel[7]){
							$scope.languageLevelPreview.push($scope.languageLevel[7]);
						}
						$scope.candidateinfo.candidate.languageLevel = $scope.languageLevelPreview;
					})	
			}
			$scope.changeState_Info = function(state){
				$scope.infoState =state;
			}
			function getStarLevel(){
				$scope.historyHrComment.appearance = angular.element('#Appearance li .active').html();
				$scope.historyHrComment.demeanour = angular.element('#Demeanour li .active').html();
				$scope.historyHrComment.english = angular.element('#English li .active').html();
			}

			function setStarLevel(level,name){
				switch(level){
					case 0:
						break;
					case 1:
						$('#'+name).find(".one").trigger("click");
						break;
					case 2:
						$('#'+name).find(".two").trigger("click");
						break;
					case 3:
						$('#'+name).find(".thr").trigger("click");
						break;
					case 4:
						$('#'+name).find(".fou").trigger("click");
						break;
					case 5:
						$('#'+name).find(".fiv").trigger("click");
						break;
				}
			}
			// get talent information
			function getTalent(){				
				var sendcondition = $scope.condition;
				console.log($scope.condition);
	        	$http.post('forHr/multiSearch', sendcondition)
	        		.success(function(redata){
	        	 		console.log(redata);
	        	 		$scope.talentDataSet = redata;
	        	 		//$scope.talentEmail = redata;
	        	 		$scope.bigTotalItems = $scope.talentDataSet.talentPage.dataTotal; // init pages when refresh talentData
	        	 		// $scope.all = false;
	        	 		$scope.page.allcheck = false;
	        	});
			}
			
			// exportTo function
			// get select nationalIds
			function exportTo(address){
				var talentDatas = $scope.talentDataSet.talentDatas;
				var nationalIds = new Array(talentDatas.length);
				var selected = false;
				for ( var i in talentDatas) {
					if (talentDatas[i].check == true) {
						selected = true;
						nationalIds[i] = talentDatas[i].nationalId;
					}
				}
				if (selected) {
					window.location.href = address + nationalIds;
				} else {
					change_dialog_error('At least one record is needed');
					show_dialog_error();
				}
			}
			
			//shuffle
			function shuffle(talentData){ //v1.0
				for(var j, x, i = talentData.length; i; j = parseInt(Math.random() * i), x = talentData[--i], talentData[i] = talentData[j], talentData[j] = x);
				return talentData;
			};	
			
			// doUpload
			function doUpload() {
				var formData = new FormData($("#uploadForm")[0]);
				$.ajax({
					url : '/wechat/uploadExcel',
					type : 'POST',
					data : formData,
					async : false,
					cache : false,
					contentType : false,
					processData : false,
					success : function(failNum) {
						getTalent();
						$('#importModal').modal('hide');
						if(failNum==0){
							change_dialog_error('Success');
							show_dialog_error(); 
						}
						else{
							change_dialog_error('Success&nbsp&nbsp'+failNum+'&nbsp record ignored');
							show_dialog_error();
						}
					},
					error : function(returndata) {
						console.log(returndata);
						change_dialog_error('Fail to import Excel');
						show_dialog_error(); 
					}
				});
			}
			//checkFileFormat
			function checkFileFormat(){
				var file = document.getElementById("importExcel");
				var filename=file.value; 
				var filetype= filename.substring(filename.indexOf(".")); ;
				if (filetype==""){ 
					change_dialog_error('can\'t find upload file');
					show_dialog_error();  
					}
				else if(filetype!=".xlsx"){
					change_dialog_error('upload file must be \'xlsx\'');
					show_dialog_error(); 
				}
				else{
					doUpload();
				}
			}
			
			
		});
				
		app.directive('navFocus', function(){
			return {
				restrict: 'A', 
				link: function(scope, element, attrs) {
					element.on('click','a',function(){
						var _this=$(this);
						if(!_this.next().hasClass('nav-second-level')){
							$('.sidebar-collapse').find('li').removeClass('on');
							_this.parent('li').addClass('on');
						}
					})				
				}
			};
		});
		
		
//		$(document).ready(function(){
//			
//			$("#input").val('<span class="im" style="color: rgb(80, 0, 80); font-family: arial, sans-serif; font-size: 17.5px;">Dear xxxqwdqdwd<br style="font-size: 12.8px;"><br style="font-size: 12.8px;"><font face="sans-serif" size="2">Thank you for your interest in PwC China Service Delivery Center.</font><span style="font-size: 12.8px;">&nbsp;</span><br style="font-size: 12.8px;"><font face="sans-serif" size="2">We would like to invite you to attend the Face-to-face Interview for the position of<strong>&nbsp;&nbsp;Admin Intern</strong></font><font face="sans-serif" size="2">&nbsp;in Shanghai office. Below are the details for the interview:</font><span style="font-size: 12.8px;">&nbsp;</span><br style="font-size: 12.8px;"><font face="sans-serif" size="2">Date: 6/8/2016 (Wednesday)</font><font face="Arial" size="2">&nbsp;</font><font face="sans-serif" size="2">Time: 14:45 p.m.</font><br style="font-size: 12.8px;"></span><span class="im" style="color: rgb(80, 0, 80); font-family: arial, sans-serif; font-size: 17.5px;"><font face="sans-serif" size="2">Venue: 上海市浦东新区金科路2889号长泰广场办公楼B座6楼PwC</font><span style="font-size: 12.8px;">&nbsp;</span><br style="font-size: 12.8px;"><font face="sans-serif" size="2">Transportation: 地铁二号线至金科路站 4 号出口左转直走100米，上电梯即到。</font><span style="font-size: 12.8px;">&nbsp;</span><br style="font-size: 12.8px;"><br style="font-size: 12.8px;"></span><span class="im" style="color: rgb(80, 0, 80); font-family: arial, sans-serif; font-size: 17.5px;"><font face="sans-serif" size="2">To Do List:</font><span style="font-size: 12.8px;">&nbsp;</span><br style="font-size: 12.8px;"><br style="font-size: 12.8px;"><font face="sans-serif" size="2">1) Please reply to this email by stating "<span style="text-decoration: underline;">&nbsp;I confirm that I will attend the interview at the PwC SDC office</span>".</font><span style="font-size: 12.8px;">&nbsp;</span></span>');	
//		})