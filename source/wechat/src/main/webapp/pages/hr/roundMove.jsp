<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html>
<html ng-app="offerModule">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="/wechat/resources/images/icon/favicon.ico">
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<link rel='stylesheet' type="text/css" href="<c:url value="/resources/css/font.css"/>"/>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/font-awesome.min.css"/>"/>
<link rel='stylesheet' type="text/css" href="<c:url value="/resources/css/bootstrap.min.css"/>"/>
<link rel='stylesheet' type="text/css" href="<c:url value="/resources/css/bootstrap-datetimepicker.min.css"/>"/>
<link rel='stylesheet' type="text/css" href="/wechat/resources/css/candidate.css"/>
<link rel='stylesheet' type="text/css" href="/wechat/resources/css/weui.min.css" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/roundmove.css"/>"/>
<link rel='stylesheet' type="text/css"  href="<c:url value="/resources/css/report.css"/>" />
<link rel="stylesheet" type="text/css" href="<c:url value ="/resources/css/style_email.css"/>"/>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/importExcelDropify.min.css"/>"/>
<link rel='stylesheet' type="text/css" href="/wechat/resources/css/trix_normalize.css" />
<link rel='stylesheet' type="text/css" href="/wechat/resources/css/stylesheet.css" />
<link rel='stylesheet' type="text/css" href="/wechat/resources/css/trix.css" />
<link rel='stylesheet' type="text/css" href="/wechat/resources/css/github-light.css" />
<script>
   	var name = "<sec:authentication property="name" htmlEscape="false"/>"
</script>
<title>list</title>
</head>
<body ng-controller="offerController">

	<!-- top-nav -->
  <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle pull-left" data-toggle="collapse" data-target=".sidebar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand">
            <img src="/wechat/resources/images/logo.png">
            <span class="">Talent Pool</span>
          </a>
        </div>
        <ul class="nav navbar-right navbar-signout">
          <li><a href="/wechat/logout">Sign out</a></li>

        </ul>
    </div>
	<!-- side-nav -->
        <div class="side-content">
        <nav nav-focus id="menu" class="navbar-default navbar-fixed-side side_menu" role="navigation">
            <div class="sidebar-collapse collapse in">
                <ul class="nav" id="side-menu">
                    <li class="sidebar-user">
                        <div class="user-img">
                            <span class="img-circle">
                                <img id="avatar"  class="dropdown img-circle img-responsive" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">   
                                <ul class="dropdown-menu changehrinfo" aria-labelledby="dLabel">
                                    <li>
                                    <a data-toggle="modal" style="color:black" href="#" data-target="#Modal_newpwd" onclick="changepasswordInit()">Change password</a>
                                    </li>
                                    <li>
                                    <a style="color:black" href="#" data-toggle="modal" data-target="#Modal_avatar">Upload Avatar</a>
                                    </li>
                                </ul> 
                            </span>
                        </div>                  
                        <div class="user-info">
                            <div class="user-name" ng-bind="name"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></div>
                             <sec:authorize access="hasRole('ROLE_HRM')">
                                <a class="manager-hyperlink" ui-sref="newhr">manager</a>
                            </sec:authorize>
                        </div>
                        
                    </li>
                    <li class="active">                     
                        <a href="javascript:;" ng-click="toggle()" data-target="#process" data-toggle="collapse">
                            <i class="fa fa-bars fa-fw"></i>
                            Recruitment Process
                            <i class="fa fa-angle-right angle-right" ng-show="!showSubnav"></i>
                            <i class="fa fa-angle-down angle-down" ng-show="showSubnav"></i>
                        </a>
                        <!-- Q0 -- Q5 -->
                        <ul id="process" class="nav nav-second-level collapse in">
                            <li ng-repeat="state in status.list" class="{{state.clazz}}" ng-click="updateState(state.name,state.title)">
                                <a ui-sref="process">
                                    <i class="{{state.iclazz}}"></i>
                                    {{state.title}}
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li ui-sref-active="on" id="sheets">
                        <a ui-sref="sheets" >
                            <i class="fa fa-file-text fa-fw"></i>
                            Sheets
                        </a>
                    </li>
                    <li ui-sref-active="on" id="candidate">
                    <a ui-sref="addcandidate.basic" >
                        <!-- <a href="html/candidate/candidate.html" > -->
                            <i class="fa fa-user-plus fa-fw"></i>
                            Add Candidate
                        </a>
                    </li>
                    <li>
                        <a href="http://208.175.209.70:8983/survey/Admin_LogIn.jsp" >
                            <i class="fa fa-pencil-square-o fa-fw"></i>
                            Online Survey
                        </a>
                    </li>
                    <li ui-sref-active="on" id="help">
                        <a ui-sref="help" >
                            <i class="fa fa-question-circle fa-fw"></i>
                            Help
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</div>
   
	<div class="content" ui-view></div>
	
	<!-- /.container -->
    <!-- changehrinfo -->
<div ng-controller="hrInfoController">
<div class="modal fade" id="Modal_newpwd" tabindex="-1" role="dialog"
        aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true">&times;</button>
                    <h3 class="modal-title" id="myModalLabel">
                        <font color="#FF8000">Hello </font>
                        <span style="color:#FF8000" ng-bind="user.userName"></span>
                    </h3>
                </div>
                <form name="newHrForm">
                    <div class="modal-body">
                        <ul class="changePwd_ul">
                            <li class="changePwd_head"><div class="title">Password rules: 8 to 16 characters must contain at least one of each of
                                    the following characters</div>
                                <div class="title">(1) Upper and lower case letter</div>
                                <div class="title">(2) Number and special characters e.g: !,@,$,~,%</div>
                            </li>
                            <li class="changePwd_content">
                                <div class="col-md-4"><span for="initialPassword">*Last password:</span></div>
                                <div class="col-md-4">
                                <input ng-model="user.password" ng-change="verifyPassword()" ng-bind="verifyPw" ng-style="newpwdstyle" type="password" autofocus="autofocus" /></div>
                                <div class="col-md-4"><span ng-show="showValid" ng-bind = "validLabel">correct</span></div>
                            </li>
                            <li class="changePwd_content">
                                <div class="col-md-4"><span for="initialPassword">*New password:</span></div>
                                <div class="col-md-4"><input ng-model="user.passwordNew" ng-bind="checkPw" type="password" ng-style="pwstyle" ng-change="checkPassword()" autofocus="autofocus"/></div>
                                <div class="col-md-4"><span for="initialPassword" ng-show="showpw" ng-bind="pwlabel"></span></div>
                            </li>
                            <li class="changePwd_content">
                                <div class="col-md-4"><span for="confirmPassword">*Confirm password:</span></div>
                                <div class="col-md-4"><input ng-model="confirmpw" ng-bind="checkConfirm" ng-style="confirmstyle" type="password" ng-change="confirmPw()" autofocus="autofocus" /></div>
                                <div class="col-md-4"><span for="confirmPassword" ng-show="showconfirm">please confirm password</span></div>
                            </li>
                        </ul>
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel
                    </button>
                        <button type="button" class="btn btn-warning" data-toggle="modal"
                        ng-click="changePassword()"
                        ng-disabled="!(checkPw&&checkConfirm&&verifyPw)"
                        data-target="#Modal_save_success" data-dismiss="modal">Complete</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal -->
    </div>
    
     <div class="modal fade" id="Modal_save_success" tabindex="-1" role="dialog"
        aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true">&times;</button>
                    <h3 class="modal-title" id="myModalLabel">
                        <font color="#FF8000">New Password</font>
                    </h3>
                </div>
                <div>
                    <br>
                    <h4 class="modal-title" id="myModalLabel" align="center">
                        <font>Saved!</font>
                    </h4>
                    <br>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close
                    </button>
                </div>
            </div>
        </div>
    </div>
         <div class="modal fade" id="Modal_avatar" tabindex="-1" role="dialog"
        aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true">&times;</button>
                    <h3 class="modal-title" id="myModalLabel">
                        <font color="#FF8000">Upload Avatar</font>
                    </h3>
                </div>
                <div class="modal-body">
                <div style="color:red;text-align: center">The picture must be less than 1M!</div><br>
                    <form id="avatarForm" enctype="multipart/form-data">
                    <input type = "file" file-model="myFile" class="dropify" data-default-file=""/>                             
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal" ng-click="uploadAvatar()">Upload</button>
                </div>
            </div>
        </div>
    </div>
    </div>
    
    
<!-- /changehrinfo -->

	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<!--<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>-->

</body>
<script type="text/javascript" src="<c:url value="/resources/js/jquery-1.11.3.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
<script type="text/javascript" src="/wechat/resources/js/tinymce-dist/tinymce.js"></script>
<script type="text/javascript" src="<c:url value="/resources/js/angular.min.js"/>"></script>
<script type="text/javascript" src="resources/js/jquery.nicescroll.js"></script>
<script type="text/javascript" src="<c:url value="/resources/js/angular-animate.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/ui-bootstrap-tpls-1.3.3.min.js"/>"></script>
<script type="text/javascript" src="resources/js/angular-ui-router.js"></script>
<script type="text/javascript" src="<c:url value="/resources/js/prettify.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/multiselect.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/multiselect.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.jqprint.js"/>"></script>
<script type="text/javascript" src="/wechat/resources/js/jquery-migrate.js"></script>
<script type="text/javascript" src="resources/js/tinymce.js"></script>
<script type="text/javascript" src="<%=path%>/resources/js/router.js"></script>
<script type="text/javascript" src="<%=path%>/resources/js/recruitmentProcess.js"></script>
<script type="text/javascript" src="<%=path%>/resources/js/report.js"></script>
<script type="text/javascript" src="/wechat/resources/js/candidate.js" charset="utf-8"></script>
<script type="text/javascript"	src="<c:url value="/resources/js/authority.js"/>"></script> 
<script type="text/javascript" src="/wechat/resources/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="/wechat/resources/js/errorinfo.js"></script>
<script type="text/javascript" src="<c:url value="/resources/js/dropify.min.js"/>"></script>
<script type="text/javascript" src="/wechat/resources/js/ui-bootstrap-tpls-1.3.3.min.js"></script>
<script type="text/javascript" src="/wechat/resources/js/hrInfo.js"></script>

<script type="text/javascript" src="/wechat/resources/js/trix.js"></script>
<script type="text/javascript" src="/wechat/resources/js/elastic.js"></script>
<script type="text/javascript" src="/wechat/resources/js/angular-trix.js"></script>
<script type="text/javascript" src="/wechat/resources/js/democtrl.js"></script>

<script type="text/javascript">
		$(function () {
            $( '#menu' ).niceScroll({
            cursorcolor: '#ccc',
            railalign: 'right', 
            cursorborder: "none", 
            horizrailenabled: false, 
            zindex: 2001, 
            cursoropacitymax: 1,
            // autohidemode:'false',
            spacebarenabled: false 
        });
    		$(window).bind("resize", function() {
    		     if ($(this).width() < 768) {
    		        $('div.sidebar-collapse').addClass('collapse')
    		    } else {
    		        $('div.sidebar-collapse').removeClass('collapse')
    		    }
    		});
    		$('.navbar-toggle').on('click', function () {
    		    if ($('#menu.hidden-xs').length){
    		        $('#menu').removeClass('hidden-xs');
    		    }
    		    else{
    		        $('#menu').addClass('hidden-xs');
    		    }
    		});
  		});
  		var config = {
            params : {
                source : 'HRAddCandidate'
            }
        };
</script>
</html> 