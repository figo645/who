<%@ page language="java" contentType="text/html" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Language" content="zh-cn" />
<meta http-equiv="description" content="Wechat Web App">
<meta name="Copyright" content="pwc" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel='stylesheet' type="text/css" href="<c:url value="/resources/css/bootstrap.css"/>"/>
<link rel='stylesheet' type="text/css" href="<c:url value="/resources/css/bootstrap-theme.css"/>"/>
<link rel='stylesheet' type="text/css" href="<c:url value="/resources/css/bootstrap-datetimepicker.min.css"/>"/> 
<script type="text/javascript" src="<c:url value="/resources/js/jquery-1.11.3.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap-datetimepicker.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/angular.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.tablesorter.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/hideshow.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery-1.5.2.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.equalHeight.js"/>"></script>