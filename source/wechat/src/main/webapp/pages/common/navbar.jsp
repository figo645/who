<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<nav class="navbar navbar-default navbar-fixed-top navbar-inverse glassFilter" role="navigation">
	<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<%-- <img src="<c:url value="/resources/pic/XXX.png"/>" alt="Pwc"
				width=100px> --%>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li><a href="<c:url value="/"/>"> 首页</a> <!-- class="active" --></li>
				<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">导航 <span
						class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="<c:url value="#aaa"/>">#aaa</a></li>
						<li><a href="<c:url value="#bbb"/>">#bbb</a></li>
						<li class="divider"></li>
						<li><a href="<c:url value="/"/>">YYY</a></li>
						<li><a href="<c:url value="/"/>">YYY</a></li>
						<li class="divider"></li>
						<li><a href="<c:url value="/"/>">YYY</a></li>
					</ul></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="#myModal" data-toggle="modal" data-target="#myModal">右边</a></li>
			</ul>
		</div>
		<!-- /.navbar-collapse -->
	</div>
	<!-- /.container-fluid -->
</nav>
</html>