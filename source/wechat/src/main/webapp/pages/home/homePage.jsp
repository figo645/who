<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html data-ng-app="searchApp" data-ng-controller="searchCtrl">
<head>
<link rel="icon" href="/wechat/resources/images/icon/favicon.ico">
<%@ include file="/pages/common/head.jsp"%>
<title>Insert title here</title>
</head>
<body>
<jsp:include page="/pages/common/navbar.jsp"></jsp:include>
<br>
<br>
<br>
<h1>Search city by id : {{yourId || 'Number'}}</h1>
<br> Your name: <input type="text" data-ng-model="yourId" placeholder="Number">
<button data-ng-click="dataSearch()">Search</button>
<h2>{{status}} </h2>
<h2>{{score}}</h2>
<div data-ng-controller="update">
Your ID:<input type="text" data-ng-model="id">
Your status:<input type="text" data-ng-model="status">
Your score: <input type="text" data-ng-model="Round_1_Score">
<button data-ng-click="commit()">Commit</button>
</div>
<script type="text/javascript">
var app = angular.module('searchApp', []);
app.controller('searchCtrl', function($scope,$http) {
	$scope.dataSearch = function() {
    	var postData = {text:'long blob of text'};
    	var config = {params: {Candidate_id_card: $scope.yourId }};
    	$http.post('forHr/selectCandidateInfo', postData, config
    	).success(function(data, status, headers, config) {
    		$scope.status = data.status;
    		$scope.score = data.round_1_Score;
    	}).error(function(data, status, headers, config) {
    		alert("error");
    	});
    };
});


app.controller('update', function($scope,$http) {
	$scope.commit = function() {
    	var postData = {text:'long blob of text'};
    	var config = {params: {Candidate_id_Card:$scope.id,status:$scope.status,Round_1_Score:$scope.Round_1_Score}};
    	$http.post('forHr/updateCandidateInfo', postData, config
    	).success(function(data, status, headers, config) {
    		alert("success");
    	}).error(function(data, status, headers, config) {
    		alert("error");
    	});
    };
});
</script>
</body>
</html>