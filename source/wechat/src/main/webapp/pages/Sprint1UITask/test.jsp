<!-- test 123-->
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel='stylesheet' type="text/css" href="<c:url value="/resources/css/bootstrap.css"/>"/>
<link rel='stylesheet' type="text/css" href="<c:url value="/resources/css/bootstrap-theme.css"/>"/>
<link rel='stylesheet' type="text/css" href="<c:url value="/resources/css/bootstrap-datetimepicker.min.css"/>"/>
<script type="text/javascript" src="<c:url value="/resources/js/jquery-1.11.3.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap-datetimepicker.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/angular.min.js"/>"></script> 
<title>Insert title here</title>
</head>
<body>
<!-- hjhjhjhj -->
<body>
<!--
	<nav class="navbar navbar-inverse navbar-fixed-top white">
	
		<div class="container">
		
			<div class="navbar-header">
			
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<div>
				&nbsp;&nbsp;&nbsp;<img src="images/footer_PWC_Logo_55_42.png">
				</div>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					
					<li><a href="#personalinformation"><b>个人信息</b></a></li>
					<li><a href="#jobinformation"><b>职位信息</b></a></li>
				</ul>
			</div>
			/.nav-collapse 
		</div>
	</nav>
  -->
	<div class="container">	
		<div class="page-header" style="text-align:center; margin: 10px 0 20px;">
			<h3>实习生申请表</h3>
			<h4>普华永道信息技术（上海）有限公司</h4>
		</div>
	<div id="personalinformation" style="text-align:center;">
		<h4>个人信息</h4>
	</div>
     <HR style="FILTER: alpha(opacity=100,finishopacity=0,style=3);margin-top: 10px;height: 1;" width="80%" color=#fd6709 SIZE=3>
		<form role="form" style="font-size: 18px;">
			<div class="form-group">中文姓名：
			  <label class="radio-inline">		
				 <input type="text" autofocus="autofocus" style="width:150px;" class="form-control" id="chinesename" ></label>			  
		   </div>
			<div class="form-group">英文姓名：
			<label class="radio-inline">
				<input type="text" style="width:150px;" id="englishname" class="form-control"/></label>
			</div>
			<div class="form-group">&nbsp;性&nbsp;&nbsp;别&nbsp;&nbsp; :&nbsp;&nbsp;&nbsp;  
			<label class="radio-inline">
			  <input type="radio"  value="option1" name="sex">男
			</label>
			<label class="radio-inline">
			  <input type="radio"  value="option2" name="sex">女
			</label>
			</div>
			<div class="form-group">身份证号：
			<label class="radio-inline">
				<input type="text" style="width:300px;" id="IDNumber" class="form-control"/></label>
			</div>
			<div class="form-group">出生年月：
			<label class="radio-inline">
				<input type="text" style="width:300px;" name="birthday" class="form-control"/></label>
			</div>
			<div class="form-group">&nbsp;学&nbsp;&nbsp;历&nbsp;&nbsp;：
			<label class="radio-inline">
			  <select class="form-control">
			      <option>大专</option> 
				  <option>本科</option> 
				  <option>硕士</option> 
				  <option>博士</option> 
				  <option>博士后</option> 
				  </select>
			</label>
			</div>
			<div class="form-group">本科院校：
			<label class="radio-inline">
				<input type="text" style="width:200px;" name="university" class="form-control"/></label>
			</div>	
			<div class="form-group">研究生院校：
			<label class="radio-inline">
				<input type="text" style="width:200px;" name="graduate" class="form-control"/></label>
			</div>
			<div class="form-group">学校所在地：
			<label class="radio-inline">
			  <select class="form-control">
			      <option>上海</option> 
				  <option>武汉</option> 
				  <option>大连</option> 
				  <option>海外</option> 
				  <option>其他</option> 
				  </select>
			</label>
			</div>	
		  <div class="form-group">外语水平：
			<label class="checkbox-inline">
			  <input type="checkbox"  value="option1">CET-4
			</label>
			<label class="checkbox-inline">
			  <input type="checkbox"  value="option2">CET-6
			</label>
			<label class="checkbox-inline">
				<input type="checkbox"  value="option3">TEM-4
			</label>
			<label class="checkbox-inline">
				<input type="checkbox"  value="option3">TEM-8
			</label>
			<label class="checkbox-inline">
				<input type="checkbox"  value="option3">日语1级
			</label>
			<label class="checkbox-inline">
				<input type="checkbox"  value="option3">日语2级
			</label>
			<label class="checkbox-inline">
				<input type="checkbox"  value="option3">日语3级
			</label>
			<label class="checkbox-inline">
				<input type="checkbox"  value="option3">其他<label class="radio-inline">
				<input type="text" style="width:200px;" name="other" class="form-control"/></label>
			</label>
		  </div>
			<div class="form-group">所学专业：
			<label class="radio-inline">
				<input type="text" style="width:200px;" name="major" class="form-control"/></label>
			</div>
			<div class="form-group">&nbsp;&nbsp;E-mail&nbsp;&nbsp;：
			<label class="radio-inline">
				<input type="email" style="width:300px;" name="email" class="form-control"/></label>
			</div>
			<div class="form-group">&nbsp;手&nbsp;&nbsp;机&nbsp;&nbsp;：
			<label class="radio-inline">
				<input type="email" style="width:300px;" name="phone" class="form-control"/></label>
			</div>
			<div class="form-group">服役情况：
			<label class="radio-inline">
			  <select class="form-control">
			      <option>已服役</option> 
				  <option>未服役</option> 
				  </select>
			</label>
			</div>
		</form>
		<form role="form" style="font-size: 18px;">		
			<div id="jobinformation" style="text-align:center;">
				<h4>职位信息</h4>
			</div>
			<HR style="FILTER: alpha(opacity=100,finishopacity=0,style=3);margin-top: 10px;height: 1;" width="80%" color=#fd6709 SIZE=3>
			<div class="form-group">应聘职位：
			<label class="radio-inline">
			  <select class="form-control">
			      <option>HR</option> 
				  <option>武汉</option> 
				  <option>大连</option> 
				  <option>海外</option> 
				  <option>其他</option> 
				  </select>
			</label>
			</div>
			<div class="form-group">毕业时间：
			<label class="radio-inline">
			  <select class="form-control">
			      <option>2015</option> 
				  <option>2016</option> 
				  <option>2017</option>  
				  </select>
			</label>
			</div>
			<div class="form-group">周出勤天数：
			<label class="radio-inline">
			  <select class="form-control">
			      <option>3</option> 
				  <option>4</option> 
				  <option>5</option>  
				  </select>
			</label>
			</div>
			<div class="form-group">最早可到岗时间：
		</form>
		<form class="form-horizontal" role="form" style="font-size: 18px;">
			<label class="radio-inline">
				<select class="form-control">
			      <option>2016</option> 
				  <option>2017</option> 
				  <option>2018</option>  
				  </select></label>年
			<label class="radio-inline">
				<select class="form-control">
			      <option>1</option> 
				  <option>2</option> 
				  <option>3</option> 
			      <option>4</option> 
				  <option>5</option> 
				  <option>6</option>
			      <option>7</option> 
				  <option>8</option> 
				  <option>9</option>
			      <option>10</option> 
				  <option>11</option> 
				  <option>12</option>				  
				  </select></label>月				
			</div>
		</form>
		<!--<form action="" class="form-horizontal"  role="form">
        <fieldset>
			<div class="form-group">
                <label for="dtp_input2" class="col-md-2 control-label">Date Picking</label>
                <div class="input-group date form_date col-md-5" data-date="" data-date-format="dd MM yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
                    <input class="form-control" size="16" type="text" value="" readonly>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
					<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>
				<input type="hidden" id="dtp_input2" value="" /><br/>
            </div>
        </fieldset>
    </form>-->
		<div style="text-align:center;">			
			<button class="btn btn-warning" type="button" style="padding: 6px 100px;" >Save</button><br><br>
		</div>
		</form>
		<div class="modal fade"
			style="vertical-align: middle; text-align: center;" id="mymodal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">
							<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
						</button>
						<h4 class="modal-title">提交信息</h4>
					</div>
					<div class="modal-body">
						<p>提交成功！</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<a href="javascript:confirmSubmit()"><button type="button" class="btn btn-warning">Sure</button></a>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->
		<div class="footerLogo"></div>
	</div>
	<!-- /.container -->
	<footer />
		<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="js/jquery-1.11.3.min.js"></script>
	<script src="js/bootstrap.js"></script>
	<script src="js/bootstrap-datetimepicker.min.js"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<!--<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>-->
	<script>
	  $(function(){
		$(".btn").click(function(){
		  $("#mymodal").modal("toggle");
		  keyboard:true;
		});
	  });
	</script>
	<script type="text/javascript">
	$('.form_date').datetimepicker({
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0,
		pickerPosition: "bottom-left"
    });
</script>
</body>
</body>
</html>