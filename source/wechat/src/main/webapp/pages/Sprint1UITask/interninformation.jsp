<%
String path = request.getContextPath();
%>
<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../../favicon.ico">
<link rel='stylesheet' type="text/css" href="resources/css/WHO_CSS.css">
<LINK href="favicon.ico" type="image/x-icon" rel=icon>
<LINK href="favicon.ico" type="image/x-icon" rel="shortcut icon">
<title>Intern Information</title>



<!-- Bootstrap core CSS -->
<link href="<%=path %>/resources/css/bootstrap.min.css" rel="stylesheet">
<!-- Custom styles for this template -->
<link href="<%=path %>/resources/css/starter-template.css" rel="stylesheet">
<link href="<%=path %>/resources/css/font.css" rel="stylesheet">
<script type="text/javascript" src="<%=path %>/resources/js/bootstrap.min.js"></script>
</head>
</head>

<body data-ng-app="insertdata"  data-ng-controller="insert">
	<nav class="navbar navbar-inverse navbar-fixed-top white">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar" id="nav">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<div>
				&nbsp;&nbsp;&nbsp;<img src="resources/images/footer_PWC_Logo_55_42.png">
				</div>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a href="#personalinformation"><b>Personal information</b></a></li>
					<li><a href="#jobinformation"><b>Position information</b></a></li>
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</nav>

	<div class="container">	
		<div class="page-header" style="text-align:center; margin: 10px 0 20px;">
			<h4>PwC SDC</h4>
			<h3><b>Intern Application Form</b></h3>
			
		</div>		
		<div class="list-group" id="personalinformation">
			<a href="##" class="list-group-item list-group-item-warning	"><b>Personal information</b></a>
				<div class="table-responsive">
					<table class="table table-condensed">
					  <tbody>
						<tr>
							<form role="form">
								<td style="width: 50px;font-size: 12pt;"><label for="name">Name:</label></td>
								<td style="width: 30px"><input data-ng-model="name" id="name" autofocus="autofocus" style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;"></td>
							</form>
						</tr>
						<tr>
							<form role="form">
								<td style="width: 50px;font-size: 12pt;"><label for="address">Address：</label></td>
								<td style="width: 150px"><input data-ng-model="address" id="address" style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;"></td>
							</form>
						</tr>	
						<tr>
							<form role="form">
								<td style="width: 50px;font-size: 12pt;"><b>Gender&nbsp;&nbsp;&nbsp;&nbsp;：</b></td>
								<td style="width: 150px; font-size: 12pt;"><input type="radio"  value="option1" name="sex" id="sex">male&nbsp;&nbsp;&nbsp;
									<input type="radio"  value="option2"  name="sex" id="sex">female</td>
							</form>	
						</tr>
						<tr>
							<form role="form">
								<td style="width: 50px;font-size: 12pt;"><label for="nationality">Nationality：</label></td>
								<td style="width: 150px"><input data-ng-model="nationality" id="nationality" style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;"></td>
							</form>
						</tr>												
						<tr>
							<form role="form">
								<td style="width: 50px;font-size: 12pt;"><label for="IDNumber">ID Number：</label></td>
								<td style="width: 150px"><input data-ng-model="IDNumber" id="IDNumber" style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;"></td>
							</form>
						</tr>
						<tr>
							<form role="form">
								<td style="width: 50px;font-size: 12pt;"><label for="passportcardnumber">Passport Card Number：</label></td>
								<td style="width: 150px"><input data-ng-model="passportcardnumber" id="passportcardnumber" style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;"></td>
							</form>
						</tr>
						<tr>
							<form role="form">
								<td style="width: 50px;font-size: 12pt;"><label for="birthday">Birthday：</label></td>
								<td style="width: 150px"><input data-ng-model="birthday" id="birthday" style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;"></td>
							</form>
						</tr>
						<tr>
							<form role="form">
								<td style="width: 50px;font-size: 12pt;"><label for="birthplace">Birthplace：</label></td>
								<td style="width: 150px"><input data-ng-model="birthplace" id="birthplace" style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;"></td>
							</form>
						</tr>
						<tr>
							<form role="form">
								<td style="width: 50px;font-size: 12pt;"><label for="homephone">Home Phone：</label></td>
								<td style="width: 150px"><input data-ng-model="homephone" id="homephone" style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;"></td>
							</form>
						</tr>
						<tr>
							<form role="form">
								<td style="width: 50px;font-size: 12pt;"><label for="cellphone">Cell Phone：</label></td>
								<td style="width: 150px"><input data-ng-model="cellphone" id="cellphone" style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;"></td>
							</form>
						</tr>
						<tr>
							<form role="form">
								<td style="width: 50px;font-size: 12pt;"><label for="email">E-mail&nbsp;&nbsp;：</label></td>
								<td style="width: 150px"><input data-ng-model="email" type="email" id="email" style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff;font-size: 12pt;"></td>
							</form>
						</tr>
						<tr>
							<form role="form">
								<td style="width: 50px;font-size: 12pt;"><label for="personalpage">Personal Page：</label></td>
								<td style="width: 150px"><input data-ng-model="personalpage" id="personalpage" style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;"></td>
							</form>
						</tr>
						<tr>
							<form role="form">
								<td style="width: 50px;font-size: 12pt;"><label for="Skills">Skills：</label></td>
								<td style="width: 150px"><input data-ng-model="Skills" id="Skills" style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;"></td>
							</form>
						</tr>
						<tr>
							<form role="form">
								<td style="width: 50px;font-size: 12pt;"><label for="furtherstudyintention">Further Study Intention：</label></td>
								<td style="width: 150px"><input data-ng-model="furtherstudyintention" id="furtherstudyintention" style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;"></td>
							</form>
						</tr>
						<tr>
							<form role="form">
								<div class="form-group">
									<td style="width: 50px;font-size: 12pt;"><b>Language skill：</b></td>
									<td style="width: 150px; font-size: 12pt;">			  
										<label class="checkbox-inline"><input type="checkbox"  name="foreignlanguage[]" value="option1">CET-4</label>	
										<label class="checkbox-inline"><input type="checkbox"  name="foreignlanguage[]" value="option2">CET-6</label><br>
										<label class="checkbox-inline"><input type="checkbox"  name="foreignlanguage[]" value="option3">TEM-4</label>
										<label class="checkbox-inline"><input type="checkbox"  name="foreignlanguage[]" value="option4">TEM-8</label><br>
										<label class="checkbox-inline"><input type="checkbox"  name="foreignlanguage[]" value="option5">JPT-1</label>
										<label class="checkbox-inline"><input type="checkbox"  name="foreignlanguage[]" value="option6">JPT-2</label><br>
										<label class="checkbox-inline"><input type="checkbox"  name="foreignlanguage[]" value="option7">JPT-3</label>
										<label class="checkbox-inline"><input type="checkbox"  name="foreignlanguage[]" value="option8"><label for="other">other:</label></label>
										<input id="other" style="width: 100px;border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;">
									</td>
								</div>
							</form>	
						</tr>
						<!--<tr>
							<form role="form">
								<td style="width: 50px;font-size: 12pt;"><b>Veteran status：</b></td>
								<td style="width: 150px; font-size: 12pt;">			  
									<select class="form-control" style="width: 150px;">
									  <option>Already served</option> 
									  <option>Not served</option>
									</select>
								</td>
							</form>	
						</tr>-->				
					</tbody>	
				</table>
			</div>
		</div>											
	  <div class="panel panel-default">
	    <div class="panel-heading">
	      <h4 class="panel-title">
	        <a data-toggle="collapse" data-parent="#accordion" href="#education" >Education:</a>
	      </h4>
	    </div>
	    <div id="education" class="panel-collapse collapse">
	      <div class="panel-body">
			<table class="table table-condensed">
			  <tbody>
				<tr>
					<form role="form">
						<td style="width: 50px;font-size: 12pt;"><label for="university">University:</label></td>
						<td style="width: 30px"><input data-ng-model="university" id="university" style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;"></td>
					</form>
				</tr>
				<tr>
					<form role="form">
						<td style="width: 50px;font-size: 12pt;"><label for="major">Major:</label></td>
						<td style="width: 30px"><input data-ng-model="major" id="major" style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;"></td>
					</form>
				</tr>
				<tr>
					<form role="form">
						<td style="width: 50px;font-size: 12pt;"><label for="studyinterval">Study Interval:</label></td>
						<td style="width: 30px"><input data-ng-model="studyinterval" id="studyinterval" style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;"></td>
					</form>
				</tr>
				<tr>
					<form role="form">
						<td style="width: 50px;font-size: 12pt;"><label for="GPA">GPA:</label></td>
						<td style="width: 30px"><input data-ng-model="GPA" id="GPA" style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;"></td>
					</form>
				</tr>
			  </tbody>	
			</table>
			<div>
			<button class="btn btn-default" type="button" style="padding: 4px 10px;" id="addeducation">Add +</button>
			</div>
	      </div>
	    </div>	    
	  </div>
	  <div class="panel panel-default">
	    <div class="panel-heading">
	      <h4 class="panel-title">
	        <a data-toggle="collapse" data-parent="#accordion" href="#workexperience">Work Experience:</a>
	      </h4>
	    </div>
	    <div id="workexperience" class="panel-collapse collapse">
	      <div class="panel-body">
			<table class="table table-condensed">
			  <tbody>
				<tr>
					<form role="form">
						<td style="width: 50px;font-size: 12pt;"><label for="company">Company:</label></td>
						<td style="width: 30px"><input data-ng-model="company" id="company" style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;"></td>
					</form>
				</tr>
				<tr>
					<form role="form">
						<td style="width: 50px;font-size: 12pt;"><label for="position">Position:</label></td>
						<td style="width: 30px"><input data-ng-model="position" id="position" style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;"></td>
					</form>
				</tr>
				<tr>
					<form role="form">
						<td style="width: 50px;font-size: 12pt;"><label for="workinginterval">Working Interval :</label></td>
						<td style="width: 30px"><input data-ng-model="workinginterval" id="workinginterval" style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;"></td>
					</form>
				</tr>
				<tr>
					<form role="form">
						<td style="width: 50px;font-size: 12pt;"><label for="detaildescription">Detail Description:</label></td>
						<td style="width: 30px"><input data-ng-model="detaildescription" id="detaildescription" style="border: #ffffff; border-style: solid; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 1px; border-left-width: 0px; background-color: #ffffff; font-size: 12pt;"></td>
					</form>
				</tr>
			  </tbody>	
			</table>
			<div>
			<button class="btn btn-default" type="button" style="padding: 4px 10px;" id="addeducation">Add +</button>
			</div>
	      </div>
	    </div>	    
	  </div>										
	  <div class="list-group" id="jobinformation">
		<a href="##" class="list-group-item list-group-item-warning	"><b>Position information</b></a>
			<div class="table-responsive">
				<table class="table table-condensed">
					<tbody>	
						<tr>
							<form role="form">
								<td style="width: 50px;font-size: 12pt;"><b>Apply Position：</b></td>
								<td style="width: 150px; font-size: 12pt;">			  
									<select class="form-control" style="width: 100px;">
									  <option>HR</option> 
									  <option>GW</option> 
									  <option>1</option> 
									  <option>2</option> 
									  <option>3</option> 
									</select>
								</td>
							</form>	
						</tr>
						<tr>
							<form role="form">
								<td style="width: 50px;font-size: 12pt;"><b>Available Day：</b></td>
								<td style="width: 150px; font-size: 12pt;">			  
									<select class="form-control" style="width: 100px;" id="availableday";>
									  <option>3</option> 
									  <option>4</option> 
									  <option>5</option>
									</select>
								</td>
							</form>	
						</tr>
					</tbody>	
				</table>
			</div>
		</div>						
		<div style="text-align:center;">
			<button class="btn btn-warning" type="button" style="padding: 6px 100px;" id="submit" data-toggle="modal" data-target="#confirm">Submit</button><br><br>
		</div>
		<div class="modal fade" id="confirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="vertical-align: middle; text-align: center;">
		   <div class="modal-dialog">
		      <div class="modal-content">
		         <div class="modal-header">
		            <button type="button" class="close" data-dismiss="modal" 
		               aria-hidden="true">×
		            </button>
		            <h4 class="modal-title" id="myModalLabel">Please confirm your information then sign!</h4>
		         </div>
				<form role="form" style="margin-bottom:0;" class="modal-tmp">
				  <div class="modal-body">
					<div ng-app="">
					<h1 style="font-family: stxingkairegular;"> {{name}}</h1>						
						<input placeholder="Please Sign:" type="text" class="form-control" required="" autocomplete="on" value="" ng-model="name" autofocus="autofocus">
					</div>
				  </div>
				</form>
		         <div class="modal-footer">
		            <button type="button" class="btn btn-warning" data-dismiss="modal" data-toggle="modal" data-target="#sure" onclick="commit()">Sure</button>
		            <button type="button" class="btn btn-default" data-dismiss="modal">Back</button>
		         </div>
		      </div><!-- /.modal-content -->
		   </div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
		<div class="modal fade" id="sure" tabindex="-1" role="dialog" 
		    aria-labelledby="myModalLabel" aria-hidden="true" style="vertical-align: middle; text-align: center;">
		   <div class="modal-dialog">
		      <div class="modal-content">
		         <div class="modal-header">
		            <button type="button" class="close" data-dismiss="modal" 
		               aria-hidden="true">×
		            </button>
		            <h4 class="modal-title" id="myModalLabel">Submit Success!</h4>
		         </div>
				  <div class="modal-body">
				  Submit Success!
				  </div>
		         <div class="modal-footer">
		            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		         </div>
		      </div><!-- /.modal-content -->
		   </div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
	</div>
	<!-- /.container -->
		<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="<%=path %>/resources/js/jquery-1.11.3.min.js"></script>
	<script src="<%=path %>/resources/js/bootstrap.min.js"></script>
	<script src="<%=path %>/resources/js/angular.min.js"></script>
	<script src="<%=path %>/resources/js/interninformation.js"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<!--<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>-->

</body>
</html>